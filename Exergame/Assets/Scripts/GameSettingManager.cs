﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettingManager : MonoBehaviour
{
    [ReadOnly]
    public bool Locked = true;

    [ReadOnly]
    public int FrameRate = 150;

    void Start()
    {
        EnableLockMouse(Locked);
        ChangeFrameRate(FrameRate);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            Locked = !Locked;
            EnableLockMouse(Locked);
        }
    }

    public void EnableLockMouse(bool input)
    {
        Cursor.lockState = input ? CursorLockMode.Locked : CursorLockMode.None;
        Cursor.visible = !input;
    }

    public void ChangeFrameRate(int count)
    {
        Application.targetFrameRate = count;
    }
}
