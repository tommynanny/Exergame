﻿using PedometerU;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StepDisplay : MonoBehaviour
{
	public Text StepText;
	public Text DistanceText;

	private Pedometer pedometer;

	private void Start()
	{
		pedometer = new Pedometer(
		(int steps, double distance)=>
		{
			// Display the values
			StepText.text = steps.ToString();
			// Display distance in feet
			DistanceText.text = (distance * 3.28084).ToString("F2") + " ft";
		});
	}
}
