﻿using Doozy.Engine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleThemeButton : MonoBehaviour
{
	public bool isShown = false;
	public Animator TargetAnimator;
	public void ShowMenu(bool input)
	{
		if (input)
		{
			TargetAnimator.SetTrigger("Show");
		}
		else
		{
			TargetAnimator.SetTrigger("Hide");
		}
		isShown = input;
	}

	public void ToggleMenu()
	{
		ShowMenu(!isShown);
	}

	private void Start()
	{
		TargetAnimator.transform.GetComponentsInChildren<UIButton>().ForeachNotNull(
		b => b.OnClick.OnTrigger.Event.AddListener(
		delegate
		{
			ShowMenu(false);
		}));
	}
}
