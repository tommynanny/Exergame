﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using SWS;

[RequireComponent(typeof(MoveAnimator))]
public class PlayerControl : MonoBehaviour
{
	public splineMove splineMove;

	[SerializeField, HideInInspector]
	float _RunningSpeed = 0f;

	[ShowInInspector]
	[PropertyRange(0, 10)]
	public float RunningSpeed
	{
		get { return _RunningSpeed; }
		set
		{
			if (value == _RunningSpeed) return;
			_RunningSpeed = value;
			UpdateSpeed();
		}
	}

	private void UpdateSpeed()
	{
		splineMove.ChangeSpeed(RunningSpeed);
	}
}
