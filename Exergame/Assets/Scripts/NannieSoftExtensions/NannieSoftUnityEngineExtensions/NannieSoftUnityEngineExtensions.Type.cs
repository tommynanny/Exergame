﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using Newtonsoft.Json;
using Sirenix.Utilities;

#if UNITY_EDITOR
using UnityEditor;
#endif

public static partial class NannieSoftUnityEngineExtensions
{
	public static T ToType<T>(this object obj)
	{
		return (T)obj;
	}

	public static dynamic ToType(this object obj, Type T)
	{
		return System.Convert.ChangeType(obj, T);
	}
}


