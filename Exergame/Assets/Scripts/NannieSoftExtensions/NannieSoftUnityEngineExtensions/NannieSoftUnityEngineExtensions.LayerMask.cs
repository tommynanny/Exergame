﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using Newtonsoft.Json;
using Sirenix.Utilities;

#if UNITY_EDITOR
using UnityEditor;
#endif

public static partial class NannieSoftUnityEngineExtensions
{
	
	#region LayerMask
	public static bool ContainsLayer(this LayerMask layermask, int layer)
	{
		return layermask == (layermask | (1 << layer));
	}
	#endregion


}


