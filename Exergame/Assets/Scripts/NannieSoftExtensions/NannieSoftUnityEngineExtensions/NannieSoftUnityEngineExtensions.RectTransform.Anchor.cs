﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using Newtonsoft.Json;
using Sirenix.Utilities;

#if UNITY_EDITOR
using UnityEditor;
#endif

public enum AnchorPresets
{
	TopLeft,
	TopCenter,
	TopRight,

	MiddleLeft,
	MiddleCenter,
	MiddleRight,

	BottomLeft,
	BottonCenter,
	BottomRight,

	StretchLeftVertical,
	StretchRightVertical,
	StretchCenterVertical,

	StretchTopHorizontal,
	StretchMiddleHorizontal,
	StretchBottomHorizontal,

	StretchAll
}
public enum PivotPresets
{
	TopLeft,
	TopCenter,
	TopRight,

	MiddleLeft,
	MiddleCenter,
	MiddleRight,

	BottomLeft,
	BottomCenter,
	BottomRight,
}

public static partial class NannieSoftUnityEngineExtensions
{
	public static void SetAnchor(this RectTransform Source, AnchorPresets AnchorPresets, bool AlsoSetPosition = false)
	{
		switch (AnchorPresets)
		{
			case (AnchorPresets.TopLeft):
				{
					Source.anchorMin = new Vector2(0, 1);
					Source.anchorMax = new Vector2(0, 1);
					if (AlsoSetPosition)
					{
						Source.anchoredPosition = Vector2.zero;
					}
					break;
				}
			case (AnchorPresets.TopCenter):
				{
					Source.anchorMin = new Vector2(0.5f, 1);
					Source.anchorMax = new Vector2(0.5f, 1);

					if (AlsoSetPosition)
					{
						Source.anchoredPosition = Vector2.zero;
					}
					break;
				}
			case (AnchorPresets.TopRight):
				{
					Source.anchorMin = new Vector2(1, 1);
					Source.anchorMax = new Vector2(1, 1);

					if (AlsoSetPosition)
					{
						Source.anchoredPosition = Vector2.zero;
					}
					break;
				}

			case (AnchorPresets.MiddleLeft):
				{
					Source.anchorMin = new Vector2(0, 0.5f);
					Source.anchorMax = new Vector2(0, 0.5f);

					if (AlsoSetPosition)
					{
						Source.anchoredPosition = Vector2.zero;
					}
					break;
				}
			case (AnchorPresets.MiddleCenter):
				{
					Source.anchorMin = new Vector2(0.5f, 0.5f);
					Source.anchorMax = new Vector2(0.5f, 0.5f);

					if (AlsoSetPosition)
					{
						Source.anchoredPosition = Vector2.zero;
					}
					break;
				}
			case (AnchorPresets.MiddleRight):
				{
					Source.anchorMin = new Vector2(1, 0.5f);
					Source.anchorMax = new Vector2(1, 0.5f);

					if (AlsoSetPosition)
					{
						Source.anchoredPosition = Vector2.zero;
					}
					break;
				}

			case (AnchorPresets.BottomLeft):
				{
					Source.anchorMin = new Vector2(0, 0);
					Source.anchorMax = new Vector2(0, 0);

					if (AlsoSetPosition)
					{
						Source.anchoredPosition = Vector2.zero;
					}
					break;
				}
			case (AnchorPresets.BottonCenter):
				{
					Source.anchorMin = new Vector2(0.5f, 0);
					Source.anchorMax = new Vector2(0.5f, 0);

					if (AlsoSetPosition)
					{
						Source.anchoredPosition = Vector2.zero;
					}
					break;
				}
			case (AnchorPresets.BottomRight):
				{
					Source.anchorMin = new Vector2(1, 0);
					Source.anchorMax = new Vector2(1, 0);

					if (AlsoSetPosition)
					{
						Source.anchoredPosition = Vector2.zero;
					}
					break;
				}

			case (AnchorPresets.StretchTopHorizontal):
				{
					Source.anchorMin = new Vector2(0, 1);
					Source.anchorMax = new Vector2(1, 1);

					if (AlsoSetPosition)
					{
						Source.anchoredPosition = Vector2.zero;
					}
					break;
				}
			case (AnchorPresets.StretchMiddleHorizontal):
				{
					Source.anchorMin = new Vector2(0, 0.5f);
					Source.anchorMax = new Vector2(1, 0.5f);

					if (AlsoSetPosition)
					{
						Source.anchoredPosition = Vector2.zero;
					}
					break;
				}
			case (AnchorPresets.StretchBottomHorizontal):
				{
					Source.anchorMin = new Vector2(0, 0);
					Source.anchorMax = new Vector2(1, 0);

					if (AlsoSetPosition)
					{
						Source.anchoredPosition = Vector2.zero;
					}
					break;
				}

			case (AnchorPresets.StretchLeftVertical):
				{
					Source.anchorMin = new Vector2(0, 0);
					Source.anchorMax = new Vector2(0, 1);

					if (AlsoSetPosition)
					{
						Source.anchoredPosition = Vector2.zero;
					}
					break;
				}
			case (AnchorPresets.StretchCenterVertical):
				{
					Source.anchorMin = new Vector2(0.5f, 0);
					Source.anchorMax = new Vector2(0.5f, 1);

					if (AlsoSetPosition)
					{
						Source.anchoredPosition = Vector2.zero;
					}
					break;
				}
			case (AnchorPresets.StretchRightVertical):
				{
					Source.anchorMin = new Vector2(1, 0);
					Source.anchorMax = new Vector2(1, 1);

					if (AlsoSetPosition)
					{
						Source.anchoredPosition = Vector2.zero;
					}
					break;
				}

			case (AnchorPresets.StretchAll):
				{
					Source.anchorMin = new Vector2(0, 0);
					Source.anchorMax = new Vector2(1, 1);

					if (AlsoSetPosition)
					{
						Source.anchoredPosition = Vector2.zero;
					}
					break;
				}
		}
	}

	public static void SetPivot(this RectTransform Source, PivotPresets PivotPresets)
	{

		switch (PivotPresets)
		{
			case (PivotPresets.TopLeft):
				{
					Source.pivot = new Vector2(0, 1);
					break;
				}
			case (PivotPresets.TopCenter):
				{
					Source.pivot = new Vector2(0.5f, 1);
					break;
				}
			case (PivotPresets.TopRight):
				{
					Source.pivot = new Vector2(1, 1);
					break;
				}

			case (PivotPresets.MiddleLeft):
				{
					Source.pivot = new Vector2(0, 0.5f);
					break;
				}
			case (PivotPresets.MiddleCenter):
				{
					Source.pivot = new Vector2(0.5f, 0.5f);
					break;
				}
			case (PivotPresets.MiddleRight):
				{
					Source.pivot = new Vector2(1, 0.5f);
					break;
				}

			case (PivotPresets.BottomLeft):
				{
					Source.pivot = new Vector2(0, 0);
					break;
				}
			case (PivotPresets.BottomCenter):
				{
					Source.pivot = new Vector2(0.5f, 0);
					break;
				}
			case (PivotPresets.BottomRight):
				{
					Source.pivot = new Vector2(1, 0);
					break;
				}
		}
	}
}


