﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using Newtonsoft.Json;
using Sirenix.Utilities;

#if UNITY_EDITOR
using UnityEditor;
#endif

public static partial class NannieSoftUnityEngineExtensions
{
	
	#region String
	public static string ToTitleCase(this string title)
	{
		return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(title.ToLower());
	}
	#endregion
	

}


