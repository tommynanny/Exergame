﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using Newtonsoft.Json;
using Sirenix.Utilities;

#if UNITY_EDITOR
using UnityEditor;
#endif

public static partial class NannieSoftUnityEngineExtensions
{
	
	#region Json
	public static string JsonSerialize(this object input)
	{
		//return JsonConvert.SerializeObject(input);
		return JsonConvert.SerializeObject(input, Formatting.Indented, new JsonSerializerSettings() { ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore });
	}

	public static T JsonDeserialize<T>(this object input)
	{
		return JsonConvert.DeserializeObject<T>(input.ToString());
	}

	public static object JsonDeserialize(this object input, Type type)
	{
		return JsonConvert.DeserializeObject(input.ToString(), type);
	}
	#endregion


}


