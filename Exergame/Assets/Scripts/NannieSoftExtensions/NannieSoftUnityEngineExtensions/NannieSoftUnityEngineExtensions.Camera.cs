﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using Newtonsoft.Json;
using Sirenix.Utilities;

#if UNITY_EDITOR
using UnityEditor;
#endif

public static partial class NannieSoftUnityEngineExtensions
{
	
	#region Camera
	public static Vector3 MouseWorldPosition(Camera cam)
	{
		return cam.ScreenToWorldPoint(Input.mousePosition.SetZ(-cam.transform.position.z)).SetZ(0f);
	}
	#endregion


}


