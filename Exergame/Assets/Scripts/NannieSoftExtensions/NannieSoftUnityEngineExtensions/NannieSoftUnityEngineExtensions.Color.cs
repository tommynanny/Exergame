﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using Newtonsoft.Json;
using Sirenix.Utilities;

#if UNITY_EDITOR
using UnityEditor;
#endif

public static partial class NannieSoftUnityEngineExtensions
{
	
	#region Color
	public static Color SetRGB(this Color color, float r = -1, float g = -1, float b = -1, float a = -1)
	{
		return new Color(r == -1 ? color.r : r, g == -1 ? color.g : g, b == -1 ? color.b : b, a == -1 ? color.a : a);
	}

	public static Color ChangeRGBAmount(this Color color, float r = 0, float g = 0, float b = 0, float a = 0)
	{
		return new Color(color.r + r, color.g + g, color.b + b, color.a + a);
	}
	#endregion


}


