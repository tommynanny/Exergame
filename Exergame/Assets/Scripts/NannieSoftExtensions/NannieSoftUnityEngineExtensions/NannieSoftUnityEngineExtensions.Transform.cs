﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using Newtonsoft.Json;
using Sirenix.Utilities;

#if UNITY_EDITOR
using UnityEditor;
#endif

public static partial class NannieSoftUnityEngineExtensions
{
	
	#region Transform
	#region Transform Position
	public static void SetPositionX(this Transform tran, float input)
	{
		Vector3 temp = tran.position;
		temp.x = input;
		tran.position = temp;
	}

	public static void SetPositionY(this Transform tran, float input)
	{
		Vector3 temp = tran.position;
		temp.y = input;
		tran.position = temp;
	}
	public static void SetPositionZ(this Transform tran, float input)
	{
		Vector3 temp = tran.position;
		temp.z = input;
		tran.position = temp;
	}

	public static void SetLocalPositionX(this Transform tran, float input)
	{
		Vector3 temp = tran.localPosition;
		temp.x = input;
		tran.localPosition = temp;
	}

	public static void SetLocalPositionY(this Transform tran, float input)
	{
		Vector3 temp = tran.localPosition;
		temp.y = input;
		tran.localPosition = temp;
	}
	public static void SetLocalPositionZ(this Transform tran, float input)
	{
		Vector3 temp = tran.localPosition;
		temp.z = input;
		tran.localPosition = temp;
	}
	#endregion

	#region Transform Rotation
	public static void SetEulerAnglesX(this Transform tran, float input)
	{
		Vector3 temp = tran.eulerAngles;
		temp.x = input;
		tran.eulerAngles = temp;
	}

	public static void SetEulerAnglesY(this Transform tran, float input)
	{
		Vector3 temp = tran.eulerAngles;
		temp.y = input;
		tran.eulerAngles = temp;
	}

	public static void SetEulerAnglesZ(this Transform tran, float input)
	{
		Vector3 temp = tran.eulerAngles;
		temp.z = input;
		tran.eulerAngles = temp;
	}

	public static void SetLocalEulerAnglesX(this Transform tran, float input)
	{
		Vector3 temp = tran.localEulerAngles;
		temp.x = input;
		tran.localEulerAngles = temp;
	}

	public static void SetLocalEulerAnglesY(this Transform tran, float input)
	{
		Vector3 temp = tran.localEulerAngles;
		temp.y = input;
		tran.localEulerAngles = temp;
	}

	public static void SetLocalEulerAnglesZ(this Transform tran, float input)
	{
		Vector3 temp = tran.localEulerAngles;
		temp.z = input;
		tran.localEulerAngles = temp;
	}

	public static void SetLocalScaleX(this Transform tran, float input)
	{
		Vector3 temp = tran.localScale;
		temp.x = input;
		tran.localScale = temp;
	}

	public static void SetLocalScaleY(this Transform tran, float input)
	{
		Vector3 temp = tran.localScale;
		temp.y = input;
		tran.localScale = temp;
	}

	public static void SetLocalScaleZ(this Transform tran, float input)
	{
		Vector3 temp = tran.localScale;
		temp.z = input;
		tran.localScale = temp;
	}

	#endregion

	//Breadth-first search
	public static Transform FindDeepChild(this Transform aParent, string aName)
	{
		Queue<Transform> queue = new Queue<Transform>();
		queue.Enqueue(aParent);
		while (queue.Count > 0)
		{
			var c = queue.Dequeue();
			if (c.name == aName)
				return c;
			foreach (Transform t in c)
				queue.Enqueue(t);
		}
		return null;
	}

	public static void Load(this Transform tran, TransformData data, bool WorldSpace = true)
	{
		if (WorldSpace)
		{
			tran.position = data.Position;
			tran.eulerAngles = data.EulerAngles;
		}
		else
		{
			tran.localPosition = data.Position;
			tran.localEulerAngles = data.EulerAngles;
		}

	}
	public static TransformData Save(this Transform tran, bool WorldSpace = true)
	{
		return WorldSpace ? new TransformData(tran.position, tran.eulerAngles) : new TransformData(tran.localPosition, tran.localEulerAngles);
	}
	#endregion

	public struct TransformData
	{
		public Vector3 Position;
		public Vector3 EulerAngles;
		public TransformData(Vector3 pos, Vector3 euler)
		{
			this.Position = pos;
			this.EulerAngles = euler;
		}
	}
}


