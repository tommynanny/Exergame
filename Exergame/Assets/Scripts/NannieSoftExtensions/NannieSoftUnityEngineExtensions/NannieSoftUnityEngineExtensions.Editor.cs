﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using Newtonsoft.Json;
using Sirenix.Utilities;

#if UNITY_EDITOR
using UnityEditor;
#endif

public static partial class NannieSoftUnityEngineExtensions
{
	
	#region Editor
#if UNITY_EDITOR

	[MenuItem("CONTEXT/Component/RENAME")]
	static void DoubleMass(MenuCommand command)
	{
		Component body = (Component)command.context;
		body.gameObject.name = body.GetType().Name;
	}
#endif
	#endregion


}


