﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using Newtonsoft.Json;
using Sirenix.Utilities;
using UnityEngine.UI;

#if UNITY_EDITOR
using UnityEditor;
#endif

public static partial class NannieSoftUnityEngineExtensions
{


	#region RectTransform
	public static void SetToCenterOfCanvas(this RectTransform rt, Canvas canvas = null)
	{
		var canvasRect = canvas == null ? rt.GetComponentInParent<Canvas>().GetComponent<RectTransform>() : canvas.GetComponent<RectTransform>();
		rt.SetAllAnchorsTo(TextAnchor.MiddleCenter);
		rt.anchoredPosition = Vector2.zero;
	}

	public static void SetAllAnchorsTo(this RectTransform rt, TextAnchor anchor)
	{
		var split = System.Text.RegularExpressions.Regex.Split(anchor.ToString(), @"(?<!^)(?=[A-Z])");
		Func<string, float> Detect = x =>
		{
			if (new Regex("Lower|Left").IsMatch(x))
			{
				return 0f;
			}
			else if (new Regex("Middle|Center").IsMatch(x))
			{
				return 0.5f;
			}
			else if (new Regex("Upper|Right").IsMatch(x))
			{
				return 1f;
			}
			return 0f;
		};

		var result = new Vector2(Detect(split[0]), Detect(split[1]));
		rt.anchorMin = result;
		rt.anchorMax = result;
	}



	public static void StretchParent(this RectTransform rt)
	{
		rt.anchorMin = Vector2.zero;
		rt.anchorMax = new Vector2(1, 1);
		rt.SetLeftRightTopBottom(0, 0, 0, 0);
	}

	public static void SetLeftRightTopBottom(this RectTransform rt, float left, float right, float top, float bottom)
	{
		rt.SetLeft(left);
		rt.SetRight(right);
		rt.SetTop(top);
		rt.SetBottom(bottom);
	}
	public static void SetLeft(this RectTransform rt, float left)
	{
		rt.offsetMin = new Vector2(left, rt.offsetMin.y);
	}

	public static void SetRight(this RectTransform rt, float right)
	{
		rt.offsetMax = new Vector2(-right, rt.offsetMax.y);
	}

	public static void SetTop(this RectTransform rt, float top)
	{
		rt.offsetMax = new Vector2(rt.offsetMax.x, -top);
	}

	public static void SetBottom(this RectTransform rt, float bottom)
	{
		rt.offsetMin = new Vector2(rt.offsetMin.x, bottom);
	}

	public static void SetAnchorX(this RectTransform rt, float x)
	{
		var pos = rt.anchoredPosition;
		pos.x = x;
		rt.anchoredPosition = pos;
	}

	public static void SetAnchorY(this RectTransform rt, float y)
	{
		var pos = rt.anchoredPosition;
		pos.y = y;
		rt.anchoredPosition = pos;
	}

	public static void EnableAllChildrenVisuals(this RectTransform rt, bool setting = true)
	{
		rt.GetComponentsInChildren<Image>().ForeachNotNull(img => img.enabled = setting);
	}


	#endregion


}


