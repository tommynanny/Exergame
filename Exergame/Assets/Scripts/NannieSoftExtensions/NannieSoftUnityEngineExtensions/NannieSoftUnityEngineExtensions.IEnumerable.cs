﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEngine;
using Newtonsoft.Json;

#if UNITY_EDITOR
using UnityEditor;
#endif

public static partial class NannieSoftUnityEngineExtensions
{
	//public static List<T> ToList<T>(this IEnumerable<T> source)
	//{
	//	return source.ToList();
	//}

	//public static T[] ToArray<T>(this IEnumerable<T> source)
	//{
	//	return source.ToArray();
	//}

	public static IEnumerable<T> GetRemovedFromMeTo<T>(this IEnumerable<T> listA, IEnumerable<T> listB)
	{
		return listA.Except(listB).ToList();
	}

	public static IEnumerable<T> GetAddedFromMeTo<T>(this IEnumerable<T> listA, IEnumerable<T> listB)
	{
		return listB.Except(listA).ToList();
	}

	public static bool HasSameGameObjectWith(this IEnumerable<Component> listA, IEnumerable<Component> listB)
	{
		if (listA.Count() != listB.Count()) return false;
		int sum = 0;
		foreach (var i in listA)
		{
			foreach (var j in listB)
			{
				if (i.gameObject.GetHashCode() == j.gameObject.GetHashCode())
				{
					sum++;
				}
			}
		}

		return sum == listA.Count();
	}

	public static System.Random random = new System.Random();
	public static T GetRandom<T>(this IEnumerable<T> input, bool ExcludeNullsFirst = true)
	{
		return input.ToArray().GetRandom();
	}
	public static T GetRandom<T>(this T[] array, bool ExcludeNullsFirst = true)
	{
		if (ExcludeNullsFirst)
		{
			array.CleanNullEntries();
		}
		if (array == null || array.Length == 0) return default(T);
		return array[random.Next(0, array.Length)];
	}
	public static IEnumerable<T> CleanNullEntries<T>(this IEnumerable<T> input)
	{
		if (input == null) return null;
		return input.Where(x => x != null);
	}
	public static List<int> AllIndexesOf(this string str, string value)
	{
		if (String.IsNullOrEmpty(value))
			throw new ArgumentException("the string to find may not be empty", "value");
		List<int> indexes = new List<int>();
		for (int index = 0; ; index += value.Length)
		{
			index = str.IndexOf(value, index);
			if (index == -1)
				return indexes;
			indexes.Add(index);
		}
	}
}


