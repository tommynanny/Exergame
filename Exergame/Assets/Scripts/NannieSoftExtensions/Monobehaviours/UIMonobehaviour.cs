﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class UIMonobehaviour : MonoBehaviour
{
	RectTransform _rectTransform;

	public RectTransform rectTransform
	{
		get
		{
			if (_rectTransform == null)
			{
				_rectTransform = transform as RectTransform;
			}
			return _rectTransform;
		}
	}


	Canvas _canvas;

	public Canvas canvas
	{
		get
		{
			if (_canvas == null)
			{
				_canvas = gameObject.GetComponent<Canvas>();
			}
			return _canvas;
		}
	}


	CanvasGroup _canvasGroup;

	public CanvasGroup canvasGroup
	{
		get
		{
			if (_canvasGroup == null)
			{
				_canvasGroup = gameObject.GetComponent<CanvasGroup>();
			}
			return _canvasGroup;
		}
	}


}
