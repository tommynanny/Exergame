﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;

namespace NannieSoft
{
	public class VRControllerManager : MonoBehaviour
	{
		#region singleton

		public static VRControllerManager _instance;
		public static VRControllerManager instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = FindObjectOfType<VRControllerManager>();
				}
				return _instance;
			}
		}

		#endregion

		[TabGroup("Left")]
		public VRController Left = new VRController() { DeviceNode = UnityEngine.XR.XRNode.LeftHand, HideDeviceNode = true };

		[TabGroup("HMD")]
		public VRDevice HMD = new VRDevice() { DeviceNode = UnityEngine.XR.XRNode.Head, HideDeviceNode = true };

		[TabGroup("Right")]
		public VRController Right = new VRController() { DeviceNode = UnityEngine.XR.XRNode.RightHand, HideDeviceNode = true };

		Transform VRCamera;
		Transform VRCameraDriver;

		private void Start()
		{
			HMD.SetupVRDevice();
			Left.SetupVRDevice();
			Right.SetupVRDevice();
		}

		private void Update()
		{
			//get trigger pressed
			HMD.HandleUpdateDevice();
			Left.HandleUpdateDevice();
			Right.HandleUpdateDevice();
		}

		public void TeleportPlayerFeetPos2(Vector3 destination)
		{
			VRCameraDriver.position = destination + new Vector3(0, VRCamera.localPosition.y, 0) - VRCamera.localPosition;
		}

		Vector2 GetTheDirection(Vector2 input)
		{
			if (input.y > input.x && input.y > -input.x)
			{
				return new Vector2(0, 1);
			}
			if (input.y < input.x && input.y > -input.x)
			{
				return new Vector2(1, 0);
			}
			if (input.y > input.x && input.y < -input.x)
			{
				return new Vector2(-1, 0);
			}
			if (input.y < input.x && input.y < -input.x)
			{
				return new Vector2(0, -1);
			}
			return new Vector2(0, 0);
		}

		[Button("Devices Info")]
		public void LogAllStatus()
		{
			var inputDevices = new List<UnityEngine.XR.InputDevice>();
			UnityEngine.XR.InputDevices.GetDevices(inputDevices);

			foreach (var device in inputDevices)
			{
				VRDevice.LogStatus(device);
			}
		}

		public bool TriggerButtonDown(UnityEngine.XR.XRNode Controller)
		{
			if (Controller == UnityEngine.XR.XRNode.LeftHand)
				return Left.TriggerButtonDown;
			if (Controller == UnityEngine.XR.XRNode.RightHand)
				return Right.TriggerButtonDown;
			return false;
		}

		public bool TriggerButtonUp(UnityEngine.XR.XRNode Controller)
		{
			if (Controller == UnityEngine.XR.XRNode.LeftHand)
				return Left.TriggerButtonUp;
			if (Controller == UnityEngine.XR.XRNode.RightHand)
				return Right.TriggerButtonUp;
			return false;
		}


		public bool TriggerButtonPressed(UnityEngine.XR.XRNode Controller)
		{
			if (Controller == UnityEngine.XR.XRNode.LeftHand)
				return Left.TriggerPressed;
			if (Controller == UnityEngine.XR.XRNode.RightHand)
				return Right.TriggerPressed;
			return false;
		}

	}


	#region data class
	[Serializable]
	public class VRDevice
	{
		[HideIf("HideDeviceNode")]
		public UnityEngine.XR.XRNode DeviceNode;
		[HideInInspector] public UnityEngine.XR.InputDevice InputDevice;
		[ReadOnly] public Vector3 DevicePosition;
		[ReadOnly] public Quaternion DeviceRotation;

		[InlineButton("CreateTrackingObj", "Create")]
		[GUIColor(0, 1, 1)]
		public Transform TrackingObj;

		void CreateTrackingObj()
		{
			TrackingObj = GameObject.CreatePrimitive(PrimitiveType.Sphere).transform;
			TrackingObj.localScale = new Vector3(.5f, .5f, .5f);
			TrackingObj.SetParent(VRControllerManager.instance.transform, true);
			TrackingObj.name = DeviceNode.ToString();
		}

		[HideInInspector]
		public bool HideDeviceNode = false;
		public virtual void HandleUpdateDevice()
		{
			//get position
			InputDevice.TryGetFeatureValue(UnityEngine.XR.CommonUsages.devicePosition, out DevicePosition);
			//get rotation
			InputDevice.TryGetFeatureValue(UnityEngine.XR.CommonUsages.deviceRotation, out DeviceRotation);

			if (TrackingObj != null)
			{
				TrackingObj.position = DevicePosition;
				TrackingObj.rotation = DeviceRotation;
			}
		}
		public virtual void SetupVRDevice()
		{
			var InputDevices = new List<UnityEngine.XR.InputDevice>();
			UnityEngine.XR.InputDevices.GetDevicesAtXRNode(DeviceNode, InputDevices);

			if (InputDevices.Count == 1)
			{
				InputDevice = InputDevices[0];
				LogStatus();
			}
			else if (InputDevices.Count > 1)
			{
				Debug.LogError($"Found more than one {UnityEngine.XR.XRNode.LeftHand}");
			}
		}


		protected IEnumerator RunNextFrame(Action action)
		{
			yield return null;
			action();
		}

		public static string LogStatus(UnityEngine.XR.InputDevice device)
		{
			var result = $"Device ['{device.name}'] with characteristics ['{ device.characteristics.ToString()}']";
			Debug.Log(result);
			return result;
		}

		public virtual string LogStatus()
		{
			return LogStatus(InputDevice);
		}
	}

	[Serializable]
	public class VRController : VRDevice
	{
		[ReadOnly] public Vector2 TrackPadVector;
		[ReadOnly] public bool TrackPadPressed;

		bool _TriggerPressedPrevious;
		bool _TriggerPressed;
		[ReadOnly]
		[ShowInInspector]
		public bool TriggerPressed
		{
			get { return _TriggerPressed; }
			set
			{
				_TriggerPressedPrevious = _TriggerPressed;
				_TriggerPressed = value;
			}
		}

		[ReadOnly]

		public bool TriggerButtonUp;

		[ReadOnly] public bool TriggerButtonDown = false; //instant bool

		public override void HandleUpdateDevice()
		{
			base.HandleUpdateDevice();

			bool isPressed;
			InputDevice.TryGetFeatureValue(UnityEngine.XR.CommonUsages.triggerButton, out isPressed);
			TriggerPressed = isPressed;

			if (TriggerPressed)
			{
				if (!_TriggerPressedPrevious)
				{
					TriggerButtonDown = true;
					VRControllerManager.instance?.StartCoroutine(RunNextFrame(() => TriggerButtonDown = false));
				}
			}
			else
			{
				if (_TriggerPressedPrevious)
				{
					TriggerButtonUp = true;
					VRControllerManager.instance?.StartCoroutine(RunNextFrame(() => TriggerButtonUp = false));
				}
			}
			InputDevice.TryGetFeatureValue(UnityEngine.XR.CommonUsages.devicePosition, out DevicePosition);
			InputDevice.TryGetFeatureValue(UnityEngine.XR.CommonUsages.deviceRotation, out DeviceRotation);
			InputDevice.TryGetFeatureValue(UnityEngine.XR.CommonUsages.primary2DAxis, out TrackPadVector);
			InputDevice.TryGetFeatureValue(UnityEngine.XR.CommonUsages.primary2DAxisClick, out TrackPadPressed);
		}

		public override void SetupVRDevice()
		{
			if (DeviceNode != UnityEngine.XR.XRNode.LeftHand && DeviceNode != UnityEngine.XR.XRNode.RightHand)
			{
				Debug.LogError($"{DeviceNode} is not a valid node for Controller");
				return;
			}
			var Characteristics = DeviceNode == UnityEngine.XR.XRNode.LeftHand ? UnityEngine.XR.InputDeviceCharacteristics.Left : UnityEngine.XR.InputDeviceCharacteristics.Right;
			var InputDevices = new List<UnityEngine.XR.InputDevice>();
			var DesiredControllerCharacteristics = Characteristics | UnityEngine.XR.InputDeviceCharacteristics.Controller; //| UnityEngine.XR.InputDeviceCharacteristics.HeldInHand;

			UnityEngine.XR.InputDevices.GetDevicesWithCharacteristics(DesiredControllerCharacteristics, InputDevices);

			if (InputDevices.Count == 1)
			{
				InputDevice = InputDevices[0];
				LogStatus();
			}
			else if (InputDevices.Count > 1)
			{
				Debug.LogError($"Found more than one {UnityEngine.XR.XRNode.LeftHand}");
			}
		}
	}
	#endregion


	//public List<GameObject> GetObjectsUnderPointer()
	//{
	//	if (objectsUnderPointer == null) objectsUnderPointer = new List<GameObject>();
	//	return objectsUnderPointer;
	//}

}




