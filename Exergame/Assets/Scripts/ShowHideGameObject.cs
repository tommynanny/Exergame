﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowHideGameObject : MonoBehaviour
{
	public GameObject Target;
	public bool HideOnStart = false;

	private void Start()
	{
		if (HideOnStart)
		{
			Target.SetActive(false);
		}
	}

	public void ToggleVisibility()
	{
		Target.SetActive(!Target.activeSelf);
	}
}
