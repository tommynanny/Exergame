﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Realtime;
using Photon.Pun;
using Sirenix.OdinInspector;

public class ConnectManager : AdvancedPunCallbacks<ConnectManager>
{
	private void Awake()
	{
		DontDestroyOnLoad(this.gameObject);
	}

	private List<RoomInfo> _Rooms = new List<RoomInfo>();

	[ShowInInspector, ReadOnly]
	public List<RoomInfo> Rooms
	{
		get => _Rooms;
		set 
		{
			if (value != Rooms)
			{
				_Rooms = value;
			}
		}
	}

	public override void OnRoomListUpdate(List<RoomInfo> roomList)
	{
		Debug.Log(roomList.Count);
		Rooms = roomList;
	}
}
