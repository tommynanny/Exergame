﻿/* Copyright(c) Tim Watts, Box of Clicks - All Rights Reserved */

using System.Collections.Generic;
using System.Linq;
using System;
#if UNITY_EDITOR
using BOC.BTagged.Shared;
#endif
#if UNITY_ENTITIES
using Unity.Entities;
#endif
using Unity.Collections;
using UnityEngine;

namespace BOC.BTagged
{
    public static class BTagged
    {
        [NonSerialized]
        static public Dictionary<BHash128, List<GameObject>> AllTaggedInOpenScenes = new Dictionary<BHash128, List<GameObject>>();

        static public bool IgnoreEditorChecks = false;

#if UNITY_EDITOR
        private static void CheckForMultipleResults(IEnumerable<UnityEngine.Object> results, IEnumerable<TagHashWithRules> tags)
        {
            if (results != null && results.Count() > 1)
            {
                string allTags = "";// string.Join(",", tags.Select( x => { return (x.rule == InclusionRule.MustExclude ? "<Color=red>-" : "<Color=yellow>+") + x.tag.name + "</Color>"; }) );
                allTags += "Have Any: " + string.Join(",", tags.Where(x => x.rule == InclusionRule.Any).Select(y => "<b>" + FindTagNameForHash(y.hash) + "</b>"));
                allTags += "\nMust Include: " + string.Join(",", tags.Where(x => x.rule == InclusionRule.MustInclude).Select(y => "<b>" + FindTagNameForHash(y.hash) + "</b>"));
                allTags += "\nMust Exclude: " + string.Join(",", tags.Where(x => x.rule == InclusionRule.MustExclude).Select(y => "<b>" + FindTagNameForHash(y.hash) + "</b>"));
                string allMatches = string.Join(", ", results);
                Debug.LogWarning("More than one GameObject matches the following query\n" + allTags + "\nThe first object will be returned but may be non-deterministic. Use .AnyIsFine() with a query when getting the same object back every time is unimportant.\n" + allMatches);
            }
        }

        internal static string FindTagNameForHash(BHash128 hash)
        {
            var t = FindTagForHash(hash);
            return t == default ? "untagged" : t.name;
        }
        internal static Tag FindTagForHash(BHash128 hash)
        {
            Tag result = default;
            string[] groupSOGuids = UnityEditor.AssetDatabase.FindAssets("t:" + typeof(TagGroup));
            for (int i = 0; i < groupSOGuids.Length; ++i)
            {
                object[] tagAssets = UnityEditor.AssetDatabase.LoadAllAssetRepresentationsAtPath(UnityEditor.AssetDatabase.GUIDToAssetPath(groupSOGuids[i]));
                if (tagAssets == null) continue;
                for (int t = 0; t < tagAssets.Length; ++t)
                {
                    if (tagAssets[t] != null && tagAssets[t] is Tag && (tagAssets[t] as Tag).Hash == hash) { result = tagAssets[t] as Tag; break; }
                }
                if (result != default) break;
            }
            if (result == default)
            {
                string[] individualSOGuids = UnityEditor.AssetDatabase.FindAssets("t:" + typeof(Tag));
                for (int i = 0; i < individualSOGuids.Length; ++i)
                {
                    Tag individualAsset = UnityEditor.AssetDatabase.LoadMainAssetAtPath(UnityEditor.AssetDatabase.GUIDToAssetPath(individualSOGuids[i])) as Tag;
                    if (individualAsset != null && individualAsset.Hash == hash) { result = individualAsset; break; }
                }
            }
            return result;
        }
#endif

        #region BTagged Queries

        internal enum InclusionRule
        {
            Any,
            MustInclude,
            MustExclude
        }
        public enum Search
        {
            TargetOnly,
            TargetAndChildren,
            ChildrenOnly
        }
        public struct TagHashWithRules
        {
            internal BHash128 hash;
            internal InclusionRule rule;
            internal Search searchOption;
        }

        internal static IEnumerable<TagHashWithRules> GetTagHashWithRules(Tag tag, InclusionRule rule = InclusionRule.Any, Search searchOption = Search.TargetAndChildren)
        {
            var tmp = new TagHashWithRules[1];
            tmp[0] = new TagHashWithRules() { hash = GetHash(tag), rule = rule, searchOption = searchOption };
            return tmp;
        }
        internal static IEnumerable<TagHashWithRules> GetTagHashesWithRules(IEnumerable<Tag> tags, InclusionRule rule = InclusionRule.Any, Search searchOption = Search.TargetAndChildren)
        {
            var tmp = new TagHashWithRules[tags.Count()];
            var e = tags.GetEnumerator();
            int i = 0;
            while (e.MoveNext())
            {
                tmp[i] = new TagHashWithRules() { hash = GetHash(e.Current), rule = rule, searchOption = searchOption };
                i++;
            }
            return tmp;
        }

#if UNITY_ENTITIES
        internal static NativeArray<TagHashWithRules> GetTagHashWithRules(BHash128 hash, InclusionRule rule = InclusionRule.Any, Search searchOption = Search.TargetAndChildren)
        {
            var tmp = new NativeArray<TagHashWithRules>(1, Allocator.Temp);
            tmp[0] = new TagHashWithRules() { hash = hash, rule = rule, searchOption = searchOption };
            return tmp;
        }
        internal static NativeArray<TagHashWithRules> GetTagHashesWithRules(NativeArray<BHash128> tags, InclusionRule rule = InclusionRule.Any, Search searchOption = Search.TargetAndChildren)
        {
            var tmp = new NativeArray<TagHashWithRules>(tags.Count(), Allocator.Temp);
            var e = tags.GetEnumerator();
            int i = 0;
            while (e.MoveNext())
            {
                tmp[i] = new TagHashWithRules() { hash = e.Current, rule = rule, searchOption = searchOption };
                i++;
            }
            return tmp;
        }
#endif

        public class BTaggedQueryGO
        {
#if UNITY_EDITOR
            internal bool DisableEditorChecks;
#endif
            internal List<TagHashWithRules> TagWithSettings;
            internal List<int> GroupCounts;
            internal List<GameObject> InitialTargets;

            public BTaggedQueryGO()
            {
                TagWithSettings = new List<TagHashWithRules>();
                GroupCounts = new List<int>();
                InitialTargets = new List<GameObject>();
            }
            internal void Reset()
            {
                TagWithSettings.Clear();
                GroupCounts.Clear();
                InitialTargets.Clear();
            }
        }

        public static GameObject GetFirstGameObject(this BTaggedQueryGO q)
        {
            return GetGameObjects(q, false).FirstOrDefault();
        }
        public static List<GameObject> GetGameObjects(this BTaggedQueryGO q, bool findAll = true)
        {
            var results = new List<GameObject>();
            GetGameObjects(q, ref results, findAll);
            return results;
        }
        public static bool GetGameObjects(this BTaggedQueryGO q, ref List<GameObject> results, bool findAll = true)
        {
#if UNITY_EDITOR
            if (q.DisableEditorChecks) IgnoreEditorChecks = true;
#endif
            int groupStartIdx = 0;
            List<GameObject> searchResults = new List<GameObject>(q.InitialTargets);
            for (int g = 0; g < q.GroupCounts.Count; ++g)
            {
                if (searchResults.Count > 0)
                {
                    var tmp = new List<GameObject>(searchResults);
                    searchResults.Clear();
                    for (int r = 0; r < tmp.Count; ++r)
                    {
                        searchResults.AddRange(GetTaggedGameObjectsWithTags(q.TagWithSettings.GetRange(groupStartIdx, q.GroupCounts[g]), tmp[r], findAll));
                    }
                }
                else if (g == 0)
                {
                    searchResults = GetTaggedGameObjectsWithTags(q.TagWithSettings.GetRange(groupStartIdx, q.GroupCounts[g]), null, findAll);
                }
                else
                {
                    return false;
                }
                groupStartIdx += q.GroupCounts[g];
            }

#if UNITY_EDITOR
            IgnoreEditorChecks = false;
#endif
            var unique = searchResults.Distinct();
            results.AddRange(unique);
            return (unique.Count() > 0);
        }

        public static C GetFirstComponent<C>(this BTaggedQueryGO q) where C : Component
        {
            return GetComponents<C>(q, false).FirstOrDefault();
        }
        public static List<C> GetComponents<C>(this BTaggedQueryGO q, bool findAll = true) where C : Component
        {
            var results = new List<C>();
            GetComponents(q, ref results, findAll);
            return results;
        }
        public static bool GetComponents<C>(this BTaggedQueryGO q, ref List<C> results, bool findAll = true) where C : Component
        {
#if UNITY_EDITOR
            if (q.DisableEditorChecks) IgnoreEditorChecks = true;
#endif
            // We only want to search for components after finding the GameObjects
            var goResults = GetGameObjects(q, findAll);
            if (goResults != null && goResults.Count > 0)
            {
                for (int r = 0; r < goResults.Count; ++r) results.AddRange(goResults[r].GetComponents<C>());
            }
            results = results.Distinct().ToList();

#if UNITY_EDITOR
            if (!BTaggedSharedSettings.DisableEditorSafetyChecks && !IgnoreEditorChecks && !findAll) CheckForMultipleResults(results, q.TagWithSettings);
            IgnoreEditorChecks = false;
#endif
            return (results.Count() > 0);
        }

        public static BTaggedQueryGO For(GameObject gameObject)
        {
            BTaggedQueryGO q = new BTaggedQueryGO();
            q.InitialTargets.Add(gameObject);
            return q;
        }
        public static BTaggedQueryGO For(IEnumerable<GameObject> gameObjects)
        {
            BTaggedQueryGO q = new BTaggedQueryGO();
            q.InitialTargets.AddRange(gameObjects);
            return q;
        }

        public static BTaggedQueryGO Find(Tag tag)
        {
            BTaggedQueryGO q = new BTaggedQueryGO();
            q.TagWithSettings.Add(new TagHashWithRules() { hash = tag, rule = InclusionRule.Any, searchOption = Search.TargetOnly });
            q.GroupCounts.Add(1);
            return q;
        }
        public static BTaggedQueryGO FindWithAny(IEnumerable<Tag> tags)
        {
            BTaggedQueryGO q = new BTaggedQueryGO();
            var e = tags.GetEnumerator();
            int count = q.TagWithSettings.Count;
            while (e.MoveNext())
            {
                if (e.Current != null && !e.Current.IsDefault)
                    q.TagWithSettings.Add(new TagHashWithRules() { hash = e.Current, rule = InclusionRule.Any, searchOption = Search.TargetOnly });
            }
            q.GroupCounts.Add(q.TagWithSettings.Count - count);
            return q;
        }
        public static BTaggedQueryGO FindWithAll(IEnumerable<Tag> tags)
        {
            BTaggedQueryGO q = new BTaggedQueryGO();
            var e = tags.GetEnumerator();
            int count = q.TagWithSettings.Count;
            while (e.MoveNext())
            {
                if (e.Current != null && !e.Current.IsDefault)
                    q.TagWithSettings.Add(new TagHashWithRules() { hash = e.Current, rule = InclusionRule.MustInclude, searchOption = Search.TargetOnly });
            }
            q.GroupCounts.Add(q.TagWithSettings.Count - count);
            return q;
        }

        public static BTaggedQueryGO AnyIsFine(this BTaggedQueryGO q)
        {
#if UNITY_EDITOR
            q.DisableEditorChecks = true;
#endif
            return q;
        }

        public static BTaggedQueryGO ChildrenWith(this BTaggedQueryGO q, Tag tag, Search search = Search.ChildrenOnly) => With(q, tag, search);
        public static BTaggedQueryGO With(this BTaggedQueryGO q, Tag tag, Search search = Search.TargetOnly)
        {
            if (tag == null || tag.IsDefault) return q;
            q.TagWithSettings.Add(new TagHashWithRules() { hash = GetHash(tag), rule = InclusionRule.MustInclude, searchOption = search });
            q.GroupCounts.Add(1);
            return q;
        }
        public static BTaggedQueryGO ChildrenWithAny(this BTaggedQueryGO q, IEnumerable<Tag> tags, Search search = Search.ChildrenOnly) => WithAny(q, tags, search);
        public static BTaggedQueryGO WithAny(this BTaggedQueryGO q, IEnumerable<Tag> tags, Search search = Search.TargetOnly)
        {
            var e = tags.GetEnumerator();
            int count = q.TagWithSettings.Count;
            while (e.MoveNext())
            {
                if (e.Current != null && !e.Current.IsDefault)
                    q.TagWithSettings.Add(new TagHashWithRules() { hash = GetHash(e.Current), rule = InclusionRule.Any, searchOption = search });
            }
            q.GroupCounts.Add(q.TagWithSettings.Count - count);
            return q;
        }

        public static BTaggedQueryGO ChildrenWithAll(this BTaggedQueryGO q, IEnumerable<Tag> tags, Search search = Search.ChildrenOnly) => WithAll(q, tags, search);
        public static BTaggedQueryGO WithAll(this BTaggedQueryGO q, IEnumerable<Tag> tags, Search search = Search.TargetOnly)
        {
            var e = tags.GetEnumerator();
            int count = q.TagWithSettings.Count;
            while (e.MoveNext())
            {
                if (e.Current != null && !e.Current.IsDefault)
                    q.TagWithSettings.Add(new TagHashWithRules() { hash = GetHash(e.Current), rule = InclusionRule.MustInclude, searchOption = search });
            }
            q.GroupCounts.Add(q.TagWithSettings.Count - count);
            return q;
        }

        public static BTaggedQueryGO Without(this BTaggedQueryGO q, Tag tag)
        {
            if (tag == null || tag.IsDefault) return q;
            q.TagWithSettings.Add(new TagHashWithRules() { hash = GetHash(tag), rule = InclusionRule.MustExclude, searchOption = Search.TargetOnly });
            if (q.GroupCounts.Count < 1)
            {
                q.GroupCounts.Add(1);
            }
            else
            {
                q.GroupCounts[q.GroupCounts.Count - 1] = q.GroupCounts[q.GroupCounts.Count - 1] + 1;
            }
            return q;
        }

        public static BTaggedQueryGO Without(this BTaggedQueryGO q, IEnumerable<Tag> tags)
        {
            var e = tags.GetEnumerator();
            int count = q.TagWithSettings.Count;
            while (e.MoveNext())
            {
                if (e.Current != null && !e.Current.IsDefault)
                    q.TagWithSettings.Add(new TagHashWithRules() { hash = GetHash(e.Current), rule = InclusionRule.MustExclude, searchOption = Search.TargetOnly });
            }
            count = (q.TagWithSettings.Count - count);
            if (q.GroupCounts.Count < 1)
            {
                q.GroupCounts.Add(count);
            }
            else
            {
                q.GroupCounts[q.GroupCounts.Count - 1] += count;
            }
            return q;
        }

        #endregion

        #region BTagged Entity Queries
#if UNITY_ENTITIES

        public class BTaggedQueryEntity : IDisposable
        {
#if UNITY_EDITOR
            internal bool DisableEditorChecks;
#endif
            internal NativeList<TagHashWithRules> TagWithSettings;
            internal NativeList<int> GroupCounts;
            internal NativeList<Entity> InitialTargets;

            public BTaggedQueryEntity()
            {
                TagWithSettings = new NativeList<TagHashWithRules>(Allocator.Persistent);
                GroupCounts = new NativeList<int>(Allocator.Persistent);
                InitialTargets = new NativeList<Entity>(Allocator.Persistent);
            }
            public void Dispose()
            {
                if (TagWithSettings.IsCreated) TagWithSettings.Dispose();
                if (GroupCounts.IsCreated) GroupCounts.Dispose();
                if (InitialTargets.IsCreated) InitialTargets.Dispose();
            }

            internal void Reset()
            {
                TagWithSettings.Clear();
                GroupCounts.Clear();
                InitialTargets.Clear();
            }
        }
        public static BTaggedQueryEntity AnyIsFine(this BTaggedQueryEntity q)
        {
#if UNITY_EDITOR
            q.DisableEditorChecks = true;
#endif
            return q;
        }

        public static BTaggedQueryEntity For(Entity entity)
        {
            BTaggedQueryEntity q = new BTaggedQueryEntity();
            q.InitialTargets.Add(entity);
            return q;
        }
        public static BTaggedQueryEntity For(NativeArray<Entity> entities)
        {
            BTaggedQueryEntity q = new BTaggedQueryEntity();
            q.InitialTargets.AddRange(entities);
            return q;
        }
        public static BTaggedQueryEntity FindEntities(Tag tag)
        {
            BTaggedQueryEntity q = new BTaggedQueryEntity();
            q.TagWithSettings.Add(new TagHashWithRules() { hash = tag, rule = InclusionRule.Any, searchOption = Search.TargetOnly });
            q.GroupCounts.Add(1);
            return q;
        }
        public static BTaggedQueryEntity FindAnyEntities(NativeArray<BHash128> tags)
        {
            BTaggedQueryEntity q = new BTaggedQueryEntity();
            var e = tags.GetEnumerator();
            int count = q.TagWithSettings.Length;
            while (e.MoveNext())
            {
                if (e.Current.IsValid)
                    q.TagWithSettings.Add(new TagHashWithRules() { hash = e.Current, rule = InclusionRule.Any, searchOption = Search.TargetOnly });
            }
            q.GroupCounts.Add(q.TagWithSettings.Length - count);
            return q;
        }
        public static BTaggedQueryEntity FindAnyEntities(IEnumerable<Tag> tags)
        {
            BTaggedQueryEntity q = new BTaggedQueryEntity();
            var e = tags.GetEnumerator();
            int count = q.TagWithSettings.Length;
            while (e.MoveNext())
            {
                if (e.Current != null && !e.Current.IsDefault)
                    q.TagWithSettings.Add(new TagHashWithRules() { hash = e.Current, rule = InclusionRule.Any, searchOption = Search.TargetOnly });
            }
            q.GroupCounts.Add(q.TagWithSettings.Length - count);
            return q;
        }
        public static BTaggedQueryEntity FindAllEntities(NativeArray<BHash128> tags)
        {
            BTaggedQueryEntity q = new BTaggedQueryEntity();
            var e = tags.GetEnumerator();
            int count = q.TagWithSettings.Length;
            while (e.MoveNext())
            {
                if (e.Current.IsValid)
                    q.TagWithSettings.Add(new TagHashWithRules() { hash = e.Current, rule = InclusionRule.MustInclude, searchOption = Search.TargetOnly });
            }
            q.GroupCounts.Add(q.TagWithSettings.Length - count);
            return q;
        }
        public static BTaggedQueryEntity FindAllEntities(IEnumerable<Tag> tags)
        {
            BTaggedQueryEntity q = new BTaggedQueryEntity();
            var e = tags.GetEnumerator();
            int count = q.TagWithSettings.Length;
            while (e.MoveNext())
            {
                if (e.Current != null && !e.Current.IsDefault)
                    q.TagWithSettings.Add(new TagHashWithRules() { hash = e.Current, rule = InclusionRule.MustInclude, searchOption = Search.TargetOnly });
            }
            q.GroupCounts.Add(q.TagWithSettings.Length - count);
            return q;
        }

        public static BTaggedQueryEntity ChildrenWith(this BTaggedQueryEntity q, Tag tag, Search search = Search.ChildrenOnly) => With(q, GetHash(tag), search);
        public static BTaggedQueryEntity ChildrenWith(this BTaggedQueryEntity q, BHash128 tagHash, Search search = Search.ChildrenOnly) => With(q, tagHash, search);
        public static BTaggedQueryEntity With(this BTaggedQueryEntity q, Tag tag, Search search = Search.TargetOnly) => With(q, GetHash(tag), search);
        public static BTaggedQueryEntity With(this BTaggedQueryEntity q, BHash128 tagHash, Search search = Search.TargetOnly)
        {
            if (!tagHash.IsValid) return q;
            q.TagWithSettings.Add(new TagHashWithRules() { hash = tagHash, rule = InclusionRule.MustInclude, searchOption = search });
            q.GroupCounts.Add(1);
            return q;
        }
        public static BTaggedQueryEntity ChildrenWithAny(this BTaggedQueryEntity q, IEnumerable<Tag> tags, Search search = Search.ChildrenOnly) => WithAny(q, tags, search);
        public static BTaggedQueryEntity WithAny(this BTaggedQueryEntity q, IEnumerable<Tag> tags, Search search = Search.TargetOnly)
        {
            var e = tags.GetEnumerator();
            int count = q.TagWithSettings.Length;
            while (e.MoveNext())
            {
                if (e.Current != null && !e.Current.IsDefault)
                    q.TagWithSettings.Add(new TagHashWithRules() { hash = GetHash(e.Current), rule = InclusionRule.Any, searchOption = search });
            }
            q.GroupCounts.Add(q.TagWithSettings.Length - count);
            return q;
        }
        public static BTaggedQueryEntity ChildrenWithAny(this BTaggedQueryEntity q, NativeArray<BHash128> hashes, Search search = Search.ChildrenOnly) => WithAny(q, hashes, search);
        public static BTaggedQueryEntity WithAny(this BTaggedQueryEntity q, NativeArray<BHash128> hashes, Search search = Search.TargetOnly)
        {
            var e = hashes.GetEnumerator();
            int count = q.TagWithSettings.Length;
            while (e.MoveNext())
            {
                if (e.Current.IsValid)
                    q.TagWithSettings.Add(new TagHashWithRules() { hash = e.Current, rule = InclusionRule.Any, searchOption = search });
            }
            q.GroupCounts.Add(q.TagWithSettings.Length - count);
            return q;
        }
        public static BTaggedQueryEntity ChildrenWithAll(this BTaggedQueryEntity q, IEnumerable<Tag> tags, Search search = Search.ChildrenOnly) => WithAll(q, tags, search);
        public static BTaggedQueryEntity WithAll(this BTaggedQueryEntity q, IEnumerable<Tag> tags, Search search = Search.TargetOnly)
        {
            var e = tags.GetEnumerator();
            int count = q.TagWithSettings.Length;
            while (e.MoveNext())
            {
                if (e.Current != null && !e.Current.IsDefault)
                    q.TagWithSettings.Add(new TagHashWithRules() { hash = GetHash(e.Current), rule = InclusionRule.MustInclude, searchOption = search });
            }
            q.GroupCounts.Add(q.TagWithSettings.Length - count);
            return q;
        }
        public static BTaggedQueryEntity ChildrenWithAll(this BTaggedQueryEntity q, NativeArray<BHash128> hashes, Search search = Search.ChildrenOnly) => WithAll(q, hashes, search);
        public static BTaggedQueryEntity WithAll(this BTaggedQueryEntity q, NativeArray<BHash128> hashes, Search search = Search.TargetOnly)
        {
            var e = hashes.GetEnumerator();
            int count = q.TagWithSettings.Length;
            while (e.MoveNext())
            {
                if (e.Current.IsValid)
                    q.TagWithSettings.Add(new TagHashWithRules() { hash = e.Current, rule = InclusionRule.MustInclude, searchOption = search });
            }
            q.GroupCounts.Add(q.TagWithSettings.Length - count);
            return q;
        }
        public static BTaggedQueryEntity Without(this BTaggedQueryEntity q, Tag tag)
        {
            if (tag == null || tag.IsDefault) return q;
            q.TagWithSettings.Add(new TagHashWithRules() { hash = GetHash(tag), rule = InclusionRule.MustExclude, searchOption = Search.TargetOnly });
            if (q.GroupCounts.Length < 1)
            {
                q.GroupCounts.Add(1);
            }
            else
            {
                q.GroupCounts[q.GroupCounts.Length - 1]++;
            }
            return q;
        }
        public static BTaggedQueryEntity Without(this BTaggedQueryEntity q, IEnumerable<Tag> tags)
        {
            var e = tags.GetEnumerator();
            int count = q.TagWithSettings.Length;
            while (e.MoveNext())
            {
                if (e.Current != null && !e.Current.IsDefault)
                    q.TagWithSettings.Add(new TagHashWithRules() { hash = GetHash(e.Current), rule = InclusionRule.MustExclude, searchOption = Search.TargetOnly });
            }
            count = q.TagWithSettings.Length - count;
            if (q.GroupCounts.Length < 1)
            {
                q.GroupCounts.Add(count);
            }
            else
            {
                q.GroupCounts[q.GroupCounts.Length - 1] += count;
            }
            return q;
        }
        public static BTaggedQueryEntity Without(this BTaggedQueryEntity q, NativeArray<BHash128> hashes)
        {
            var e = hashes.GetEnumerator();
            int count = q.TagWithSettings.Length;
            while (e.MoveNext())
            {
                if (e.Current.IsValid)
                    q.TagWithSettings.Add(new TagHashWithRules() { hash = e.Current, rule = InclusionRule.MustExclude, searchOption = Search.TargetOnly });
            }
            count = q.TagWithSettings.Length - count;
            if (q.GroupCounts.Length < 1)
            {
                q.GroupCounts.Add(count);
            }
            else
            {
                q.GroupCounts[q.GroupCounts.Length - 1] += count;
            }
            return q;
        }


        public static Entity GetFirstEntity(this BTaggedQueryEntity q)
        {
            NativeList<Entity> tmp = new NativeList<Entity>(Allocator.Temp);
            bool found = GetEntities(q, ref tmp, false);
            q.Dispose();
            return found ? tmp[0] : default;
        }
        public static NativeArray<Entity> GetEntities(this BTaggedQueryEntity q)
        {
            NativeList<Entity> tmp = new NativeList<Entity>(Allocator.Temp);
            GetEntities(q, ref tmp, true);
            q.Dispose();
            return tmp;
        }
        public static bool GetEntities(this BTaggedQueryEntity q, ref NativeList<Entity> results, bool findAll = true)
        {
#if UNITY_EDITOR
            if (q.DisableEditorChecks) IgnoreEditorChecks = true;
#endif
            int groupStartIdx = 0;
            NativeList<Entity> searchResults = new NativeList<Entity>(Allocator.Temp);
            searchResults.AddRange(q.InitialTargets);
            for (int g = 0; g < q.GroupCounts.Length; ++g)
            {
                if (searchResults.Length > 0)
                {
                    var tmp = new NativeList<Entity>(Allocator.Temp); ;
                    tmp.AddRange(searchResults);
                    searchResults.Clear();
                    for (int r = 0; r < tmp.Length; ++r)
                    {
                        var subArray = q.TagWithSettings.AsArray().GetSubArray(groupStartIdx, q.GroupCounts[g]);
                        GetEntitiesWithTags(subArray, tmp[r], ref searchResults, findAll);
                    }
                }
                else if (g == 0)
                {
                    var subArray = q.TagWithSettings.AsArray().GetSubArray(groupStartIdx, q.GroupCounts[g]);
                    GetEntitiesWithTags(subArray, default, ref searchResults, findAll);
                }
                else
                {
                    return false;
                }
                groupStartIdx += q.GroupCounts[g];
            }

#if UNITY_EDITOR
            IgnoreEditorChecks = false;
#endif
            //var unique = searchResults.Distinct().ToArray();
            results.AddRange(searchResults);
            q.Dispose();
            //searchResults.Dispose();
            return (results.Length > 0);
        }
#endif


        #endregion

        #region BTagged First GameObject Queries

        /// <summary>
        /// Returns the <b>first</b> GameObject in the scene matching the passed Tag.
        /// Do note that if multiple GameObjects in the scene match this Tag, you should not rely on the result being deterministic.
        /// </summary>
        /// <returns>Null if no matching GameObject found</returns>
        public static GameObject GetGameObjectForTag(this Tag tag) =>
            GetTaggedGameObjectsWithTags(GetTagHashWithRules(tag), null, false).FirstOrDefault();

        /// <summary>
        /// Returns the <b>first</b> GameObject with a matching tag that is a child of the passed gameObject or the gameObject itself.
        /// Do note that if multiple GameObjects in the scene match this Tag, you should not rely on the result being deterministic.
        /// </summary>
        /// <returns>Null if no matching GameObject found</returns>
        public static GameObject GetGameObjectForTag(this GameObject gameObject, Tag tag) =>
            GetTaggedGameObjectsWithTags(GetTagHashWithRules(tag), gameObject, false).FirstOrDefault();

        /// <summary>
        /// Returns the <b>first</b> GameObject in the scene matching <b>all</b> Tags passed in.
        /// Do note that if multiple GameObjects in the scene match this Tag, you should not rely on the result being deterministic.
        /// </summary>
        /// <returns>Null if no matching GameObject found</returns>
        public static GameObject GetGameObjectWithAllTags(this IEnumerable<Tag> tags) =>
            GetTaggedGameObjectsWithTags(GetTagHashesWithRules(tags), null, false).FirstOrDefault();

        /// <summary>
        /// Returns the <b>first</b> GameObject in the scene matching <b>all</b> Tags passed in
        /// that is also a child of the passed gameObject or the gameObject itself.
        /// Do note that if multiple GameObjects in the scene match this Tag, you should not rely on the result being deterministic.
        /// </summary>
        /// <returns>Null if no matching GameObject found</returns>
        public static GameObject GetGameObjectWithAllTags(this GameObject gameObject, IEnumerable<Tag> tags) =>
            GetTaggedGameObjectsWithTags(GetTagHashesWithRules(tags), gameObject, false).FirstOrDefault();

        /// <summary>
        /// Returns the <b>first</b> GameObject in the scene matching <b>any</b> Tag in the passed tags.
        /// Do note that if multiple GameObjects in the scene match this Tag, you should not rely on the result being deterministic.
        /// </summary>
        /// <returns>Null if no matching GameObject found</returns>
        public static GameObject GetGameObjectWithAnyTags(this IEnumerable<Tag> tags) =>
            GetTaggedGameObjectsWithTags(GetTagHashesWithRules(tags), null, false).FirstOrDefault();

        /// <summary>
        /// Returns the <b>first</b> GameObject in the scene matching <b>any</b> Tag in the passed tags.
        /// Do note that if multiple GameObjects in the scene match this Tag, you should not rely on the result being deterministic.
        /// </summary>
        /// <returns>Null if no matching GameObject found</returns>
        public static GameObject GetGameObjectWithAnyTags(this GameObject gameObject, IEnumerable<Tag> tags) =>
            GetTaggedGameObjectsWithTags(GetTagHashesWithRules(tags), gameObject, false).FirstOrDefault();

        #endregion

        #region BTagged First Component Queries

        /// <summary>
        /// Returns the <b>first</b> Component in the scene matching the passed Tag.
        /// Do note that if multiple Component in the scene match this Tag, you should not rely on the result being deterministic.
        /// </summary>
        /// <returns>Null if no matching Component found</returns>
        public static C GetComponentForTag<C>(this Tag tag) where C : Component =>
            GetTaggedComponentsWith<C>(GetTagHashWithRules(tag), null, false).FirstOrDefault();

        /// <summary>
        /// Returns the <b>first</b> Component with a matching tag that is a child of the passed gameObject or the gameObject itself.
        /// Do note that if multiple Components in the scene match this Tag, you should not rely on the result being deterministic.
        /// </summary>
        /// <returns>Null if no matching Component found</returns>
        public static C GetComponentForTag<C>(this GameObject gameObject, Tag tag) where C : Component =>
            GetTaggedComponentsWith<C>(GetTagHashWithRules(tag), gameObject, false).FirstOrDefault();

        /// <summary>
        /// Returns the <b>first</b> Component in the scene matching <b>all</b> Tags passed in.
        /// Do note that if multiple Components in the scene match this Tag, you should not rely on the result being deterministic.
        /// </summary>
        /// <returns>Null if no matching Component found</returns>
        public static C GetComponentWithAllTags<C>(this IEnumerable<Tag> tags) where C : Component =>
            GetTaggedComponentsWith<C>(GetTagHashesWithRules(tags, InclusionRule.MustInclude), null, false).FirstOrDefault();

        /// <summary>
        /// Returns the <b>first</b> Component in the scene matching <b>all</b> Tags passed in
        /// that is also a child of the passed gameObject or the gameObject itself.
        /// Do note that if multiple Components in the scene match this Tag, you should not rely on the result being deterministic.
        /// </summary>
        /// <returns>Null if no matching Component found</returns>
        public static C GetComponentWithAllTags<C>(this GameObject gameObject, IEnumerable<Tag> tags) where C : Component =>
            GetTaggedComponentsWith<C>(GetTagHashesWithRules(tags, InclusionRule.MustInclude), gameObject, false).FirstOrDefault();

        /// <summary>
        /// Returns the <b>first</b> Component in the scene matching <b>any</b> Tag in the passed tags.
        /// Do note that if multiple Components in the scene match this Tag, you should not rely on the result being deterministic.
        /// </summary>
        /// <returns>Null if no matching Component found</returns>
        public static C GetComponentWithAnyTags<C>(this IEnumerable<Tag> tags) where C : Component =>
            GetTaggedComponentsWith<C>(GetTagHashesWithRules(tags), null, false).FirstOrDefault();

        /// <summary>
        /// Returns the <b>first</b> Component in the scene matching <b>any</b> Tag in the passed tags.
        /// Do note that if multiple Components in the scene match this Tag, you should not rely on the result being deterministic.
        /// </summary>
        /// <returns>Null if no matching Component found</returns>
        public static C GetComponentWithAnyTags<C>(this GameObject gameObject, IEnumerable<Tag> tags) where C : Component =>
            GetTaggedComponentsWith<C>(GetTagHashesWithRules(tags), gameObject, false).FirstOrDefault();

        #endregion

        #region BTagged List<GameObject> Queries

        /// <summary>
        /// Returns <b>all</b> GameObjects in the scene matching the provided Tag.
        /// </summary>
        public static List<GameObject> GetGameObjectsForTag(this Tag tag) =>
            GetTaggedGameObjectsWithTags(GetTagHashWithRules(tag), null, true);

        /// <summary>
        /// Returns <b>all</b> GameObjects in the scene matching the provided Tag
        /// that are also a child of the passed gameObject or the gameObject itself.
        /// </summary>
        public static List<GameObject> GetGameObjectsForTag(this GameObject gameObject, Tag tag) =>
            GetTaggedGameObjectsWithTags(GetTagHashWithRules(tag), gameObject, true);

        /// <summary>
        /// Returns <b>all</b> GameObjects in the scene matching <b>all</b> Tags passed in.
        /// </summary>
        public static List<GameObject> GetGameObjectsWithAllTags(this IEnumerable<Tag> tags) =>
            GetTaggedGameObjectsWithTags(GetTagHashesWithRules(tags, InclusionRule.MustInclude), null, true);

        /// <summary>
        /// Returns <b>all</b> GameObjects in the scene matching <b>all</b> Tags passed in
        /// that are also children of the passed gameObject or the gameObject itself.
        /// </summary>
        public static List<GameObject> GetGameObjectsWithAllTags(this GameObject gameObject, IEnumerable<Tag> tags) =>
            GetTaggedGameObjectsWithTags(GetTagHashesWithRules(tags, InclusionRule.MustInclude), gameObject, true);

        /// <summary>
        /// Returns <b>all</b> GameObjects in the scene matching <b>any</b> Tag in the passed tags.
        /// </summary>
        public static List<GameObject> GetGameObjectsWithAnyTags(this IEnumerable<Tag> tags) =>
            GetTaggedGameObjectsWithTags(GetTagHashesWithRules(tags), null, true);

        /// <summary>
        /// Returns <b>all</b> GameObjects in the scene matching <b>any</b> Tag in the passed tags
        /// that are also children of the passed gameObject or the gameObject itself.
        /// </summary>
        public static List<GameObject> GetGameObjectsWithAnyTags(this GameObject gameObject, IEnumerable<Tag> tags) =>
            GetTaggedGameObjectsWithTags(GetTagHashesWithRules(tags), gameObject, true);

        #endregion

        #region BTagged List<Component> Queries

        /// <summary>
        /// Returns <b>all</b> Components in the scene matching the provided Tag.
        /// </summary>
        /// <returns>Note: Every matching Component for a given GameObject, of which there can be multiple per GameObject, will be returned.</returns>
        public static List<C> GetComponentsForTag<C>(this Tag tag) where C : Component =>
            GetTaggedComponentsWith<C>(GetTagHashWithRules(tag), null, false);

        /// <summary>
        /// Returns <b>all</b> Components in the scene matching the provided Tag
        /// that are also a child of the passed gameObject or the gameObject itself.
        /// </summary>
        /// <returns>Note: Every matching Component for a given GameObject, of which there can be multiple per GameObject, will be returned.</returns>
        public static List<C> GetComponentsForTag<C>(this GameObject gameObject, Tag tag) where C : Component =>
            GetTaggedComponentsWith<C>(GetTagHashWithRules(tag), gameObject, false);

        /// <summary>
        /// Returns <b>all</b> Components in the scene matching <b>all</b> Tags passed in.
        /// </summary>
        /// <returns>Note: Every matching Component for a given GameObject, of which there can be multiple per GameObject, will be returned.</returns>
        public static List<C> GetComponentsWithAllTags<C>(this IEnumerable<Tag> tags) where C : Component =>
            GetTaggedComponentsWith<C>(GetTagHashesWithRules(tags, InclusionRule.MustInclude), null, true);

        /// <summary>
        /// Returns <b>all</b> Components in the scene matching <b>all</b> Tags passed in
        /// that are also children of the passed gameObject or the gameObject itself.
        /// </summary>
        /// <returns>Note: Every matching Component for a given GameObject, of which there can be multiple per GameObject, will be returned.</returns>
        public static List<C> GetComponentsWithAllTags<C>(this GameObject gameObject, IEnumerable<Tag> tags) where C : Component =>
            GetTaggedComponentsWith<C>(GetTagHashesWithRules(tags, InclusionRule.MustInclude), gameObject, true);

        /// <summary>
        /// Returns <b>all</b> Components in the scene matching <b>any</b> Tag in the passed tags.
        /// </summary>
        /// <returns>Note: Every matching Component for a given GameObject, of which there can be multiple per GameObject, will be returned.</returns>
        public static List<C> GetComponentsWithAnyTags<C>(this IEnumerable<Tag> tags) where C : Component =>
            GetTaggedComponentsWith<C>(GetTagHashesWithRules(tags), null, true);

        /// <summary>
        /// Returns <b>all</b> Components in the scene matching <b>any</b> Tag in the passed tags
        /// that are also children of the passed gameObject or the gameObject itself.
        /// </summary>
        /// <returns>Note: Every matching Component for a given GameObject, of which there can be multiple per GameObject, will be returned.</returns>
        public static List<C> GetComponentsWithAnyTags<C>(this GameObject gameObject, IEnumerable<Tag> tags) where C : Component =>
            GetTaggedComponentsWith<C>(GetTagHashesWithRules(tags), gameObject, true);

        #endregion



        internal static bool TransformMatchesTags(Transform transform, ref IEnumerable<TagHashWithRules> tags)
        {
            bool found = false;
            bool onlyDidExclusion = true;
            var tagComponents = transform.gameObject.GetComponents<TagGameObject>();
            BHash128[] tagsInComponents = new BHash128[tagComponents.Length];
            for (int c = 0; c < tagComponents.Length; ++c) tagsInComponents[c] = tagComponents[c].tag;
            foreach (var tagEntry in tags)
            {
                switch (tagEntry.rule)
                {
                    case InclusionRule.MustExclude:
                        if (tagsInComponents.Contains(tagEntry.hash)) return false;
                        break;
                    case InclusionRule.MustInclude:
                        if (!tagsInComponents.Contains(tagEntry.hash)) return false;
                        found = true;
                        onlyDidExclusion = false;
                        break;
                    case InclusionRule.Any:
                        if (!found && tagsInComponents.Contains(tagEntry.hash)) found = true;
                        onlyDidExclusion = false;
                        break;
                }
            }
            return (onlyDidExclusion || found);
        }

#if UNITY_ENTITIES
        private static void GetEntitiesWithTags(NativeArray<TagHashWithRules> tags, Entity entity, ref NativeList<Entity> foundEntities, bool findAll = true)
        {
            if (!tags.IsCreated || tags.Length < 1) return;
            InitTagSystem();
            tagSearchSystem.RespectHierarchy = tags[0].searchOption != Search.TargetOnly && entity != default;
            tagSearchSystem.FindAll = findAll;
            tagSearchSystem.ParentEntity = entity;
            tagSearchSystem.Results = foundEntities;
            tagSearchSystem.EntityFirstSearch = entity != default;
            tagSearchSystem.MatchingHashes = tags;
            tagSearchSystem.Update();
            return;
        }
#endif

        /// <summary>
        /// Returns all GameObjects matching the provided Tags.
        /// If a GameObject is provided, results will only be within and including that GameObject's search.
        /// </summary>
        /// <param name="tags"></param>
        /// <param name="gameObject"></param>
        /// <param name="matchAnyTag"></param>
        /// <returns></returns>
        internal static List<GameObject> GetTaggedGameObjectsWithTags(IEnumerable<TagHashWithRules> tags, GameObject gameObject = null, bool findAll = true)
        {
            var results = new List<GameObject>();
            var e = tags.GetEnumerator();
            //If no tags, early out
            if (!e.MoveNext()) return results;
            var firstTag = e.Current;

            if (gameObject != null)
            {
                var allTransforms = firstTag.searchOption == Search.TargetOnly ? gameObject.GetComponents<Transform>() : gameObject.GetComponentsInChildren<Transform>();
                for (int t = 0; t < allTransforms.Length; ++t)
                {
                    // If only children, skip past GameObject's own transform
                    if (firstTag.searchOption == Search.ChildrenOnly && allTransforms[t] == gameObject.transform) continue;

                    bool found = TransformMatchesTags(allTransforms[t], ref tags);
                    if (found && !results.Contains(allTransforms[t].gameObject))
                    {
                        results.Add(allTransforms[t].gameObject);
                        if (!findAll)
#if UNITY_EDITOR
                            if (IgnoreEditorChecks || BTaggedSharedSettings.DisableEditorSafetyChecks || results.Count > 2)
#endif
                                break;
                    }
                }
            }
            else
            {
                results = GetTaggedWith(TagGameObject.AllTaggedGOs, tags, findAll);
            }

#if UNITY_EDITOR
            if (!BTaggedSharedSettings.DisableEditorSafetyChecks && !IgnoreEditorChecks && !findAll) CheckForMultipleResults(results, tags);
#endif
            return results;
        }

        internal static List<C> GetTaggedComponentsWith<C>(IEnumerable<TagHashWithRules> tags, GameObject gameObject = null, bool findAll = true) where C : Component
        {
            var results = new List<C>();
            if (tags == null) return results;
            var e = tags.GetEnumerator();
            //If no tags, early out
            if (!e.MoveNext()) return results;
            var firstTag = e.Current;

            if (gameObject != null)
            {
                var allTransforms = firstTag.searchOption == Search.TargetOnly ? gameObject.GetComponents<Transform>() : gameObject.GetComponentsInChildren<Transform>();
                for (int t = 0; t < allTransforms.Length; ++t)
                {
                    // If only children, skip passed GameObject's own transform
                    if (firstTag.searchOption == Search.ChildrenOnly && allTransforms[t] == gameObject.transform) continue;

                    bool found = TransformMatchesTags(allTransforms[t], ref tags);
                    if (found)
                    {
                        results.AddRange(allTransforms[t].GetComponents<C>());
                        if (!findAll)
#if UNITY_EDITOR
                            if (IgnoreEditorChecks || BTaggedSharedSettings.DisableEditorSafetyChecks || results.Count > 2)
#endif
                                break;
                    }
                }
            }
            else
            {
                var cResults = GetTaggedWith(TagGameObject.AllTaggedGOs, tags, findAll);
                if (cResults != null)
                {
                    for (int r = 0; r < cResults.Count; ++r)
                    {
                        results.AddRange(cResults[r].GetComponents<C>());
                    }
                }
            }
#if UNITY_EDITOR
            if (!BTaggedSharedSettings.DisableEditorSafetyChecks && !IgnoreEditorChecks && !findAll) CheckForMultipleResults(results, tags);
#endif
            return results;
        }


        //internal static List<GameObject> GetTaggedWith(Dictionary<BHash128, List<GameObject>> allTagged, IEnumerable<TagWithRule> tags, bool matchAnyTag = false, bool findAll = true)
        internal static List<GameObject> GetTaggedWith(Dictionary<BHash128, List<GameObject>> allTagged, IEnumerable<TagHashWithRules> tags, bool findAll = true)
        {
#if UNITY_EDITOR
            // If in the Editor and not playing, find all tagged GameObjects in open scenes and use those for search methods
            if (!Application.isPlaying)
            {
                allTagged = AllTaggedInOpenScenes;
                BTaggedEditorExtensions.FindAll();
            }
#endif
            var results = new List<GameObject>();
            if (tags == null || tags.Count() < 1)
            {
                Debug.LogWarning("List of provided tags is empty");
                return results;
            }

            TagHashWithRules firstInclusiveTag = tags.FirstOrDefault(x => x.rule != InclusionRule.MustExclude && x.hash.IsValid);
            // If there are only tags for exclusion, add everything that doesn't include the passed tags
            if (!firstInclusiveTag.hash.IsValid)
            {
                foreach (var kvp in allTagged)
                {
                    var e = tags.GetEnumerator();
                    bool isExcluded = false;
                    while (e.MoveNext() && !isExcluded)
                    {
                        isExcluded |= !e.Current.hash.IsValid || e.Current.hash == kvp.Key;
                    }
                    if (!isExcluded)
                    {
                        for (int i = 0; i < kvp.Value.Count; ++i)
                        {
                            if (!results.Contains(kvp.Value[i])) results.Add(kvp.Value[i]);
                        }
                    }
                }
            }
            else
            {
                BHash128 firstTag = firstInclusiveTag.hash;
                if (allTagged.ContainsKey(firstTag))
                {
                    var firstTagsMatches = new List<GameObject>(allTagged[firstTag]);
                    if (tags.Count() == 1)
                    {
                        results.AddRange(firstTagsMatches);
                    }
                    else
                    {
                        foreach (var tagEntry in tags)
                        {
                            if (tagEntry.hash != firstTag && tagEntry.rule == InclusionRule.Any)
                            {
                                if (allTagged.ContainsKey(tagEntry.hash))
                                {
                                    for (int i = 0; i < allTagged[tagEntry.hash].Count; ++i)
                                    {
                                        if (!firstTagsMatches.Contains(allTagged[tagEntry.hash][i])) firstTagsMatches.Add(allTagged[tagEntry.hash][i]);
                                    }
                                }
                            }
                        }

                        for (int i = 0; i < firstTagsMatches.Count; ++i)
                        {
                            bool goHasAllTags = true;
                            // For every Tag that's not the first Tag, check to see
                            // the list of GameObjects for the tag contains the GameObject
                            foreach (var tagEntry in tags)
                            {
                                if (tagEntry.hash != firstTag)
                                {
                                    switch (tagEntry.rule)
                                    {
                                        case InclusionRule.Any: continue;
                                        case InclusionRule.MustInclude:
                                            if (!allTagged.ContainsKey(tagEntry.hash) || !allTagged[tagEntry.hash].Contains(firstTagsMatches[i])) goHasAllTags = false;
                                            break;
                                        case InclusionRule.MustExclude:
                                            if (allTagged.ContainsKey(tagEntry.hash) && allTagged[tagEntry.hash].Contains(firstTagsMatches[i])) goHasAllTags = false;
                                            break;
                                    }
                                }
                            }
                            if (goHasAllTags && !results.Contains(firstTagsMatches[i])) results.Add(firstTagsMatches[i]);
                        }
                    }
                }
            }

            return results;
        }

        // Checks the Tag is not null and returns the default Hash if it is
        public static BHash128 GetHash(Tag tag)
        {
            bool valid = tag != null;
#if UNITY_EDITOR
            if (!valid) Debug.LogWarning("-Untagged- was passed into a query");
#endif
            return valid ? tag.Hash : default;
        }


#if UNITY_ENTITIES
        #region ENTITY Extensions

        static TagSearchSystem tagSearchSystem;
        private static void InitTagSystem()
        {
            if (tagSearchSystem == null || tagSearchSystem.World == null) tagSearchSystem = World.DefaultGameObjectInjectionWorld.GetOrCreateSystem<TagSearchSystem>();
        }

        public static NativeArray<BHash128> ToHashes(this IEnumerable<Tag> tags, Allocator allocator)
        {
            NativeArray<BHash128> hashes = new NativeArray<BHash128>(tags.Count(), allocator);
            var enumerator = tags.GetEnumerator();
            int idx = 0;
            while (enumerator.MoveNext())
            {
                hashes[idx] = GetHash(enumerator.Current);
                idx++;
            }
            return hashes;
        }
        public static void ToHashes(this IEnumerable<Tag> tags, ref NativeArray<BHash128> hashes)
        {
            var enumerator = tags.GetEnumerator();
            int idx = 0;
            while (enumerator.MoveNext() && idx < hashes.Length)
            {
                hashes[idx] = GetHash(enumerator.Current);
                idx++;
            }
        }
        public static void ToHashes(this IEnumerable<Tag> tags, ref NativeList<BHash128> hashes)
        {
            if (hashes.Capacity < tags.Count()) hashes.ResizeUninitialized(tags.Count());
            var enumerator = tags.GetEnumerator();
            int idx = 0;
            while (enumerator.MoveNext())
            {
                hashes[idx] = GetHash(enumerator.Current);
                idx++;
            }
        }

        public static bool Match(this BHash128 hash, ref BlobArray<BHash128> entityHashes, bool matchAny = false)
        {
            if (matchAny)
                return MatchAny(hash, ref entityHashes);
            else
                return MatchAll(hash, ref entityHashes);
        }
        public static bool Match(this NativeArray<BHash128> tagHashes, ref BlobArray<BHash128> entityHashes, bool matchAny = false)
        {
            if (matchAny)
                return MatchAny(tagHashes, ref entityHashes);
            else
                return MatchAll(tagHashes, ref entityHashes);
        }

        public static bool Match(this NativeArray<TagHashWithRules> tagHashes, ref BlobArray<BHash128> entityHashes)
        {
            bool onlyDidExclusion = true;
            bool matched = false;

            for (int i = 0; i < tagHashes.Length; ++i)
            {
                switch (tagHashes[i].rule)
                {
                    case InclusionRule.MustExclude:
                        if (BlobArrayContains(ref entityHashes, tagHashes[i].hash)) return false;
                        break;
                    case InclusionRule.MustInclude:
                        if (!BlobArrayContains(ref entityHashes, tagHashes[i].hash)) return false;
                        matched = true;
                        onlyDidExclusion = false;
                        break;
                    case InclusionRule.Any:
                        if (!matched && BlobArrayContains(ref entityHashes, tagHashes[i].hash)) matched = true;
                        onlyDidExclusion = false;
                        break;
                }
            }
            return matched || onlyDidExclusion;
        }
        public static bool BlobArrayContains(ref BlobArray<BHash128> ba, BHash128 val)
        {
            for (int i = 0; i < ba.Length; ++i) if (ba[i].Equals(val)) return true;
            return false;
        }

        public static bool MatchAny(this BHash128 hash, ref BlobArray<BHash128> entityHashes)
        {
            bool matched = false;
            // On the assumption most entities will contain ~1 tags, if we're trying to match
            // any tag let's go with an entity first search. So long as the entity
            // has at least one tag and that tag is in our tagHashes Array, it's valid
            for (int h = 0; h < entityHashes.Length; ++h)
            {
                if (entityHashes[h] == hash)
                {
                    matched = true;
                    break;
                }
            }
            return matched;
        }

        public static bool MatchAny(this NativeArray<BHash128> tagHashes, ref BlobArray<BHash128> entityHashes)
        {
            bool matched = false;
            // On the assumption most entities will contain ~1 tags, if we're trying to match
            // any tag let's go with an entity first search. So long as the entity
            // has at least one tag and that tag is in our tagHashes Array, it's valid
            for (int h = 0; h < entityHashes.Length; ++h)
            {
                if (tagHashes.Contains(entityHashes[h]))
                {
                    matched = true;
                    break;
                }
            }
            return matched;
        }

        public static bool MatchAll(this BHash128 hash, ref BlobArray<BHash128> entityHashes)
        {
            // If the entity has less tags than the tags we require, it clearly doesn't match
            if (entityHashes.Length < 1) return false;

            bool matched = true;
            bool entityHasHash = false;
            for (int h = 0; h < entityHashes.Length; ++h)
            {
                if (entityHashes[h] == hash)
                {
                    entityHasHash = true;
                    break;
                }
            }
            if (!entityHasHash) matched = false;
            return matched;
        }

        public static bool MatchAll(this NativeArray<BHash128> tagHashes, ref BlobArray<BHash128> entityHashes)
        {
            // If the entity has less tags than the tags we require, it clearly doesn't match
            if (entityHashes.Length < tagHashes.Length) return false;

            bool matched = true;
            bool entityHasHash = false;

            // As we need to ensure every tag is present on an entity we go with
            // a tag first search - checking that for every tag provided it exists 
            // within the entitie's BlobArray
            for (int i = 0; i < tagHashes.Length; ++i)
            {
                entityHasHash = false;
                for (int h = 0; h < entityHashes.Length; ++h)
                {
                    if (entityHashes[h] == tagHashes[i])
                    {
                        entityHasHash = true;
                        break;
                    }
                }
                if (!entityHasHash)
                {
                    matched = false;
                    break;
                }
            }
            return matched;
        }

        /// <summary>
        /// MatchAllExactly is similar to MatchAll but in addition, checks that <b>only</b>
        /// the specified tags are present.
        /// </summary>
        /// <param name="tagHashes"></param>
        /// <param name="entityHashes"></param>
        /// <returns></returns>
        public static bool MatchExactly(this BHash128 hash, ref BlobArray<BHash128> entityHashes)
        {
            if (entityHashes.Length != 1) return false;
            return entityHashes[0] == hash;
        }

        /// <summary>
        /// MatchAllExactly is similar to MatchAll but in addition, checks that <b>only</b>
        /// the specified tags are present.
        /// </summary>
        /// <param name="tagHashes"></param>
        /// <param name="entityHashes"></param>
        /// <returns></returns>
        public static bool MatchAllExactly(this NativeArray<BHash128> tagHashes, ref BlobArray<BHash128> entityHashes)
        {
            if (tagHashes.Length != entityHashes.Length) return false;
            return MatchAll(tagHashes, ref entityHashes);
        }

        public static Unity.Entities.Entity GetEntityForTag(this Tag tag)
        {
            if (tag == null) return default;
            NativeList<Entity> tmp = new NativeList<Entity>(Allocator.TempJob);
            GetEntitiesForTag(default, tag, ref tmp, false);
            Entity result = tmp.Length > 0 ? tmp[0] : default;
            tmp.Dispose();
            return result;
        }

        public static void GetTagListForEntity(this Entity entity, EntityManager entityManager, ref NativeList<BHash128> foundTags)
        {
            InitTagSystem();
            if (!entityManager.HasComponent<TagICD>(entity)) return;
            ref var hashes = ref entityManager.GetComponentData<TagICD>(entity).Blob.Value.hashes;
            for (int i = 0; i < hashes.Length; ++i) foundTags.Add(hashes[i]);
            return;
        }

        public static void GetEntitiesForTag(this Tag tag, ref NativeList<Entity> foundEntities) =>
            GetEntitiesForHash(GetHash(tag), ref foundEntities);
        public static void GetEntitiesForHash(this BHash128 hash, ref NativeList<Entity> foundEntities)
        {
            if (!hash.IsValid) return;
            GetEntitiesWithTags(GetTagHashWithRules(hash), default, ref foundEntities, true);
            return;
        }

        public static Unity.Entities.Entity GetEntityForTag(this Entity entity, Tag tag) =>
            GetEntityForHash(entity, GetHash(tag));
        public static Unity.Entities.Entity GetEntityForHash(this Entity entity, BHash128 hash)
        {
            NativeList<Entity> foundEntities = new NativeList<Entity>(1, Allocator.Temp);
            GetEntitiesForTag(entity, hash, ref foundEntities, false);
            return foundEntities.Length > 0 ? foundEntities[0] : default;
        }

        public static void GetEntitiesForTag(this Entity entity, Tag tag, ref NativeList<Entity> foundEntities, bool findAll = true) =>
            GetEntitiesForTag(entity, GetHash(tag), ref foundEntities, findAll);
        public static void GetEntitiesForTag(this Entity entity, BHash128 hash, ref NativeList<Entity> foundEntities, bool findAll = true)
        {
            if (!hash.IsValid) return;
            GetEntitiesWithTags(GetTagHashWithRules(hash), entity, ref foundEntities, findAll);
        }

        public static Unity.Entities.Entity GetEntityWithAnyTags(this Entity entity, ref NativeArray<BHash128> hashes)
        {
            NativeList<Entity> foundEntities = new NativeList<Entity>(1, Allocator.Temp);
            GetEntitiesWithAnyTags(entity, ref hashes, ref foundEntities, false);
            return foundEntities.Length > 0 ? foundEntities[0] : default;
        }
        public static void GetEntitiesWithAnyTags(this Entity entity, ref NativeArray<BHash128> hashes, ref NativeList<Entity> foundEntities, bool findAll = true)
        {
            GetEntitiesWithTags(GetTagHashesWithRules(hashes), entity, ref foundEntities, findAll);
        }

        public static Unity.Entities.Entity GetEntityWithAllTags(this Entity entity, ref NativeArray<BHash128> hashes)
        {
            NativeList<Entity> foundEntities = new NativeList<Entity>(1, Allocator.Temp);
            GetEntitiesWithAllTags(entity, ref hashes, ref foundEntities, false);
            return foundEntities.Length > 0 ? foundEntities[0] : default;
        }
        public static void GetEntitiesWithAllTags(this Entity entity, ref NativeArray<BHash128> hashes, ref NativeList<Entity> foundEntities, bool findAll = true)
        {
            GetEntitiesWithTags(GetTagHashesWithRules(hashes, InclusionRule.MustInclude), entity, ref foundEntities, findAll);
        }


        // Used in TagGameObject.cs
        // This utility method can be used to turn an array of tags into a BTaggedBlobAsset reference
        // It's recommended to tag at author time - conversion is an ideal time to for this to be utilised
        public static void PopulateBlobAssetRefenceForTags(Tag[] tags, ref BlobAssetReference<BTaggedBlobAsset> blobAssetReference)
        {
            using (BlobBuilder blobBuilder = new BlobBuilder(Allocator.Temp))
            {
                int validTags = 0;
                for (int t = 0; t < tags.Length; ++t)
                {
                    if (!tags[t].IsDefault) validTags++;
                }
                if (validTags < 1) return;

                ref BTaggedBlobAsset blobAsset = ref blobBuilder.ConstructRoot<BTaggedBlobAsset>();
                BlobBuilderArray<BHash128> hashArray = blobBuilder.Allocate(ref blobAsset.hashes, validTags);
                for (int t = 0; t < tags.Length; ++t)
                {
                    if (!tags[t].IsDefault) hashArray[t] = GetHash(tags[t]);
                }
                blobAssetReference = blobBuilder.CreateBlobAssetReference<BTaggedBlobAsset>(Allocator.Persistent);
            }
        }
        #endregion
#endif
    }
}
