﻿/* Copyright(c) Tim Watts, Box of Clicks - All Rights Reserved */

using UnityEngine;
using System.Collections.Generic;
using System.Linq;
#if UNITY_ENTITIES
using Unity.Entities;
using Unity.Collections;
#endif

namespace BOC.BTagged
{
    [HelpURL("https://docs.google.com/document/d/10FXOb7qEpLLTN9G8t8vi6b_JdCpanXJwkyyHINyU9Z0/")]
    [AddComponentMenu("BTagged/Tag", 1)]
    public class TagGameObject : MonoBehaviour
#if UNITY_ENTITIES
        , IConvertGameObjectToEntity
#endif
    {
        // Fast-ish lookup for tagged GameObjects - populated on Awake per GameObject
        static bool AwakeComplete = false;
        static public Dictionary<BHash128, List<GameObject>> _AllTaggedGOs = new Dictionary<BHash128, List<GameObject>>();
        static public Dictionary<BHash128, List<GameObject>> AllTaggedGOs
        {
            get
            {
                // If AllTaggedGOs are requested before Awake has completed for all MonoBehaviours, we manually invoke this components Init method
                if(!AwakeComplete)
                {
                    //Debug.LogWarning("Forcing Init on all components");
                    var components = GameObject.FindObjectsOfType<TagGameObject>();
                    for (int c = 0; c < components.Length; ++c) components[c].Init();
                    AwakeComplete = true;
                }
                return _AllTaggedGOs;
            }
        }

        // The Tag - replaces Component.tag mostly to avoid confusion and
        // as I believe it is rarely used
        public new Tag tag;
        public string TagLabel() => (tag == null ? "None" : tag.name);

        void Awake()
        {
            AwakeComplete = false;
            Init();
        }
        bool initialized = false;
        public void Init()
        {
            if (initialized) return;

            initialized = true;
            // If the tag is set to default / empty then don't tag this GameObject
            if (tag == null || tag.IsDefault) return;

            // If it's the first time the tag is registered, create a list
            if (!_AllTaggedGOs.ContainsKey(tag.Hash)) _AllTaggedGOs.Add(tag.Hash, new List<GameObject>());
            // Add this gameObject to the list associated with the tag
            if (!_AllTaggedGOs[tag.Hash].Contains(gameObject)) _AllTaggedGOs[tag.Hash].Add(gameObject);
        }

        void Start()
        {
            AwakeComplete = true;
            triggerOnStart = true;
        }

        // Invoke in LateUpdate, giving user scripts an opportunity to Awake/Enable
        bool triggerOnStart = false;
        internal void TriggerOnStart()
        {
            triggerOnStart = false;
            if (tag != null) tag.OnGameObjectStart?.Invoke(gameObject);
        }

        // Invoke in LateUpdate, giving user scripts an opportunity to Awake/Enable
        bool triggerOnEnable = false;
        void OnEnable() => triggerOnEnable = true;
        internal void TriggerOnEnable()
        {
            triggerOnEnable = false;
            if (tag != null) tag.OnGameObjectEnabled?.Invoke(gameObject);
        }

        void LateUpdate()
        {
            if (triggerOnEnable) TriggerOnEnable();
            if (triggerOnStart) TriggerOnStart();
        }

        void OnDisable()
        {
            if (tag != null) tag.OnGameObjectDisabled?.Invoke(gameObject);
        }
        void OnDestroy()
        {
            if (tag != null) tag.OnGameObjectDestroyed?.Invoke(gameObject);
            // When this GameObject is destroyed, remove its reference from the lookup list
            if (tag != null && !tag.IsDefault && _AllTaggedGOs.ContainsKey(tag) && _AllTaggedGOs[tag].Contains(gameObject)) _AllTaggedGOs[tag].Remove(gameObject);
        }


#if UNITY_ENTITIES
        // If the Unity.Entities package is present, support Converting to entities
        // It's possible to tag a GameObject multiple times therefore using a buffer/BlobArray.
        // We also choose not to store the data in the chunk and use a BlobAssetReference instead.
        // This is beneficial when instantiating Prefabs and also keeps the chunk utilisation higher than it might otherwise
        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            // For each converting GameObject, we want to create and Populate a TagICD just once, 
            // using all the BTaggedTagGameObject components associated with this GameObject.
            // If the Tag already exists we assume the conversion for this GO/Entity has already occured.
            if (conversionSystem.EntityManager.HasComponent<TagICD>(entity)) return;

            // Don't tag this entity if the Tag is set to default/empty
            if (tag == null || tag.IsDefault) return;

            BlobAssetReference<BTaggedBlobAsset> blobAssetReference = default;

            // Select all BTaggedTagGameObject 'Tags' and use the utility method to create a BlobAssetReference of them
            BTagged.PopulateBlobAssetRefenceForTags(gameObject.GetComponents<TagGameObject>().Select(btgo => btgo.tag).ToArray(), ref blobAssetReference);
            dstManager.AddComponentData(entity, new TagICD() { Blob = blobAssetReference });
        }
#endif
    }
}
