﻿/* Copyright(c) Tim Watts, Box of Clicks - All Rights Reserved */

using BOC.BTagged.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using static BOC.BTagged.Shared.BTaggedSharedSettings;

namespace BOC.BTagged.EditorTools
{
    static class BTaggedSettingsUIElementsRegister
    {
        const string HelpFullRefresh = "A Full Refresh will find references in:\n - Open Scenes\n - Prefabs\n - ScriptableObjects\n - Scenes\nEquivalent to manually clicking the 'Find All References' button via the inspector.";
        const string HelpOpenSceneRefresh = "Searching just the open scenes can be much faster than searching the whole project. A good default for larger projects.";
        const string HelpCachedOnly = "This option will only display the previously found results held in cache. This is a great option for those who prefer to manually click the 'Find All References' button and effectively disable the automatic refresh upon selection.";

        static Label helpLabel;
        [SettingsProvider]
        public static SettingsProvider CreateMyCustomSettingsProvider()
        {
            var provider = new SettingsProvider("Preferences/BTaggedSettings", SettingsScope.User)
            {
                label = "BTagged",
                // activateHandler is called when the user clicks on the Settings item in the Settings window.
                activateHandler = (searchContext, rootElement) =>
                {
                    var contents = new VisualElement();
                    contents.style.paddingBottom = contents.style.paddingLeft = contents.style.paddingRight = contents.style.paddingTop = 10;
                    var settings = BTaggedSharedSettings.GetSerializedSettings();

                    var title = new Label("BTagged Settings")
                    {
                        style = { fontSize = 18, unityFontStyleAndWeight = new StyleEnum<FontStyle>(FontStyle.Bold), paddingBottom = 10 }
                    };
                    title.AddToClassList("title");
                    contents.Add(title);

                    var pf = new PropertyField(settings.FindProperty("searchMode"), "Auto-Reference Finding");
                    pf.RegisterCallback<ChangeEvent<string>>(UpdateHelpTextLabel);
                    pf.Bind(settings);
                    Label desc = new Label("The extents of the project that are automatically searched upon Selecting a Tag asset in the project window.");
                    desc.style.marginBottom = desc.style.marginLeft = desc.style.marginRight = desc.style.marginTop = 4f;
                    desc.style.whiteSpace = new StyleEnum<WhiteSpace>(WhiteSpace.Normal);
                    contents.Add(desc);
                    contents.Add(pf);
                    helpLabel = new Label("");
                    helpLabel.style.marginBottom = helpLabel.style.marginLeft = helpLabel.style.marginRight = helpLabel.style.marginTop = 4f;
                    helpLabel.style.paddingBottom = helpLabel.style.paddingLeft = helpLabel.style.paddingRight = helpLabel.style.paddingTop = 8f;
                    helpLabel.style.borderTopLeftRadius = helpLabel.style.borderBottomLeftRadius = helpLabel.style.borderBottomRightRadius = helpLabel.style.borderTopRightRadius = 5f;
                    helpLabel.style.backgroundColor = new StyleColor(new Color(0.5f, 0.5f, 0.5f, 0.2f));
                    helpLabel.style.whiteSpace = new StyleEnum<WhiteSpace>(WhiteSpace.Normal);
                    UpdateHelpTextLabel();
                    contents.Add(helpLabel);


                    Toggle editorCheckToggle = new Toggle("Enable Editor Safety Checks ");
                    editorCheckToggle.value = !settings.FindProperty("disableEditorChecks").boolValue;
                    editorCheckToggle.RegisterValueChangedCallback( ce => { settings.FindProperty("disableEditorChecks").boolValue = !ce.newValue; settings.ApplyModifiedProperties(); });
                    contents.Add(editorCheckToggle);
                    Label editorCheckDesc = new Label("It is possible for multiple GameObjects to have the same Tag. The Editor Safety Checks will warn when a query in code would return more than one match but is only requesting the first. This is because it is non-deterministc - which object you get back might vary. You can, for example, add another Tag to ensure the query is more specific or add .AnyIsFine() to the query.");
                    editorCheckDesc.style.marginBottom = editorCheckDesc.style.marginLeft = editorCheckDesc.style.marginRight = editorCheckDesc.style.marginTop = 4f;
                    editorCheckDesc.style.paddingBottom = editorCheckDesc.style.paddingLeft = editorCheckDesc.style.paddingRight = editorCheckDesc.style.paddingTop = 8f;
                    editorCheckDesc.style.borderTopLeftRadius = editorCheckDesc.style.borderBottomLeftRadius = editorCheckDesc.style.borderBottomRightRadius = editorCheckDesc.style.borderTopRightRadius = 5f;
                    editorCheckDesc.style.backgroundColor = new StyleColor(new Color(0.5f, 0.5f, 0.5f, 0.2f));
                    editorCheckDesc.style.whiteSpace = new StyleEnum<WhiteSpace>(WhiteSpace.Normal);
                    contents.Add(editorCheckDesc);

                    Button btn = new Button(CheckForCollisions);
                    btn.text = "Check for Duplicated Assets";
                    contents.Add(btn);
                    rootElement.Add(contents);
                },

                // Populate the search keywords to enable smart search filtering and label highlighting:
                keywords = new HashSet<string>(new[] { "Auto-Reference", "Finding", "Search", "Refresh", "Tag" })
            };

            return provider;
        }

        private static void CheckForCollisions()
        {
            BTaggedSORegistry.FindAllAssets<BTaggedGroupBase, BTaggedSOBase>();
            for(int i = 0; i < BTaggedSORegistry.AllAssets.Count; ++i)
            {
                var asset = BTaggedSORegistry.AllAssets[i].asset;
                if(asset != null && !asset.IsDefault)
                {
                    for (int j = 0; j < BTaggedSORegistry.AllAssets.Count; ++j)
                    {
                        if (j != i && BTaggedSORegistry.AllAssets[j].asset.Hash.Equals(asset.Hash))
                        {
                            Debug.LogWarning(asset + " is a duplicate of " + BTaggedSORegistry.AllAssets[j].asset + " : suggest removing one of these assets.\n");
                        }

                    }
                }


            }
        }

        private static void UpdateHelpTextLabel(ChangeEvent<string> evt = null)
        {
            switch (BTaggedSharedSettings.SearchMode)
            {
                case SearchRegistryOption.FullRefresh:
                    helpLabel.text = HelpFullRefresh;
                    break;
                case SearchRegistryOption.OpenScenesRefresh:
                    helpLabel.text = HelpOpenSceneRefresh;
                    break;
                case SearchRegistryOption.CachedResultsOnly:
                    helpLabel.text = HelpCachedOnly;
                    break;
            }
        }
    }
}