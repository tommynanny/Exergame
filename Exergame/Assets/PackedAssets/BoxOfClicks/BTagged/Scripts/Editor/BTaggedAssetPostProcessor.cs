﻿/* Copyright(c) Tim Watts, Box of Clicks - All Rights Reserved */

using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace BOC.BTagged.EditorTools
{
    /// <summary>
    /// These methods are concerned with ensuring all BTagged assets have unique hashes,
    /// even if they are created through a Duplicate command
    /// </summary>
    public class BTaggedDetectDuplicates : UnityEditor.AssetModificationProcessor
    {
        public static List<string> newAssets = new List<string>();

        // Seems to reliably detect ctrl+drag for duplication but not Ctrl+D
        // Luckily there's no Right-click duplicate so an asset has to be selected before using Ctrl+D
        // So we can use the asset's editor to detect that instance - it will call RegenerateHashForAsset below
        static void OnWillCreateAsset(string aMetaAssetPath)
        {
            string assetPath = aMetaAssetPath.Substring(0, aMetaAssetPath.Length - 5);
            newAssets.Add(assetPath);
        }
    }

    public class BTaggedAssetPostProcessor : AssetPostprocessor
    {
        public static bool DuplicationOccurred = false;
        public static void RegenerateHashForAsset(BTaggedSOBase asset)
        {
            asset.EditorGenerateNewHash();
            EditorUtility.SetDirty(asset);
            //Debug.LogWarning("Regenerating hash for " + asset + " to ensure uniqueness. It's generally preferable to create new assets via the Drop Down Menu via e.g. the Tag component.");
        }
        public static void RegenerateHashForGroup(BTaggedGroupBase group)
        {
            UnityEngine.Object[] assets = AssetDatabase.LoadAllAssetRepresentationsAtPath(AssetDatabase.GetAssetPath(group));
            for(int i = 0; i < assets.Length; ++i)
            {
                if(assets[i] is BTaggedSOBase) (assets[i] as BTaggedSOBase).EditorGenerateNewHash();
            }
            EditorUtility.SetDirty(group);
            //Debug.LogWarning("Regenerating hash for all assets in " + group + " to ensure uniqueness. It's generally preferable to create new assets via the Drop Down Menu via e.g. the Tag component.");
        }

        static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            //if (BTaggedDetectDuplicates.newAssets.Count == 0) return;
            foreach (string str in importedAssets)
            {
                if (BTaggedDetectDuplicates.newAssets.Contains(str) || (DuplicationOccurred && CheckForDuplicate(str)))
                {
                    BTaggedSOBase so = AssetDatabase.LoadAssetAtPath<BTaggedSOBase>(str);
                    if (so != null && AssetDatabase.IsMainAsset(so))
                    {
                        RegenerateHashForAsset(so);
                    }
                    else
                    {
                        BTaggedGroupBase group = AssetDatabase.LoadAssetAtPath<BTaggedGroupBase>(str);
                        if (group != null) RegenerateHashForGroup(group);
                    }
                    if (BTaggedDetectDuplicates.newAssets.Contains(str)) BTaggedDetectDuplicates.newAssets.Remove(str);
                }
            }
        }

        private static bool CheckForDuplicate(string assetOrGroupPath)
        {
            BTaggedSOBase asset = AssetDatabase.LoadAssetAtPath<BTaggedSOBase>(assetOrGroupPath);
            if (!AssetDatabase.IsMainAsset(asset)) asset = null;
            BTaggedGroupBase group = AssetDatabase.LoadAssetAtPath<BTaggedGroupBase>(assetOrGroupPath);
            if (asset != null || group != null)
            {
                // If the file wasn't created within the last 30 seconds we don't want to automatically modify it 
                // as we may be catching a previously used asset.
                if (System.IO.File.GetCreationTimeUtc(assetOrGroupPath) < DateTime.UtcNow.Subtract(new TimeSpan(0, 0, 30)))
                {
                    //Debug.LogWarning(System.IO.File.GetCreationTimeUtc(assetPath) + " is earlier than " + DateTime.UtcNow.Subtract(new TimeSpan(0, 0, 30)));
                    return false;
                }
                BTaggedSORegistry.FindAllAssets<BTaggedGroupBase, BTaggedSOBase>();

                UnityEngine.Object[] assets = default;
                if (group != null) assets = AssetDatabase.LoadAllAssetRepresentationsAtPath(assetOrGroupPath);
                for (int i = 0; i < BTaggedSORegistry.AllAssets.Count; ++i)
                {
                    if (asset != null && BTaggedSORegistry.AllAssets[i].asset.Hash == asset.Hash && BTaggedSORegistry.AllAssets[i].asset != asset)
                    {
                        //Debug.LogWarning("Found Hash collision. Fixing");
                        return true;
                    }
                    else if (group != null)
                    {
                        for (int a = 0; a < assets.Length; ++a)
                        {
                            if (assets[a] is BTaggedSOBase && BTaggedSORegistry.AllAssets[i].asset.Hash == (assets[a] as BTaggedSOBase).Hash && BTaggedSORegistry.AllAssets[i].asset != (assets[a] as BTaggedSOBase)) return true;
                        }
                    }
                }
            }
            return false;
        }
    }
}
