﻿/* Copyright(c) Tim Watts, Box of Clicks - All Rights Reserved */
/* Modified exerpts from:
 * https://github.com/yasirkula/UnityAssetUsageDetector/
 * The code from the above repository has the following license and only pertains to partial code in this file.
 * MIT License
    Copyright (c) 2016 Süleyman Yasir KULA

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
 */
#if UNITY_EDITOR
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using System.Reflection;
using UnityEngine.Events;
using System.IO;
using System.Linq;
using static BOC.BTagged.Shared.BTaggedSharedSettings;
using BOC.BTagged.Shared;

namespace BOC.BTagged.EditorTools
{
    public class BTaggedSORegistry : MonoBehaviour
    {
        static private BindingFlags fieldModifiers = BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.NonPublic;
        static private BindingFlags propertyModifiers = BindingFlags.Instance | BindingFlags.DeclaredOnly;
        static private List<VariableGetterHolder> validVariables = new List<VariableGetterHolder>(32);
        // An optimization to fetch & filter fields and properties of a class only once
        static private Dictionary<Type, VariableGetterHolder[]> typeToVariables = new Dictionary<Type, VariableGetterHolder[]>(4096);
        static private Dictionary<BTaggedSOBase, bool> assetReferencesHadFullRefresh = new Dictionary<BTaggedSOBase, bool>();
        static private Dictionary<BTaggedSOBase, List<SceneObjectIDBundle>> cachedAssetReferences = new Dictionary<BTaggedSOBase, List<SceneObjectIDBundle>>();
        static BHash128 WorkingHash = default;

        public static bool CancelSearch = false;
        public static Action<float, float> OnSearchProgress;
        public static Action<float, float> OnOpenSceneSearchProgress;

        public static List<(BTaggedGroupBase group, BTaggedSOBase asset)> AllAssets = new List<(BTaggedGroupBase, BTaggedSOBase)>();
        public static void FindAllAssets<GROUP, SO>() where GROUP : BTaggedGroupBase where SO : BTaggedSOBase
        {
            AllAssets.Clear();

            string[] groupGuids = AssetDatabase.FindAssets("t:" + typeof(GROUP));
            for (int groupIndex = 0; groupIndex < groupGuids.Length; ++groupIndex)
            {
                string groupPath = AssetDatabase.GUIDToAssetPath(groupGuids[groupIndex]);
                string groupName = Path.GetFileNameWithoutExtension(groupPath) + "/";
                UnityEngine.Object[] allAssets = AssetDatabase.LoadAllAssetsAtPath(groupPath);
                List<string> uniqueNames = new List<string>();

                for (int i = 0; i < allAssets.Length; ++i)
                {
                    if (allAssets[i] == null) continue;
                    if (allAssets[i] is SO)
                    {
                        SO asset = allAssets[i] as SO;
                        AllAssets.Add((AssetDatabase.LoadMainAssetAtPath(groupPath) as GROUP, asset));
                    }
                }
            }
            string[] individualSOGuids = AssetDatabase.FindAssets("t:" + typeof(SO));
            for (int i = 0; i < individualSOGuids.Length; ++i)
            {
                SO individualAsset = AssetDatabase.LoadMainAssetAtPath(AssetDatabase.GUIDToAssetPath(individualSOGuids[i])) as SO;
                if (individualAsset != null)
                {
                    if (!individualAsset.IsDefault)
                    {
                        AllAssets.Add((null, individualAsset));
                    }
                }
            }
        }

        static public void RefreshSOUsage(BTaggedSOBase target, SearchRegistryOption searchOption)
        {
            if (target == null) return;
            WorkingHash = target.Hash;
    
            string[] openScenePaths = new string[EditorSceneManager.sceneCount];
            for (int s = 0; s < openScenePaths.Length; ++s) openScenePaths[s] = EditorSceneManager.GetSceneAt(s).path;

            string selectedObjGUID = "";
            long fileID = 0;
            AssetDatabase.TryGetGUIDAndLocalFileIdentifier(target, out selectedObjGUID, out fileID);

            List<string> guids = new List<string>(AssetDatabase.FindAssets("t:ScriptableObject", new string[] { "Assets" }));

            if (!assetReferencesHadFullRefresh.ContainsKey(target))
            {
                assetReferencesHadFullRefresh[target] = false;
            }
            if (searchOption == SearchRegistryOption.FullRefresh || (!assetReferencesHadFullRefresh[target] && BTaggedSharedSettings.SearchMode == SearchRegistryOption.FullRefresh))
            {
                assetReferencesHadFullRefresh[target] = true;
                guids.AddRange(AssetDatabase.FindAssets("t:Scene", new string[] { "Assets" }).Where( x => !openScenePaths.Contains(AssetDatabase.GUIDToAssetPath(x))));
                guids.AddRange(AssetDatabase.FindAssets("t:Prefab", new string[] { "Assets" }));
            }

            if (!cachedAssetReferences.ContainsKey(target))
            {
                cachedAssetReferences[target] = new List<SceneObjectIDBundle>();
            }
            else
            {
                if(searchOption == SearchRegistryOption.FullRefresh) cachedAssetReferences[target].Clear();
                // Clear out any entries for currently open scenes as these will be refreshed
                for(int i = cachedAssetReferences[target].Count-1; i >= 0; --i)
                {
                    if(openScenePaths.Contains(cachedAssetReferences[target][i].scenePath))
                    {
                        cachedAssetReferences[target].RemoveAt(i);
                    }
                }
            }

            for (int i = 0; i < guids.Count; ++i)
            {
                if (CancelSearch) return;
                OnSearchProgress?.Invoke(i+1, guids.Count);
                string assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);

                GameObject loadedAssetGO = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
                SceneAsset loadedAssetScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(assetPath);
                ScriptableObject loadedAssetSO = AssetDatabase.LoadAssetAtPath<ScriptableObject>(assetPath);

                if (loadedAssetSO != null)
                {
                    SearchFieldsAndPropertiesOf(loadedAssetSO);
                }
                else if (loadedAssetGO != null)
                {
                    try
                    {
                        GameObject prefabInstance = AssetDatabase.LoadAssetAtPath<GameObject>(assetPath);
                        Component[] components = prefabInstance.GetComponentsInChildren<Component>(true);
                        for (int c = 0; c < components.Length; c++)
                        {
                            if (components[c] == null || components[c].Equals(null)) continue;

                            SearchFieldsAndPropertiesOf(components[c]);
                        }
                    }
                    catch{}
                }
                else if(loadedAssetScene != null)
                {
                    string scenePath = AssetDatabase.GetAssetPath(loadedAssetScene);
                    int lineCount = 0;
                    foreach(var line in File.ReadLines(scenePath))
                    {
                        if (line.Contains("guid:"))
                        {
                            if (line.Contains(selectedObjGUID))
                            {
                                if(line.Contains("fileID:") && line.Contains(fileID.ToString()))
                                {
                                    SceneObjectIDBundle sceneGOGUID = new SceneObjectIDBundle(scenePath, string.Empty, lineCount);
                                    Add(target, sceneGOGUID);
                                }
                            }
                        }
                        lineCount++;
                    }
                }
            }

            SearchOpenScenes();
        }

        private static void SearchOpenScenes()
        {
            for (int s = 0; s < SceneManager.sceneCount; ++s)
            {
                if (CancelSearch) return;
                OnOpenSceneSearchProgress?.Invoke(s+1, SceneManager.sceneCount);
                for (int r = 0; r < SceneManager.GetSceneAt(s).GetRootGameObjects().Length; ++r)
                {
                    GameObject rootGO = SceneManager.GetSceneAt(s).GetRootGameObjects()[r] as GameObject;
                    if (rootGO != null)
                    {
                        // If searched asset is a GameObject, include its components in the search
                        Component[] components = rootGO.GetComponentsInChildren<Component>(true);
                        for (int c = 0; c < components.Length; c++)
                        {
                            if (components[c] == null || components[c].Equals(null)) continue;

                            SearchFieldsAndPropertiesOf(components[c]);
                        }
                    }
                }
            }
        }

        static public void CleanReferences(BTaggedSOBase obj)
        {
            if (cachedAssetReferences.ContainsKey(obj))
            {
                for (int i = 0; i < cachedAssetReferences[obj].Count; i++)
                {
                    if(cachedAssetReferences[obj][i] == null)
                    {
                        cachedAssetReferences[obj].RemoveAt(i);
                        i--;
                    }
                }
            }
        }

        static public void Add(BTaggedSOBase obj)
        {
            if (!cachedAssetReferences.ContainsKey(obj))
            {
                cachedAssetReferences.Add(obj, new List<SceneObjectIDBundle>());
            }
            RefreshSOUsage(obj, SearchRegistryOption.OpenScenesRefresh);
        }

        static public void Add(BTaggedSOBase obj, SceneObjectIDBundle c)
        {
            if(obj == null) return;

            if (!cachedAssetReferences.ContainsKey(obj)) cachedAssetReferences[obj] = new List<SceneObjectIDBundle>();

            if (!cachedAssetReferences[obj].Contains(c))
            {
                cachedAssetReferences[obj].Add(c);
            }
            else
            {
                // TODO This can happen when there are multiple results for one component - might account for this in the future
                //Debug.LogWarning(obj + " already had " + c + " reference in list");
            }
        }

        static public void Remove(BTaggedSOBase obj)
        {
            if (cachedAssetReferences.ContainsKey(obj))
            {
                cachedAssetReferences.Remove(obj);
            }
        }
        static public void Remove(BTaggedSOBase obj, UnityEngine.Object targ)
        {
            SceneObjectIDBundle sceneGOGUID = new SceneObjectIDBundle(AssetDatabase.GetAssetOrScenePath(targ), targ.ToString(), targ.GetInstanceID());
            Remove(obj, sceneGOGUID);
        }
        static public void Remove(BTaggedSOBase obj, SceneObjectIDBundle c)
        {
            if (obj == null) return;

            if(cachedAssetReferences.ContainsKey(obj))
            {
                cachedAssetReferences[obj].Remove(c);
            }
        }

        static public bool HasCheckedAssetsFor(BTaggedSOBase obj)
        {
            return cachedAssetReferences.ContainsKey(obj);
        }

        static public bool HasCachedResults(BTaggedSOBase obj)
        {
            return (cachedAssetReferences.ContainsKey(obj));
        }
        static public void References(BTaggedSOBase obj, ref List<SceneObjectIDBundle> val, SearchRegistryOption searchOption)
        {
            if (obj == null) return;
            CancelSearch = false;

            if (searchOption != SearchRegistryOption.CachedResultsOnly)
            {
                RefreshSOUsage(obj, searchOption);
            }
            if (cachedAssetReferences.ContainsKey(obj))
            {
                val = cachedAssetReferences[obj];
                return;
            }
        }

        static private void SearchFieldsAndPropertiesOf(UnityEngine.Object component)
        {
            // Get filtered variables for this object
            VariableGetterHolder[] variables = GetFilteredVariablesForType(component.GetType());
            for (int i = 0; i < variables.Length; i++)
            {
                try
                {
                    object variableValue = variables[i].Get(component);
                    if (variableValue == null)
                        continue;

                    if(variableValue is IEnumerable)
                    {
                        foreach (object arrayItem in (IEnumerable)variableValue)
                        {
                            AddIfTrackedOrReference(arrayItem, component);
                        }
                    }
                    else if ( variableValue is UnityEventBase)
                    {
                        for(int j = (variableValue as UnityEventBase).GetPersistentEventCount()-1; j >= 0; --j)
                        {
                            AddIfTrackedOrReference((variableValue as UnityEventBase).GetPersistentTarget(j), component);
                        }
                    }

                    AddIfTrackedOrReference(variableValue, component);
                }
                catch (UnassignedReferenceException)
                { }
                catch (MissingReferenceException)
                { }
            }
        }

        private static void AddIfTrackedOrReference(object variableValue, UnityEngine.Object referenceNode)
        {
            if (variableValue is BTaggedSOBase && ((BTaggedSOBase)variableValue).Hash.Equals(WorkingHash))
            {
                SceneObjectIDBundle sceneGOGUID = new SceneObjectIDBundle(AssetDatabase.GetAssetOrScenePath(referenceNode), referenceNode.ToString(), referenceNode.GetInstanceID());
                Add(((BTaggedSOBase)variableValue), sceneGOGUID);
            }
        }

        // Get filtered variables for a type
        static private VariableGetterHolder[] GetFilteredVariablesForType(Type type)
        {
            VariableGetterHolder[] result;
            if (typeToVariables.TryGetValue(type, out result)) return result;

            // This is the first time this type of object is seen, filter and cache its variables
            // Variable filtering process:
            // 1- skip Obsolete variables
            // 2- skip primitive types, enums and strings
            // 3- skip common Unity types that can't hold any references (e.g. Vector3, Rect, Color, Quaternion)

            validVariables.Clear();

            // Filter the fields
            if (fieldModifiers != (BindingFlags.Instance | BindingFlags.DeclaredOnly))
            {
                Type currType = type;
                while (currType != typeof(object))
                {
                    FieldInfo[] fields = currType.GetFields(fieldModifiers);
                    for (int i = 0; i < fields.Length; i++)
                    {
                        // Skip obsolete fields
                        if (Attribute.IsDefined(fields[i], typeof(ObsoleteAttribute)))
                            continue;

                        // Skip primitive types
                        Type fieldType = fields[i].FieldType;
                        if (fieldType.IsPrimitive || fieldType == typeof(string) || fieldType.IsEnum)
                            continue;

                        VariableGetVal getter = fields[i].GetValue;
                        if (getter != null)
                        {
                            validVariables.Add(new VariableGetterHolder(fields[i], getter));
                        }
                    }
                    currType = currType.BaseType;
                }
            }

            if (propertyModifiers != (BindingFlags.Instance | BindingFlags.DeclaredOnly))
            {
                Type currType = type;
                while (currType != typeof(object))
                {
                    PropertyInfo[] properties = currType.GetProperties(propertyModifiers);
                    for (int i = 0; i < properties.Length; i++)
                    {
                        // Skip obsolete properties
                        if (Attribute.IsDefined(properties[i], typeof(ObsoleteAttribute)))
                            continue;

                        // Skip primitive types
                        Type propertyType = properties[i].PropertyType;
                        if (propertyType.IsPrimitive || propertyType == typeof(string) || propertyType.IsEnum)
                            continue;

                        // Additional filtering for properties:
                        // 1- Ignore "gameObject", "transform", "rectTransform" and "attachedRigidbody" properties of Component's to get more useful results
                        // 2- Ignore "canvasRenderer" and "canvas" properties of Graphic components
                        // 3 & 4- Prevent accessing properties of Unity that instantiate an existing resource (causing leak)
                        string propertyName = properties[i].Name;
                        if (typeof(Component).IsAssignableFrom(currType) && (propertyName.Equals("gameObject") ||
                            propertyName.Equals("transform") || propertyName.Equals("attachedRigidbody") || propertyName.Equals("rectTransform")))
                            continue;
                        else if (typeof(UnityEngine.UI.Graphic).IsAssignableFrom(currType) &&
                            (propertyName.Equals("canvasRenderer") || propertyName.Equals("canvas")))
                            continue;
                        else if (typeof(MeshFilter).IsAssignableFrom(currType) && propertyName.Equals("mesh"))
                            continue;
                        else if (typeof(Renderer).IsAssignableFrom(currType) &&
                            (propertyName.Equals("sharedMaterial") || propertyName.Equals("sharedMaterials")))
                            continue;
                        else if ((propertyName.Equals("material") || propertyName.Equals("materials")) &&
                            (typeof(Renderer).IsAssignableFrom(currType) || typeof(Collider).IsAssignableFrom(currType) ||
                            typeof(Collider2D).IsAssignableFrom(currType)))
                            continue;
                        else
                        {
                            VariableGetVal getter = properties[i].GetValue;
                            if (getter != null) validVariables.Add(new VariableGetterHolder(properties[i], getter));
                        }
                    }
                    currType = currType.BaseType;
                }
            }

            result = validVariables.ToArray();

            // Cache the filtered fields
            typeToVariables.Add(type, result);
            return result;
        }
        
    }
    // Delegate to get the value of a variable (either field or property)
    public delegate object VariableGetVal(object obj);

    // Custom struct to hold a variable, its important properties and its getter function
    public struct VariableGetterHolder
    {
        public readonly string name;
        public readonly bool isProperty;
        private readonly VariableGetVal getter;

        public VariableGetterHolder(FieldInfo fieldInfo, VariableGetVal getter)
        {
            name = fieldInfo.Name;
            isProperty = false;
            this.getter = getter;
        }

        public VariableGetterHolder(PropertyInfo propertyInfo, VariableGetVal getter)
        {
            name = propertyInfo.Name;
            isProperty = true;
            this.getter = getter;
        }

        public object Get(object obj)
        {
            return getter(obj);
        }
    }

    public class SceneObjectIDBundle : IEquatable<SceneObjectIDBundle>
    {
        public string scenePath;
        public string objectName;
        public int id;
        public SceneObjectIDBundle(string scene, string name, int id)
        {
            scenePath = scene;
            objectName = name;
            this.id = id;
        }
        public override int GetHashCode()
        {
            int result = 1;
            result = result * 31 * scenePath.GetHashCode();
            result = result * 31 * objectName.GetHashCode();
            result += id;
            return result;
        }

        public static bool operator ==(SceneObjectIDBundle lhs, SceneObjectIDBundle rhs)
        {
            return lhs.Equals(rhs);
        }

        public static bool operator !=(SceneObjectIDBundle lhs, SceneObjectIDBundle rhs)
        {
            return !(lhs.Equals(rhs));
        }
        public override bool Equals(object obj)
        {
            return this.Equals((SceneObjectIDBundle)obj);
        }
        public bool Equals(SceneObjectIDBundle obj)
        {
            return this.scenePath == obj.scenePath && this.objectName == obj.objectName && this.id == obj.id;
        }
    }

    // Credit: http://stackoverflow.com/questions/724143/how-do-i-create-a-delegate-for-a-net-property
    public interface IPropertyAccessor
    {
        object GetValue(object source);
    }
    // A wrapper class for properties to get their values more efficiently
    public class PropertyWrapper<TObject, TValue> : IPropertyAccessor where TObject : class
    {
        private readonly Func<TObject, TValue> getter;

        public PropertyWrapper(MethodInfo getterMethod)
        {
            getter = (Func<TObject, TValue>)Delegate.CreateDelegate(typeof(Func<TObject, TValue>), getterMethod);
        }

        public object GetValue(object obj)
        {
            try
            {
                return getter((TObject)obj);
            }
            catch
            {
                // Property getters may return various kinds of exceptions
                // if their backing fields are not initialized (yet)
                return null;
            }
        }
    }
}
#endif
