﻿/* Copyright(c) Tim Watts, Box of Clicks - All Rights Reserved */

using System.Collections.Generic;
using UnityEditor;

namespace BOC.BTagged.EditorTools
{
    public class BTaggedEditorUtils<GROUP, SO> where GROUP : BTaggedGroupBase<SO> where SO : BTaggedSOBase
    {
        internal static List<SO> GetSOsForGroup(GROUP group)
        {
            List<SO> results = new List<SO>();
            if (group != null)
            {
                var potentialTags = AssetDatabase.LoadAllAssetRepresentationsAtPath(AssetDatabase.GetAssetPath(group));
                for (int i = 0; i < potentialTags.Length; ++i)
                {
                    if (potentialTags[i] is SO) results.Add(potentialTags[i] as SO);
                }
            }
            return results;
        }
        internal static SO CreateSO(GROUP group, string tagName, bool createIfAlreadyExists = false)
        {
            return BTaggedSOPropertyDrawerBase<GROUP, SO>.CreateAsset(group, tagName, createIfAlreadyExists );
        }
        internal static SO CreateSO(string groupPath, string tagName, bool createIfAlreadyExists = false)
        {
            return BTaggedSOPropertyDrawerBase<GROUP, SO>.CreateAsset(groupPath, tagName, createIfAlreadyExists );
        }
    }
}
