﻿/* Copyright(c) Tim Watts, Box of Clicks - All Rights Reserved */

using BOC.BTagged.Shared;
using System.IO;
using UnityEditor;
using UnityEditor.Compilation;
using UnityEngine;

namespace BOC.BTagged.EditorTools
{
    [CustomEditor(typeof(BTaggedGroupBase<>), true)]
    [CanEditMultipleObjects]
    public class BTaggedGroupEditor : Editor
    {
        private void ShowSettings()
        {
            SettingsService.OpenUserPreferences("Preferences/BTaggedSettings");
        }
        private void OnEnable()
        {
            EditorApplication.projectWindowItemOnGUI += HandleProjectWindowItemOnGUI;
        }
        private void OnDestroy()
        {
            EditorApplication.projectWindowItemOnGUI -= HandleProjectWindowItemOnGUI;
        }

        private void HandleProjectWindowItemOnGUI(string guid, Rect selectionRect)
        {
            if (Event.current.type == EventType.ExecuteCommand && Event.current.commandName == "Duplicate")
            {
                BTaggedAssetPostProcessor.DuplicationOccurred = true;
                EditorApplication.delayCall += () => BTaggedAssetPostProcessor.DuplicationOccurred = false;
            }
        }

        Vector2 scrollPos = Vector2.zero;
        public override void OnInspectorGUI()
        {
            EditorGUILayout.HelpBox(target + "\nSelect any of the children (sub assets) to see where they are used.\n\nCreate a new Group via the BTagged drop-down menu or by Right-Clicking in the project window.", MessageType.Info);
            (target as BTaggedGroupBase).CheckNotEmpty();

            EditorGUILayout.LabelField("Group children:");
            Rect r = EditorGUILayout.GetControlRect(false, 0);
            r.y += EditorGUIUtility.standardVerticalSpacing;
            r.height = 200;
            EditorGUI.DrawRect(r, new Color(0, 0, 0, 0.1f));

            scrollPos = EditorGUILayout.BeginScrollView(scrollPos, GUILayout.Height(200));
            GUIContent pickerBtnContent = new GUIContent(EditorGUIUtility.FindTexture("Record Off@2x"), "Click to select the asset associated with this " + target.name);
            GUIStyle pickerBtnStyle = new GUIStyle(GUI.skin.button);
            pickerBtnStyle.alignment = TextAnchor.MiddleLeft;
            pickerBtnStyle.padding = new RectOffset(3, 3, 3, 3);
            pickerBtnStyle.fixedHeight = EditorGUIUtility.singleLineHeight;
            string localPath = BTaggedSharedUtils.GetASMDEFDirectory(@"BOC.BTagged.Editor");
            var subAssets = AssetDatabase.LoadAllAssetRepresentationsAtPath(AssetDatabase.GetAssetPath(target));
            for (int s = 0; s < subAssets.Length; ++s)
            {
                if (subAssets[s] != null && subAssets[s] is BTaggedSOBase)
                {
                    EditorGUILayout.BeginHorizontal();
                    pickerBtnContent.text = "  " + subAssets[s].name;
                    pickerBtnContent.tooltip = "Click to select " + subAssets[s];
                    if (GUILayout.Button(pickerBtnContent, pickerBtnStyle))
                    {
                        Selection.activeObject = (subAssets[s] as BTaggedSOBase);
                    }
                    if (GUILayout.Button(AssetDatabase.LoadAssetAtPath<Texture2D>(Path.Combine(localPath, "BTagged_Delete.png")), GUILayout.Width(20), GUILayout.Height(20)))
                    {
                        BTaggedSOPropertyDrawerBase<BTaggedGroupBase<BTaggedSOBase>, BTaggedSOBase>.TryDeleteSO(subAssets[s] as BTaggedSOBase);
                    }
                    //EditorGUILayout.LabelField(subAssets[s].name);
                    //if (GUILayout.Button(subAssets[s].name)) Selection.activeObject = (subAssets[s] as BTaggedSOBase);
                    EditorGUILayout.EndHorizontal();
                }
            }
            EditorGUILayout.EndScrollView();
            //EditorGUILayout.BeginHorizontal();
            //GUILayout.FlexibleSpace();
            GUIContent lbl = new GUIContent("BTagged Settings", EditorGUIUtility.FindTexture("_Popup@2x"));
            EditorGUILayout.Space();
            if (GUILayout.Button(lbl)) ShowSettings();
            //GUILayout.FlexibleSpace();
            //EditorGUILayout.EndHorizontal();

        }
    }
}
