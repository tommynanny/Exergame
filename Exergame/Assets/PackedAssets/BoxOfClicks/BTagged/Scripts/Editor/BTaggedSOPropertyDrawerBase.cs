﻿/* Copyright(c) Tim Watts, Box of Clicks - All Rights Reserved */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using static BOC.BTagged.Shared.BTaggedSharedSettings;

namespace BOC.BTagged.EditorTools
{
    public abstract class BTaggedSOPropertyDrawerBase : PropertyDrawer
    {
        [NonSerialized]
        // Used for a visual highlight to help editor navigation of assets
        public static int SentFromInstanceId = 0;
    }

    [CanEditMultipleObjects]
    public abstract class BTaggedSOPropertyDrawerBase<GROUP, SO> : BTaggedSOPropertyDrawerBase
        where GROUP : BTaggedGroupBase<SO>
        where SO : BTaggedSOBase
    {
        static bool Initialized = false;
        static BTaggerDropDownMenuWindow menuPopup;
        static List<(GROUP group, SO asset)> foundAssets = new List<(GROUP, SO)>();
        static List<(string label, int index, bool editable)> menuEntries = new List<(string, int, bool editable)>();

        // This is quite unfortunate.
        // It seems creating an asset can cause the underlying serializedProperty to update and consequently cause the property drawer to be rebuilt.
        // Therefore something like a local bool will be lost during an operation such as add or duplicate.
        // Consequently we need to store and lookup whether to edit and what the edit string is through a combination of the serializedObject and property path.
        // 'serializedProperty' and 'this' cannot be used as keys as they will be unique upon rebuild. Thus: 
        // key = serializedProperty.serializedObject, serializedProperty.propertyPath
        static internal Dictionary<(SerializedObject, string), bool> propertyDrawerShouldEdit = new Dictionary<(SerializedObject, string), bool>();
        static internal Dictionary<(SerializedObject, string), bool> propertyDrawerIsEditing = new Dictionary<(SerializedObject, string), bool>();
        static internal Dictionary<(SerializedObject, string), string> propertyDrawerEditString = new Dictionary<(SerializedObject, string), string>();
        static internal Dictionary<(SerializedObject, string), float> propertyDrawerHelpHeight = new Dictionary<(SerializedObject, string), float>();

        bool shouldEdit
        {
            set { if (serializedProperty.serializedObject != null) propertyDrawerShouldEdit[(serializedProperty.serializedObject, serializedProperty.propertyPath)] = value; }
            get => serializedProperty.serializedObject == null ? default : propertyDrawerShouldEdit.TryGetValue((serializedProperty.serializedObject, serializedProperty.propertyPath), out var result) ? result : default;
        }
        bool isEditing
        {
            set { if(serializedProperty.serializedObject != null) propertyDrawerIsEditing[(serializedProperty.serializedObject, serializedProperty.propertyPath)] = value; }
            get => serializedProperty.serializedObject == null ? default : propertyDrawerIsEditing.TryGetValue((serializedProperty.serializedObject, serializedProperty.propertyPath), out var result) ? result : default;
        }
        string editString
        {
            set { if (serializedProperty.serializedObject != null) propertyDrawerEditString[(serializedProperty.serializedObject, serializedProperty.propertyPath)] = value; }
            get => serializedProperty.serializedObject == null ? default : propertyDrawerEditString.TryGetValue((serializedProperty.serializedObject, serializedProperty.propertyPath), out var result) ? result : default;
        }
        float HelpTextHeight
        {
            set { if (serializedProperty.serializedObject != null) propertyDrawerHelpHeight[(serializedProperty.serializedObject, serializedProperty.propertyPath)] = value; }
            get => serializedProperty.serializedObject == null ? default : propertyDrawerHelpHeight.TryGetValue((serializedProperty.serializedObject, serializedProperty.propertyPath), out var result) ? result : default;
        }

        SerializedProperty serializedProperty;
        protected string label;
        protected string defaultLabel = "-";

        bool editingCategory = false;
        bool renamingMainAsset = false;
        List<int> assetIndiciesToEdit = new List<int>();

        ~BTaggedSOPropertyDrawerBase()
        {
            var key = (serializedProperty.serializedObject, serializedProperty.propertyPath);
            if (propertyDrawerShouldEdit.ContainsKey(key)) propertyDrawerShouldEdit.Remove(key);
            if (propertyDrawerIsEditing.ContainsKey(key)) propertyDrawerIsEditing.Remove(key);
            if (propertyDrawerEditString.ContainsKey(key)) propertyDrawerEditString.Remove(key);
            if (propertyDrawerHelpHeight.ContainsKey(key)) propertyDrawerHelpHeight.Remove(key);
            SentFromInstanceId = 0;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) + (serializedProperty != null ? HelpTextHeight : 0);
        }

        const string generalHelpText_1 = "Now that you have a Tag selected you can find this GameObject easily in code.\n\nFor example, add (to your own MonoBehaviour):\n<b>public BOC.BTagged.Tag myTag;</b>\n\nThen also set that tag in the inspector to \n<b>";
        const string generalHelpText_2 = "</b>\n\nLastly you can then use the API to find the GameObject. E.g.:\n<b>BTagged.Find(myTag).GetFirstGameObject();</b>\n\nFor more documentation see <b><color=lightblue>boxofclicks.com/btagged</color></b>";
        const string noTagSlectedHelpText = "<b>Untagged</b> means there is no Tag associated with this component and so it will be ignored completely.\n\nChoose a Tag or create a new one using the dropdown menu below.";
        const string noTagsHelpText = "To create your first tag, click the dropdown and choose 'New Group'.\n\nThe part before the '/' represents the Group and the last part is the name of the Tag.";
        const float OpenButtonWidth = 20f;
        const float HelpButtonWidth = 20f;

        float MaxLabelWidth = 80f;
        float labelWidth = 80f;
        Rect dropDownRect;
        GUIStyle helpStyle;
        float helpBackgroundBorder = 5;
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent lbl)
        {
            string origLabel = lbl.text;
            serializedProperty = property;
            SO currentSO = serializedProperty.objectReferenceValue as SO;
            Init();
            if(currentSO == null || (!AssetDatabase.IsSubAsset(currentSO) && !AssetDatabase.IsMainAsset(currentSO)))
            {
                if (defaultSO == null) FindAllAssets();
                if (defaultSO != null) SetValue(defaultSO);
                currentSO = defaultSO;
            }

            bool tagsExists = menuEntries.Count > 2;
            GUIContent helpTxt = GUIContent.none;
            if (HelpTextHeight > 0 || !tagsExists)
            {
                helpStyle = new GUIStyle(GUI.skin.label);
                helpStyle.wordWrap = true; helpStyle.richText = true;
                helpTxt = new GUIContent(!tagsExists ? noTagsHelpText : (currentSO == null || currentSO.IsDefault ? noTagSlectedHelpText : (generalHelpText_1 + currentSO.name + generalHelpText_2)));
                float helpTxtWidth = position.width - (helpBackgroundBorder * 2f) - 20f;
                if (Event.current.type == EventType.Repaint) HelpTextHeight = Mathf.Round(helpStyle.CalcHeight(helpTxt, helpTxtWidth) + 20f);

                Rect helpTxtRect = new Rect(position);
                helpTxtRect.x -= helpBackgroundBorder;
                helpTxtRect.y += helpBackgroundBorder;
                helpTxtRect.height = HelpTextHeight - 10;
                EditorGUI.DrawRect(helpTxtRect, new Color(0.5f, 0.5f, 0.5f, 0.2f));
                Rect iconRect = new Rect(helpTxtRect);
                iconRect.x += 5f; iconRect.y += 7f;
                iconRect.width = iconRect.height = 14f;
                GUI.DrawTexture(iconRect, EditorGUIUtility.FindTexture("console.infoicon@2x"));
                helpTxtRect.y -= helpBackgroundBorder;
                helpTxtRect.height = HelpTextHeight;
                helpTxtRect.width = helpTxtWidth;
                helpTxtRect.x += helpBackgroundBorder + 20;
                EditorGUI.SelectableLabel(helpTxtRect, helpTxt.text, helpStyle);
                position.y += HelpTextHeight;
                position.height -= HelpTextHeight;
            }
            Rect editRect = new Rect(position);
            if (Event.current.type == EventType.Repaint)
            {
                MaxLabelWidth = (position.width - 150f);
            }

            // This line is required - at some point over the last few lines this label text changes 
            // Possibly due to a content change during a layout phase or similar
            lbl.text = origLabel;
            float widthForLabel = EditorStyles.textField.CalcSize(lbl).x + 10f;
            if (widthForLabel > MaxLabelWidth)
            {
                int idx = lbl.text.Length - Mathf.Clamp(Mathf.RoundToInt(MaxLabelWidth / widthForLabel * lbl.text.Length) - 1, 8, lbl.text.Length - 1);
                lbl.text = lbl.text.Substring(0, 3) + ".." + lbl.text.Substring(idx + 3);
            }
            labelWidth = Mathf.Min(widthForLabel, MaxLabelWidth);
            float buttonLabelWidth = (labelWidth + OpenButtonWidth + 10);
            editRect.x += buttonLabelWidth;
            editRect.y += 1;
            editRect.width -= buttonLabelWidth + HelpButtonWidth + 5;

            float totalWidth = position.width;
            position.width = OpenButtonWidth;

            GUIStyle lblStyle = new GUIStyle(GUI.skin.label);
            lblStyle.richText = true;

            GUIContent pickerBtnContent = new GUIContent(EditorGUIUtility.FindTexture("Record Off@2x"), "Click to select the asset associated with this " + label);
            GUIStyle pickerBtnStyle = new GUIStyle(GUI.skin.button);
            pickerBtnStyle.padding = new RectOffset(0, 0, 3, 3);
            Color defaultGUIColor = GUI.color;

            if (currentSO != null && SentFromInstanceId == currentSO.GetInstanceID()) GUI.color = Color.yellow;
            if (GUI.Button(position, pickerBtnContent, pickerBtnStyle))
            {
                if (property.objectReferenceValue != null)
                {
                    BTaggedSOEditor.SentFromInstanceId = serializedProperty.serializedObject.targetObject.GetInstanceID();
                    Selection.activeObject = property.objectReferenceValue;
                }
            }
            GUI.color = defaultGUIColor;
            position.x += position.width + 6f;
            position.width = labelWidth;
            lbl.tooltip = origLabel + "\nClick to ping the asset associated with this " + label;
            if (GUI.Button(position, lbl, GUI.skin.label))
            {
                if (property.objectReferenceValue != null) EditorGUIUtility.PingObject(property.objectReferenceValue);
            }

            position.x += position.width;
            position.y += 1f;
            position.width = totalWidth - labelWidth - OpenButtonWidth - HelpButtonWidth - 10f;

            string tfControlName = "edit_tf";
            if (shouldEdit)
            {
                GUI.SetNextControlName(tfControlName);
                editString = EditorGUI.TextField(editRect, editString);
                if (!isEditing)
                {
                    switch (Event.current.type)
                    {
                        case EventType.Layout:
                            EditorGUI.FocusTextInControl(tfControlName);
                            break;
                        case EventType.Repaint:
                            TextEditor tEditor = GetCurrTextEditor();
                            if (tEditor != null)
                            {
                                isEditing = true;
                                float txtW = EditorStyles.textField.CalcSize(new GUIContent(editString)).x;
                                tEditor.selectIndex = editString.LastIndexOf("/") + 1;
                                tEditor.cursorIndex = editString.Length;
                                tEditor.scrollOffset = new Vector2(txtW - tEditor.position.width + 1, 0);
                            }
                            break;
                    }
                }
                if ((Event.current.keyCode == KeyCode.Return || Event.current.keyCode == KeyCode.KeypadEnter || GUI.GetNameOfFocusedControl() != tfControlName))
                {
                    shouldEdit = false;
                    isEditing = false;
                    TryApplyEdit();
                }
                else if (Event.current.keyCode == KeyCode.Escape)
                {
                    shouldEdit = false;
                    isEditing = false;
                }
            }
            else
            {

                GUIContent content = new GUIContent(currentSO == null ? defaultLabel : currentSO.name);
                content.tooltip = content.text + "\nClick to choose a different " + label;
                float widthForDropDown = EditorStyles.textField.CalcSize(content).x + 10f;
                int lblLength = content.text.Length;
                if (widthForDropDown > (position.width - 15f))
                {
                    int idx = content.text.Length - Mathf.Clamp(Mathf.RoundToInt((position.width - 15f) / widthForDropDown * lblLength) - 1, 8, lblLength - 1);
                    if (idx > 5 && lblLength > 5) content.text = content.text.Substring(0, 5) + ".." + content.text.Substring(idx + 5);
                }
                bool pressed = EditorGUI.DropdownButton(position, content, FocusType.Keyboard);
                if (pressed)
                {
                    FindAllAssets();
                    dropDownRect = EditorGUIUtility.GUIToScreenRect(position);
                    ShowPopup(dropDownRect);
                }
            }

            position.x += position.width + 5;
            position.width = HelpButtonWidth;
            if (GUI.Button(position, new GUIContent("?", "Toggle Help")))
            {
                if (HelpTextHeight > 0)
                {
                    HelpTextHeight = 0;
                }
                else
                {
                    HelpTextHeight = 1f;
                }
            }
        }

        // Just WOW
        // Not only is *this* the way to change selection but we have to use reflection for EditorGUI.TextField
        // Although we could use GUI.TextField, text doesn't scroll as you type or allow copy/paste with one of those so here we are
        // Oh and it's not available on the first frame but we only want to override the selction on the frame we started editing
        // so there's that too. 
        TextEditor GetCurrTextEditor()
        {
            return typeof(EditorGUI)
                .GetField("activeEditor", BindingFlags.Static | BindingFlags.NonPublic)
                .GetValue(null) as TextEditor;
        }

        const float MinPopupWidth = 270f;
        public void ShowPopup(Rect popupButtonRect)
        {
            if (menuEntries == null || menuEntries.Count < 1) FindAllAssets();
            if (menuPopup == null)
            {
                menuPopup = EditorWindow.CreateInstance<BTaggerDropDownMenuWindow>();
            }
            else
            {
                menuPopup.Close();
                menuPopup = EditorWindow.CreateInstance<BTaggerDropDownMenuWindow>();
            }

            Rect popupWindowSize = new Rect(popupButtonRect);
            if (popupWindowSize.width < MinPopupWidth)
            {
                popupButtonRect.x += popupWindowSize.width - MinPopupWidth;
                popupWindowSize.width = MinPopupWidth;
            }
            SO currentSO = serializedProperty.objectReferenceValue as SO;
            if (currentSO != null)
            {
                GROUP grp = AssetDatabase.IsMainAsset(currentSO) ? null : AssetDatabase.LoadMainAssetAtPath(AssetDatabase.GetAssetPath(currentSO)) as GROUP;
                menuPopup.SelectedEntry = foundAssets.IndexOf((grp, currentSO));
            }
            else
            {
                menuPopup.SelectedEntry = -1;
            }

            // Arrays call the same property drawer multiple times so we need the 
            // popup window to return the property it was actually called for
            menuPopup.RelatedProperty = serializedProperty;
            menuPopup.OnAddCategory = HandleAddCategory;
            menuPopup.OnEditCategory = HandleEditCategory;
            menuPopup.OnDeleteCategory = HandleDeleteCategory;
            menuPopup.OnSelect = HandleSelect;
            menuPopup.OnEdit = HandleEdit;
            menuPopup.OnDuplicate = HandleDuplicate;
            menuPopup.OnDelete = HandleDelete;
            menuPopup.Build(menuEntries, popupWindowSize.width);
            menuPopup.ShowAsDropDown(popupButtonRect, new Vector2(popupWindowSize.width, 200f));
            menuPopup.Focus();
        }

        #region Selection
        private void HandleSelect(SerializedProperty prop, int menuIdx)
        {
            serializedProperty = prop;
            if (serializedProperty.serializedObject.targetObject == null) Debug.LogWarning("No targetobj for " + serializedProperty.serializedObject);
            Undo.RecordObject(serializedProperty.serializedObject.targetObject, "Change " + label);
            if (menuIdx < 0 || menuIdx >= foundAssets.Count)
            {
                SetToEmpty();
                return;
            }
            SetValue(foundAssets[menuIdx].asset);
        }

        private void SetValue(SO asset)
        {
            serializedProperty.objectReferenceValue = asset;
            serializedProperty.serializedObject.ApplyModifiedProperties();
            if (menuPopup != null) menuPopup.Close();
        }
        #endregion

        #region Adding
        private void HandleAddCategory(SerializedProperty prop, int menuIdx, string categoryLabel)
        {
            serializedProperty = prop;
            if (menuIdx < 0 || menuIdx >= foundAssets.Count) return;
            SetValue(foundAssets[menuIdx].asset);
            AddNew(AssetDatabase.GetAssetPath(foundAssets[menuIdx].group), foundAssets[menuIdx].asset != null ? foundAssets[menuIdx].asset.name : "New");
            FindAllAssets();
        }

        //Group path is set when 'New' is clicked within a groups popup menu
        internal void AddNew(string groupPath = "", string existingName = "")
        {
            if (menuPopup != null) menuPopup.Close();

            SO currentSO = serializedProperty.objectReferenceValue as SO;
            GROUP soGroup = (currentSO != null && !AssetDatabase.IsMainAsset(currentSO)) ? AssetDatabase.LoadMainAssetAtPath(AssetDatabase.GetAssetPath(currentSO)) as GROUP : null;

            SO newSO = CreateAsset(soGroup, existingName);

            if(newSO != null)
            {
                // Select the newly created asset
                SetValue(newSO);

                // Immediately make it available to be renamed
                SetEdit(soGroup, newSO);
            }
        }

        static internal SO CreateAsset(string groupPath = "", string soName = "", bool createIfAlreadyExists = false)
        {
            if (string.IsNullOrEmpty(groupPath)) groupPath = "Assets/NewGroup";
            bool addAssetExt = !Path.HasExtension(groupPath);
            GROUP soGroup = AssetDatabase.LoadMainAssetAtPath(groupPath + (addAssetExt ? ".asset" : string.Empty)) as GROUP;

            if (soGroup == null)
            {
                soGroup = ScriptableObject.CreateInstance<GROUP>();
                soGroup.name = GetNextAvailableName("", "NewGroup");
                AssetDatabase.CreateAsset(soGroup, groupPath + (addAssetExt ? ".asset" : string.Empty));
            }
            return (soGroup != null ? CreateAsset(soGroup, soName, createIfAlreadyExists) : null);
        }
        static internal SO CreateAsset(GROUP soGroup, string soName = "", bool createIfAlreadyExists = false)
        {
            if (soGroup == null) return CreateAsset(string.Empty, soName, createIfAlreadyExists);
            if(!createIfAlreadyExists)
            {
                var existing = BTaggedEditorUtils<GROUP, SO>.GetSOsForGroup(soGroup).FirstOrDefault(x => x.name.ToLower() == soName.ToLower());
                if (existing != null) return existing;
            }
            string newAssetName = GetNextAvailableName(AssetDatabase.GetAssetPath(soGroup), soName);

            // Create the new asset & reimport it to force update
            SO newSO = ScriptableObject.CreateInstance<SO>();
            newSO.name = newAssetName;
            AssetDatabase.AddObjectToAsset(newSO, soGroup);
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(soGroup));
            return newSO;
        }
        #endregion

        #region Duplication
        private void HandleDuplicate(SerializedProperty prop, int menuIdx)
        {
            serializedProperty = prop;
            if (menuIdx < 0 || menuIdx >= foundAssets.Count) return;
            SetValue(foundAssets[menuIdx].asset);
            AddNew(AssetDatabase.GetAssetPath(foundAssets[menuIdx].group), foundAssets[menuIdx].asset.name);
        }
        #endregion

        #region Editing
        private void HandleEdit(SerializedProperty prop, int menuIdx)
        {
            serializedProperty = prop;
            if (menuIdx < 0 || menuIdx >= foundAssets.Count) return;
            SetValue(foundAssets[menuIdx].asset);
            SetEdit(foundAssets[menuIdx].group, foundAssets[menuIdx].asset);
            FindAllAssets();
        }

        GROUP workingGroup;
        private void HandleEditCategory(SerializedProperty prop, int menuIdx, int depth, string categoryLabel)
        {
            serializedProperty = prop;
            if (menuPopup != null) menuPopup.Close();

            editingCategory = true;

            assetIndiciesToEdit.Clear();
            FindmatchingAssets(menuIdx, depth, ref assetIndiciesToEdit);
            renamingMainAsset = false;
            if (assetIndiciesToEdit.Count > 0)
            {
                GROUP group = foundAssets[menuIdx].group;
                workingGroup = group;
                string assetName = foundAssets[menuIdx].asset.name;
                string[] splitLabels = assetName.Split('/');
                for (int i = 1; i < splitLabels.Length; ++i) splitLabels[i] = splitLabels[i - 1] + "/" + splitLabels[i];
                string subDirName = string.Empty;
                if (depth > 0 && splitLabels.Length > 1 && depth <= splitLabels.Length) subDirName = splitLabels[depth - 1];

                if (subDirName == string.Empty)
                {
                    editString = group != null ? group.name : string.Empty;
                    renamingMainAsset = true;
                }
                else
                {
                    editString = (group != null ? group.name + "/" : string.Empty) + subDirName;
                }
                //editString = partToEdit;
                shouldEdit = true;
            }
        }

        private void SetEdit(GROUP soGroup, SO currentSO)
        {
            if (currentSO != null)
            {
                shouldEdit = true;
                editString = (soGroup != null ? soGroup.name + "/" : string.Empty) + currentSO.name;
            }
        }

        private void TryApplyEdit()
        {
            Undo.SetCurrentGroupName("Renaming " + label + "s");
            if (editingCategory)
            {
                string newName = editString;
                if (renamingMainAsset)
                {
                    RenameMainAsset(workingGroup, newName);
                }
                else
                {
                    for (int i = 0; i < assetIndiciesToEdit.Count; ++i)
                    {
                        SO asset = foundAssets[assetIndiciesToEdit[i]].asset;
                        int lastDirIdx = Mathf.Max(0, asset.name.LastIndexOf("/"));

                        //string start = asset.name.Substring(0, subDirName.Length);
                        //string end = asset.name.Substring(partToEdit.Length, asset.name.Length - partToEdit.Length);
                        //string newName = soTextField.value;// + end;
                        //Debug.LogWarning("Renaming " + asset.name + " to " + newName + asset.name.Substring(lastDirIdx));
                        RenameSubAsset(workingGroup, asset, newName + asset.name.Substring(lastDirIdx));

                    }
                }
                editingCategory = false;
            }
            else
            {
                SO currentSO = serializedProperty.objectReferenceValue as SO;
                if (currentSO == null) return;
                string newAssetName = editString;
                workingGroup = AssetDatabase.LoadAssetAtPath<GROUP>(AssetDatabase.GetAssetPath(currentSO));
                if (workingGroup != null)
                {
                    if (!newAssetName.Contains("/")) newAssetName += "/New";
                    string groupName = newAssetName.Substring(0, newAssetName.IndexOf("/"));
                    if (groupName != workingGroup.name)
                    {
                        workingGroup.name = groupName;
                        RenameMainAsset(workingGroup, groupName);
                    }
                }

                RenameSubAsset(
                    AssetDatabase.LoadAssetAtPath<GROUP>(AssetDatabase.GetAssetPath(currentSO)),
                    currentSO,
                    newAssetName);
            }

            isEditing = false;
            FindAllAssets();
        }
        #endregion

        #region Deleting
        void HandleDelete(SerializedProperty prop, int menuIdx)
        {
            serializedProperty = prop;
            if (menuIdx < 0 || menuIdx >= foundAssets.Count) return;
            var delRslt = TryDeleteSO(foundAssets[menuIdx].asset, serializedProperty);
            FindAllAssets();
            if (delRslt) ShowPopup(dropDownRect);
        }

        void HandleDeleteCategory(SerializedProperty prop, int menuIdx, int depth, string categoryLabel)
        {
            int numToDelete = 0;
            int numDeleted = 0;
            serializedProperty = prop;
            List<int> assetIndiciesToDelete = new List<int>();
            FindmatchingAssets(menuIdx, depth, ref assetIndiciesToDelete);

            for (int i = 0; i < assetIndiciesToDelete.Count; ++i)
            {
                if (assetIndiciesToDelete[i] < 0 && depth == 0 && foundAssets[menuIdx].group != null)
                {
                    for (int a = 0; a < foundAssets.Count; ++a)
                    {
                        if (foundAssets[a].group == foundAssets[menuIdx].group)
                        {
                            numToDelete++;
                            if (TryDeleteSO(foundAssets[a].asset, serializedProperty)) numDeleted++;
                        }
                    }
                }
                else
                {
                    numToDelete++;
                    if (TryDeleteSO(foundAssets[assetIndiciesToDelete[i]].asset, serializedProperty)) numDeleted++;
                }
            }

            if (numDeleted == numToDelete)
            {
                ShowNotification("Deleted " + categoryLabel);
            }
            else if (numDeleted > 0)
            {
                ShowNotification("Deleted " + numDeleted + "/" + numToDelete + " items in " + categoryLabel);
            }
            if (numDeleted == numToDelete && depth == 0)
            {
                if (foundAssets[menuIdx].group != null) DeleteAsset(foundAssets[menuIdx].group, serializedProperty);
            }
            if (menuPopup != null) menuPopup.Close();
            FindAllAssets();
            ShowPopup(dropDownRect);
        }

        static public bool TryDeleteSO(SO asset, SerializedProperty serializedProperty = null)
        {
            bool success = true;
            if (asset != null)
            {
                success = CanDelete(asset);
                if (success) DeleteAsset(asset, serializedProperty);
            }
            return success;
        }

        static private bool CanDelete(SO asset)
        {
            if (asset == null) return false;
            List<SceneObjectIDBundle> assetReferences = new List<SceneObjectIDBundle>();
            BTaggedSORegistry.References(asset, ref assetReferences, SearchRegistryOption.FullRefresh);

            if (assetReferences.Count > 0)
            {
                var showReferences = EditorUtility.DisplayDialog("Warning", asset.name + " has " + assetReferences.Count + " references in this project. Would you like to see them?", "Yes", "No");
                if (showReferences)
                {
                    if (menuPopup != null) menuPopup.Close();
                    Selection.activeObject = asset;
                    return false;
                }
                string msg = "This can't be undone. Any remaining references to " + asset.name + " in your project will become invalid.\nAre you really sure you wish to delete it?";
                bool shouldDelete = !EditorUtility.DisplayDialog(asset.name + " uses", msg, "No", "Yes");

                if (!shouldDelete)
                {
                    GROUP group = AssetDatabase.LoadMainAssetAtPath(AssetDatabase.GetAssetPath(asset)) as GROUP;
                    msg = "<b>" + (group != null && group != asset ? group.name + "/" : string.Empty) + asset.name + "</b> is referenced in the following locations:\n";
                    msg += "<color=yellow>Warning:</color> Automated search is experimental and is not guaranteed to catch 100% of occurances. In particular Prefab overrides.\n\n";
                    for (int a = 0; a < Math.Min(100, assetReferences.Count); ++a)
                    {
                        msg += "<b>" + assetReferences[a].scenePath + ": " + assetReferences[a].objectName + "</b>\n";
                    }
                    msg += "\n";
                    Debug.LogWarning(msg);
                }
                return shouldDelete;
            }

            return true;
        }

        static private void DeleteAsset(ScriptableObject asset, SerializedProperty serializedProperty = null)
        {
            string assetPath = AssetDatabase.GetAssetPath(asset);
            if (AssetDatabase.IsMainAsset(asset))
            {
                AssetDatabase.DeleteAsset(assetPath);
            }
            else
            {
                AssetDatabase.RemoveObjectFromAsset(asset);
                AssetDatabase.ImportAsset(assetPath);
            }
            if(serializedProperty != null)
            {
                bool deletedAssetWasSelected = asset == serializedProperty.objectReferenceValue;
                if (deletedAssetWasSelected) SetToEmpty(serializedProperty);
            }
        }
        #endregion

        #region Utils
        static private void SetToEmpty(SerializedProperty serializedProperty = null)
        {
            serializedProperty.objectReferenceValue = defaultSO;
            serializedProperty.serializedObject.ApplyModifiedProperties();
            if (menuPopup != null) menuPopup.Close();
        }

        private void ShowNotification(string msg)
        {
            foreach (SceneView scene in SceneView.sceneViews)
            {
                scene.ShowNotification(new GUIContent(msg));
            }
        }

        private void FindmatchingAssets(int menuIdx, int depth, ref List<int> results)
        {
            GROUP group = foundAssets[menuIdx].group;

            if (group != null && depth == 0)
            {
                // If we're affecting an entire group - i.e. a main asset,
                // simply return an entry out of range and the main asset will be edited
                results.Add(-1);
                return;
            }
            string assetName = foundAssets[menuIdx].asset.name;
            string[] splitLabels = assetName.Split('/');
            for (int i = 1; i < splitLabels.Length; ++i) splitLabels[i] = splitLabels[i - 1] + "/" + splitLabels[i];
            string subDirName = string.Empty;
            if (depth > 0 && splitLabels.Length > 1 && depth <= splitLabels.Length) subDirName = splitLabels[depth - 1];
            for (int i = 0; i < foundAssets.Count; ++i)
            {
                if (foundAssets[i].asset == null) continue;
                string curAssetName = foundAssets[i].asset.name;
                bool matches = group != null && foundAssets[i].group == group && !assetName.Contains('/');
                if (curAssetName.StartsWith(subDirName)) matches = foundAssets[i].group == group;
                if (matches) results.Add(i);
            }
        }

        private void RenameMainAsset(GROUP group, string newName)
        {
            if (string.IsNullOrEmpty(newName)) return;

            Undo.RegisterCompleteObjectUndo(group, "Rename " + group.name + " to " + newName);

            newName = ReplaceInvalidChars(newName);
            string newMainAsset = newName.Split('/')[0];
            string remainingName = newName.Length > newMainAsset.Length ? newName.Substring(newMainAsset.Length + 1) : string.Empty;
            GROUP existingGroup = null;
            for (int a = 0; a < foundAssets.Count; ++a)
            {
                if (foundAssets[a].group != null && foundAssets[a].group != group && foundAssets[a].group.name.ToLower() == newMainAsset.ToLower())
                {
                    existingGroup = foundAssets[a].group;
                    break;
                }
            }

            bool moveSubAssets = false;

            // If moving all assets in a group to a different existing group
            if (existingGroup != null)
            {
                // This will potentially change guids for serialized assets 
                if (!CheckOKIfInvalidateReference(group, label)) return;
                moveSubAssets = true;
            }
            else
            {
                // If renaming the main name of the group, do that here
                AssetDatabase.SetMainObject(group, AssetDatabase.GetAssetPath(group));
                //Debug.LogWarning("Renaming " + group + " to " + newMainAsset);
                AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(group), newMainAsset);
            }

            // It's possible that an entire group could be renamed in the following way:
            // GroupA -> Group/A
            // In such a case, as well as renaming the main asset we also want to change the names
            // of all the subassets of GroupA
            if (remainingName.Length > 0 || moveSubAssets)
            {
                for (int a = 0; a < foundAssets.Count; ++a)
                {
                    if (foundAssets[a].group == group && foundAssets[a].asset != null)
                    {
                        //Debug.LogWarning("Renaming " + foundAssets[a].asset.name + " to " + remainingName + "/" + foundAssets[a].asset.name);
                        foundAssets[a].asset.name = remainingName + "/" + foundAssets[a].asset.name;

                        // Subassets are moving to a different main asset
                        // Doing so will change GUIDs for any serialized asset references
                        if (moveSubAssets)
                        {
                            var existingHash = foundAssets[a].asset.Hash;
                            AssetDatabase.RemoveObjectFromAsset(foundAssets[a].asset);
                            AssetDatabase.SetMainObject(existingGroup, AssetDatabase.GetAssetPath(existingGroup));
                            AssetDatabase.AddObjectToAsset(foundAssets[a].asset, AssetDatabase.GetAssetPath(existingGroup));
                            var prop = typeof(BTaggedSOBase).GetField("_Hash", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                            prop.SetValue(foundAssets[a].asset, existingHash);
                        }
                    }
                }
            }

            // Reimport asset to update project window
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(group));
            // Update names for dropdown
            FindAllAssets();
        }

        static internal void RenameSubAsset(ScriptableObject group, SO subAsset, string newName)
        {
            if (string.IsNullOrEmpty(newName)) return;

            Undo.RecordObject(subAsset, "Rename " + subAsset.name);

            newName = ReplaceInvalidChars(newName);

            if (group == null)
            {
                // If the asset has no group, it is a main asset and can be simply renamed
                //Debug.LogWarning("Renaming individual  asset " + subAsset.name + " to " + newName);
                AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(subAsset), newName);
            }
            else
            {
                var nameSlices = newName.Split('/');
                string newMainAsset = nameSlices[0];
                string remainingName = newName.Length > newMainAsset.Length ? newName.Substring(newMainAsset.Length + 1) : newName;

                if (newMainAsset == group.name)
                {
                    // If the subasset is still a member of it's original group then simply rename
                    //Debug.LogWarning("Renaming subasset " + subAsset.name + " to " + newName + " for " + group);
                    subAsset.name = newName.Substring(group.name.Length + 1);
                    AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(group), ImportAssetOptions.ForceUpdate);
                }
                else
                {
                    // Otherwise this asset is moving to a different asset
                    // This will potentially change guids for serialized assets 
                    if (!CheckOKIfInvalidateReference(group, "asset's")) return;

                    GROUP existingGroup = null;
                    GROUP newGROUP = null;
                    for (int a = 0; a < foundAssets.Count; ++a)
                    {
                        if (foundAssets[a].group != null && foundAssets[a].group.name.ToLower() == newMainAsset.ToLower())
                        {
                            existingGroup = foundAssets[a].group;
                            break;
                        }
                    }
                    string newGroupPath = string.Empty;
                    if (existingGroup != null)
                    {
                        newGroupPath = AssetDatabase.GetAssetPath(existingGroup);
                        newGROUP = existingGroup;
                    }
                    else
                    {
                        newGroupPath = AssetDatabase.GetAssetPath(group).Replace(group.name, newMainAsset);
                        newGROUP = ScriptableObject.CreateInstance<GROUP>();
                        newGROUP.name = newMainAsset;
                        newGROUP.hideFlags = HideFlags.None;
                        AssetDatabase.CreateAsset(newGROUP, newGroupPath);
                        AssetDatabase.ImportAsset(newGroupPath);
                    }
                    //Debug.LogWarning("Renaming individual  asset " + subAsset.name + " to " + remainingName);// + endOfAssetName);
                    subAsset.name = remainingName;// + endOfAssetName;
                    bool alreadyExists = false;
                    SO existingAsset = null;
                    if (existingGroup != null)
                    {
                        // If a subasset within an existing group has the same name, check whether user
                        // wants to merge them or keep two with identical names
                        var existingSubAssets = AssetDatabase.LoadAllAssetRepresentationsAtPath(newGroupPath);
                        for (int i = 0; i < existingSubAssets.Length; ++i)
                        {
                            if (existingSubAssets[i] is SO && existingSubAssets[i].name.ToLower() == subAsset.name.ToLower())
                            {
                                existingAsset = existingSubAssets[i] as SO;
                                alreadyExists = true;
                                break;
                            }
                        }
                    }
                    if (alreadyExists)
                    {
                        bool shouldHaveSameName = EditorUtility.DisplayDialog("Renaming " + subAsset.name, "An asset in " + group + " already has the same name: " + subAsset.name + ". Are you sure you want two with identical names?", "Yes", "No");
                        if (shouldHaveSameName) alreadyExists = false;
                    }
                    if (!alreadyExists)
                    {
                        var existingHash = subAsset.Hash;
                        AssetDatabase.RemoveObjectFromAsset(subAsset);
                        AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(group));
                        AssetDatabase.SetMainObject(newGROUP, newGroupPath);
                        AssetDatabase.AddObjectToAsset(subAsset, newGroupPath);
                        var prop = typeof(BTaggedSOBase).GetField("_Hash", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
                        prop.SetValue(subAsset, existingHash);

                        if(group is GROUP) (group as GROUP).CheckNotEmpty();
                        AssetDatabase.ImportAsset(newGroupPath);
                    }
                }
            }
        }

        static private string GetNextAvailableName(string groupPath, string defaultName)
        {
            string absolutePath = Application.dataPath + Path.DirectorySeparatorChar + groupPath;
            string[] existingNames;
            if (AssetDatabase.LoadMainAssetAtPath(groupPath) != null)
            {
                UnityEngine.Object[] allAssets;
                allAssets = AssetDatabase.LoadAllAssetsAtPath(groupPath);
                existingNames = new string[allAssets.Length];
                for (int a = 0; a < allAssets.Length; ++a) existingNames[a] = allAssets[a].name;
            }
            else
            {
                existingNames = Directory.GetFiles(absolutePath);
                for (int i = 0; i < existingNames.Length; ++i) existingNames[i] = Path.GetFileNameWithoutExtension(existingNames[i]);
            }
            return ObjectNames.GetUniqueName(existingNames, defaultName);
        }

        static char[] invalidChars = Path.GetInvalidFileNameChars().Where(x => !x.Equals('/')).ToArray();
        static public string ReplaceInvalidChars(string filename)
        {
            while (filename.Contains("//")) filename = filename.Replace("//", "/");
            if (filename.StartsWith("/")) filename = filename.Substring(1);
            return string.Join("_", filename.Split(invalidChars));
        }

        // BTagged serialized references to a base ScriptableObject (inheriting from BTaggedSOBase).
        // As names and categories are managed by manipulating Assets & Subassets it's important to note that:
        // If a subasset moves to another asset (i.e. it gets re-categorised) the GUID associated with the SO 
        // will change. This can lead to components referencing either un-intended or non-existent assets.
        // For now, we warn the user any time this might happen. 
        // The alternative would be an editor script that scrapes all scenes for references to the existing GUID and updates them.
        static private bool CheckOKIfInvalidateReference(UnityEngine.Object obj, string label)
        {
            return EditorUtility.DisplayDialog("Renaming " + obj.name, "Changing the " + label + @"'s main Group will cause it's GUID to change and all references to this asset in the project to be lost. Are you *sure* you wish to continue?", "Ok", "Cancel");
        }
        #endregion

        #region Static Find Assets
        static private void Init()
        {
            if (!Initialized)
            {
                Initialized = true;
                FindAllAssets();
                Undo.undoRedoPerformed -= FindAllAssets;
                Undo.undoRedoPerformed += FindAllAssets;
            }
        }
        static SO defaultSO;
        static internal void FindAllAssets()
        {
            menuEntries.Clear();
            foundAssets.Clear();

            string[] groupGuids = AssetDatabase.FindAssets("t:" + typeof(GROUP));
            for (int groupIndex = 0; groupIndex < groupGuids.Length; ++groupIndex)
            {
                string groupPath = AssetDatabase.GUIDToAssetPath(groupGuids[groupIndex]);
                string groupName = Path.GetFileNameWithoutExtension(groupPath) + "/";
                UnityEngine.Object[] allAssets = AssetDatabase.LoadAllAssetsAtPath(groupPath);
                List<string> uniqueNames = new List<string>();

                for (int i = 0; i < allAssets.Length; ++i)
                {
                    if (allAssets[i] == null) continue;
                    string name = groupName + (allAssets[i].name.Length >= 1 ? allAssets[i].name : " -- ");
                    int attempts = 0;
                    while (uniqueNames.Contains(name) && attempts < 100)
                    {
                        attempts++;
                        name = groupName + allAssets[i].name + " (" + attempts + ")";
                    }
                    uniqueNames.Add(name);
                    if (allAssets[i] is SO)
                    {
                        SO asset = allAssets[i] as SO;
                        foundAssets.Add((AssetDatabase.LoadMainAssetAtPath(groupPath) as GROUP, asset));
                        menuEntries.Add((name, foundAssets.Count, true));
                    }
                }
            }
            string[] individualSOGuids = AssetDatabase.FindAssets("t:" + typeof(SO));
            for (int i = 0; i < individualSOGuids.Length; ++i)
            {
                SO individualAsset = AssetDatabase.LoadMainAssetAtPath(AssetDatabase.GUIDToAssetPath(individualSOGuids[i])) as SO;
                if (individualAsset != null)
                {
                    if(individualAsset.IsDefault)
                    {
                        defaultSO = individualAsset;
                    }
                    else
                    {
                        foundAssets.Add((null, individualAsset));
                        menuEntries.Add((individualAsset.name, foundAssets.Count, true));
                    }
                }
            }

            menuEntries.Sort((x, y) => x.label.CompareTo(y.label));

            // Add -None- entry
            foundAssets.Insert(0, (null, null));
            menuEntries.Insert(0, ("- None -", -1, false));

            // Add Create New Group entry
            foundAssets.Add((null, null));
            menuEntries.Add(("New Group", foundAssets.Count - 1, false));
        }

        #endregion
    }
}
