﻿/* Copyright(c) Tim Watts, Box of Clicks - All Rights Reserved */

using BOC.BTagged.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.Compilation;
using UnityEditor.SceneManagement;
using UnityEngine;
using static BOC.BTagged.Shared.BTaggedSharedSettings;

namespace BOC.BTagged.EditorTools
{
    [CustomEditor(typeof(BTaggedSOBase), true)]
    [CanEditMultipleObjects]
    public class BTaggedSOEditor : Editor
    {
        [NonSerialized]
        // Used for a visual highlight to help editor navigation of assets
        public static int SentFromInstanceId = 0;

        static UnityEngine.Object lastSearchObject = null;
        List<SceneObjectIDBundle> openSceneReferences;
        List<SceneObjectIDBundle> prefabReferences;
        List<SceneObjectIDBundle> otherSceneReferences;

        private void ShowSettings()
        {
            SettingsService.OpenUserPreferences("Preferences/BTaggedSettings");
        }
        private void OnEnable()
        {
            if (lastSearchObject == null || lastSearchObject != target || !BTaggedSORegistry.HasCachedResults(target as BTaggedSOBase))
            {
                RefreshUses(SearchMode);
                lastSearchObject = target;
            }
            else
            {
                RefreshUses(SearchRegistryOption.CachedResultsOnly);
            }
            EditorApplication.projectWindowItemOnGUI += HandleProjectWindowItemOnGUI;
        }

        private void OnDestroy()
        {
            SentFromInstanceId = 0;
            lastOpenSceneRefPinged = -1;
            EditorApplication.projectWindowItemOnGUI -= HandleProjectWindowItemOnGUI;
        }

        private void HandleProjectWindowItemOnGUI(string guid, Rect selectionRect)
        {
            if (Event.current.type == EventType.ExecuteCommand && Event.current.commandName == "Duplicate")
            {
                BTaggedAssetPostProcessor.DuplicationOccurred = true;
                EditorApplication.delayCall += () => BTaggedAssetPostProcessor.DuplicationOccurred = false;
            }
        }

        bool refresh = false;
        int lastOpenSceneRefPinged = -1;
        public override void OnInspectorGUI()
        {
            Color defaultGUIColor = GUI.color;
            GUIContent pickerBtnContent = new GUIContent(EditorGUIUtility.FindTexture("Record Off@2x"), "Hover to Ping, click to Select");
            GUIStyle pickerBtnStyle = new GUIStyle(GUI.skin.button);
            pickerBtnStyle.padding = new RectOffset(0, 0, 3, 3);

            var mousePos = Event.current.mousePosition;
            if (openSceneReferences != null)
            {
                Rect r = new Rect();
                r.width = EditorGUIUtility.currentViewWidth;
                r.y += EditorGUIUtility.standardVerticalSpacing;
                r.height = EditorGUIUtility.standardVerticalSpacing + 2f * (EditorGUIUtility.singleLineHeight);
                GUILayoutUtility.GetRect(r.width, EditorGUIUtility.singleLineHeight * 0.4f);
                EditorGUI.DrawRect(r, new Color(0, 0, 0, 0.1f));

                EditorGUILayout.BeginHorizontal();
                var group = AssetDatabase.LoadMainAssetAtPath(AssetDatabase.GetAssetPath(target)) as BTaggedGroupBase;
                string existingGroupName = (group == null ? string.Empty : group.name + "/");
                GUIContent groupLabel = new GUIContent(existingGroupName);

                float defaultLabelWidth = EditorGUIUtility.labelWidth;
                EditorGUIUtility.labelWidth = GUI.skin.textField.CalcSize(groupLabel).x;
                EditorGUILayout.PrefixLabel(groupLabel);
                EditorGUIUtility.labelWidth = defaultLabelWidth;
                string newAssetName = EditorGUILayout.TextField(target.name);

                if(newAssetName != target.name)
                {
                    BTaggedSOPropertyDrawerBase<BTaggedGroupBase<BTaggedSOBase>, BTaggedSOBase>.RenameSubAsset(group, target as BTaggedSOBase, existingGroupName + newAssetName);
                }
                if(group != null)
                {
                    string localPath = BTaggedSharedUtils.GetASMDEFDirectory(@"BOC.BTagged.Editor");
                    if (GUILayout.Button(AssetDatabase.LoadAssetAtPath<Texture2D>(Path.Combine(localPath, "BTagged_Delete.png")), GUILayout.Width(20), GUILayout.Height(20)))
                    {
                        if (group != null) Selection.activeObject = group;
                        BTaggedSOPropertyDrawerBase<BTaggedGroupBase<BTaggedSOBase>, BTaggedSOBase>.TryDeleteSO(target as BTaggedSOBase);
                    }
                }
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.Space();
                GUIStyle lblStyle = new GUIStyle(GUI.skin.label);
                lblStyle.richText = true;

                if (openSceneReferences.Count > 0)
                {
                    EditorGUILayout.Space();
                    EditorGUILayout.LabelField("<b><color=cyan>In Open Scenes:</color> <color=yellow>" + openSceneReferences.Count + "</color> reference(s)</b>", lblStyle);
                    EditorGUILayout.Space();
                    for (int i = 0; i < openSceneReferences.Count; ++i)
                    {
                        EditorGUILayout.BeginHorizontal();
                        if (SentFromInstanceId == openSceneReferences[i].id) GUI.color = Color.yellow;
                        if (GUILayout.Button(pickerBtnContent, pickerBtnStyle, GUILayout.Width(20), GUILayout.Height(20)))
                        {
                            Selection.activeInstanceID = openSceneReferences[i].id;
                            BTaggedSOPropertyDrawerBase.SentFromInstanceId = target.GetInstanceID();
                        }
                        if(GUILayoutUtility.GetLastRect().Contains(mousePos) && lastOpenSceneRefPinged != i)
                        {
                            lastOpenSceneRefPinged = i;
                            EditorGUIUtility.PingObject(openSceneReferences[i].id);
                        }
                        GUI.color = defaultGUIColor;
                        EditorGUILayout.LabelField(openSceneReferences[i].objectName);
                        EditorGUILayout.EndHorizontal();
                    }
                    EditorGUILayout.Space();
                }

                if (prefabReferences.Count > 0)
                {
                    EditorGUILayout.Space();
                    EditorGUILayout.LabelField("<b><color=cyan>Prefabs:</color> <color=yellow>" + prefabReferences.Count + "</color> reference(s)</b>", lblStyle);
                    DrawDivider();
                    EditorGUILayout.Space();
                    for (int i = 0; i < prefabReferences.Count; ++i)
                    {
                        EditorGUILayout.BeginHorizontal();
                        if (GUILayout.Button(pickerBtnContent, pickerBtnStyle, GUILayout.Width(20), GUILayout.Height(20)))
                        {
                            AssetDatabase.OpenAsset(AssetDatabase.LoadMainAssetAtPath(prefabReferences[i].scenePath));
                        }
                        if (GUILayoutUtility.GetLastRect().Contains(mousePos) && lastOpenSceneRefPinged != i + openSceneReferences.Count)
                        {
                            lastOpenSceneRefPinged = i + openSceneReferences.Count;
                            EditorGUIUtility.PingObject(AssetDatabase.LoadMainAssetAtPath(prefabReferences[i].scenePath));
                        }
                        EditorGUILayout.LabelField(Path.GetFileName(prefabReferences[i].scenePath) + " : " + prefabReferences[i].objectName);
                        EditorGUILayout.EndHorizontal();
                    }
                }

                if(otherSceneReferences.Count > 0)
                {
                    EditorGUILayout.Space();
                    EditorGUILayout.Space();
                    EditorGUILayout.LabelField("<b><color=cyan>Other Scenes:</color> <color=yellow>" + otherSceneReferences.Count + "</color> reference(s)</b>", lblStyle);
                    DrawDivider();
                    EditorGUILayout.Space();
                    for (int i = 0; i < otherSceneReferences.Count; ++i)
                    {
                        EditorGUILayout.BeginHorizontal();
                        if (GUILayout.Button(pickerBtnContent, pickerBtnStyle, GUILayout.Width(20), GUILayout.Height(20)))
                        {
                            EditorSceneManager.OpenScene(otherSceneReferences[i].scenePath, OpenSceneMode.Single);
                        }
                        if (GUILayoutUtility.GetLastRect().Contains(mousePos) && lastOpenSceneRefPinged != i + openSceneReferences.Count + prefabReferences.Count)
                        {
                            lastOpenSceneRefPinged = i + openSceneReferences.Count + prefabReferences.Count;
                            EditorGUIUtility.PingObject(AssetDatabase.LoadMainAssetAtPath(otherSceneReferences[i].scenePath));
                        }
                        EditorGUILayout.LabelField(Path.GetFileName(otherSceneReferences[i].scenePath) + " : " + otherSceneReferences[i].id + " uses");
                        EditorGUILayout.EndHorizontal();
                    }
                }

                if (openSceneReferences.Count + prefabReferences.Count + otherSceneReferences.Count <= 0)
                {
                    EditorGUILayout.LabelField(" - No References Found -");
                }
            }

            EditorGUILayout.Space();
            DrawDivider();
            EditorGUILayout.Space();
            EditorGUILayout.Space();

            EditorGUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();
            
            if (GUILayout.Button("Find All References", GUILayout.Width(150))) refresh = true;
            if (GUILayout.Button(EditorGUIUtility.FindTexture("_Popup@2x"))) ShowSettings();

            GUILayout.FlexibleSpace();
            EditorGUILayout.EndHorizontal();

            if (refresh && Event.current.type == EventType.Repaint)
            {
                RefreshUses(SearchRegistryOption.FullRefresh);
                refresh = false;
            }
        }

        private void DrawDivider()
        {
            var rect = EditorGUILayout.BeginHorizontal();
            Handles.color = Color.gray;
            Handles.DrawLine(new Vector2(rect.x - 15, rect.y), new Vector2(rect.width + 15, rect.y));
            EditorGUILayout.EndHorizontal();
        }

        private void RefreshUses(SearchRegistryOption searchOption)
        {
            if (searchOption != SearchRegistryOption.CachedResultsOnly)
            {
                BTaggedSORegistry.OnSearchProgress = UpdateProgressBar;
                BTaggedSORegistry.OnOpenSceneSearchProgress = UpdateOpenSceneProgressBar;
                EditorUtility.DisplayProgressBar("Searching Project", "Finding references to " + target, 0);
            }
            try
            {
                int sceneCount = EditorSceneManager.sceneCount;
                string[] originalScenes = new string[sceneCount];
                for (int s = 0; s < sceneCount; ++s) originalScenes[s] = EditorSceneManager.GetSceneAt(s).path;

                var allReferences = new List<SceneObjectIDBundle>();
                BTaggedSORegistry.References((target as BTaggedSOBase), ref allReferences, searchOption);
                prefabReferences = new List<SceneObjectIDBundle>();
                openSceneReferences = new List<SceneObjectIDBundle>();
                otherSceneReferences = new List<SceneObjectIDBundle>();

                // Distribute all references into appropriate categories
                for (int i = allReferences.Count - 1; i >= 0; --i)
                {
                    if (allReferences[i].scenePath.EndsWith(".prefab"))
                    {
                        prefabReferences.Add(allReferences[i]);
                    }
                    else if (originalScenes.Contains(allReferences[i].scenePath))
                    {
                        openSceneReferences.Add(allReferences[i]);
                    }
                    else
                    {
                        otherSceneReferences.Add(allReferences[i]);
                    }
                }

                // Group other scene references by their path and replace them with a new bundle containing the count
                otherSceneReferences = otherSceneReferences.GroupBy(x => x.scenePath, (scene, sceneRefs) => new SceneObjectIDBundle(scene, string.Empty, sceneRefs.Count())).ToList();
            }
            finally
            {
                if (searchOption != SearchRegistryOption.CachedResultsOnly)
                {
                    BTaggedSORegistry.OnSearchProgress = null;
                    BTaggedSORegistry.OnOpenSceneSearchProgress = null;
                    EditorUtility.ClearProgressBar();
                }
            }
        }

        private void UpdateProgressBar(float current, float total)
        {
            bool cancel = EditorUtility.DisplayCancelableProgressBar("Searching Project", "Finding references to " + target + " " + current + "/" + total, current / total);
            if (cancel) BTaggedSORegistry.CancelSearch = true;
        }
        private void UpdateOpenSceneProgressBar(float current, float total)
        {
            bool cancel = EditorUtility.DisplayCancelableProgressBar("Searching Open Scenes", "Finding references to " + target + " " + current + "/" + total, current / total);
            if (cancel) BTaggedSORegistry.CancelSearch = true;
        }
    }
}
