﻿/* Copyright(c) Tim Watts, Box of Clicks - All Rights Reserved */

using UnityEditor;

namespace BOC.BTagged.EditorTools
{
    [CustomPropertyDrawer(typeof(Tag), true)]
    public class BTaggedTagPropertyDrawer : BTaggedSOPropertyDrawerBase<TagGroup, Tag>
    {
        public BTaggedTagPropertyDrawer()
        {
            label = "Tag";
            defaultLabel = "- Untagged -";
        }
    }
}
