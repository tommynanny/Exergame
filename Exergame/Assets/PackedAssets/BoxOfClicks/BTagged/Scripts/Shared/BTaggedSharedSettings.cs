﻿/* Copyright(c) Tim Watts, Box of Clicks - All Rights Reserved */

using System.IO;
using UnityEditor;
using UnityEditor.Compilation;
using UnityEngine;

namespace BOC.BTagged.Shared
{
    public class BTaggedSharedSettings : ScriptableObject
    {
        static string bTaggedSettingsName = "BTaggedSettings.asset";
        static public string BTaggedSettingsPath
        {
            get
            {
                return Path.Combine(BTaggedSharedUtils.GetASMDEFDirectory(@"BOC.BTagged.Editor"), bTaggedSettingsName);
            }
        }

        public enum SearchRegistryOption
        {
            FullRefresh,
            OpenScenesRefresh,
            CachedResultsOnly
        }


        static public SearchRegistryOption SearchMode => GetOrCreateSettings().searchMode;
        static public bool DisableEditorSafetyChecks => GetOrCreateSettings().disableEditorChecks;

        [SerializeField]
        public SearchRegistryOption searchMode;

        [SerializeField]
        public bool disableEditorChecks;

        internal static BTaggedSharedSettings GetOrCreateSettings()
        {
            string fullPath = BTaggedSettingsPath;
            var settings = AssetDatabase.LoadAssetAtPath<BTaggedSharedSettings>(fullPath);
            if (settings == null)
            {
                settings = ScriptableObject.CreateInstance<BTaggedSharedSettings>();
                settings.searchMode = SearchRegistryOption.FullRefresh;
                settings.disableEditorChecks = false;
                AssetDatabase.CreateAsset(settings, fullPath);
                AssetDatabase.SaveAssets();
            }
            return settings;
        }

        public static SerializedObject GetSerializedSettings()
        {
            return new SerializedObject(GetOrCreateSettings());
        }
    }
}