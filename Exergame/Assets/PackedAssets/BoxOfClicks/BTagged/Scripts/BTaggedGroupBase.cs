﻿/* Copyright(c) Tim Watts, Box of Clicks - All Rights Reserved */

#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace BOC.BTagged
{
    public class BTaggedGroupBase : ScriptableObject 
    {
        public virtual void CheckNotEmpty() { }
    }
    public class BTaggedGroupBase<SO> : BTaggedGroupBase
        where SO : ScriptableObject
    {
#if UNITY_EDITOR
        public override void CheckNotEmpty()
        {
            if (!EditorUtility.IsPersistent(this)) return;

            // If the group is empty, add a default child
            if(AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GetAssetPath(this)).Length == 1)
            {
                var mainObj = AssetDatabase.LoadMainAssetAtPath(AssetDatabase.GetAssetOrScenePath(this));
                SO newSO = ScriptableObject.CreateInstance<SO>();
                newSO.name = "New";
                newSO.hideFlags = HideFlags.None;
                AssetDatabase.AddObjectToAsset(newSO, this);
                AssetDatabase.SetMainObject(mainObj, AssetDatabase.GetAssetPath(this));
                EditorApplication.delayCall += () => AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(this));
            }
        }
#endif
    }
}
