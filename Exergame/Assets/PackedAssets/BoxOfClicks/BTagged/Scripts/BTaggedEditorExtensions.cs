﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BOC.BTagged
{
    public class BTaggedEditorExtensions
    {
        public static void FindAll()
        {
            for (int s = 0; s < SceneManager.sceneCount; ++s)
            {
                for (int r = 0; r < SceneManager.GetSceneAt(s).GetRootGameObjects().Length; ++r)
                {
                    GameObject rootGO = SceneManager.GetSceneAt(s).GetRootGameObjects()[r] as GameObject;
                    if (rootGO != null)
                    {
                        // If searched asset is a GameObject, include its components in the search
                        TagGameObject[] tagComponents = rootGO.GetComponentsInChildren<TagGameObject>(true);
                        for (int c = 0; c < tagComponents.Length; c++)
                        {
                            if (tagComponents[c] == null || tagComponents[c].Equals(null)) continue;

                            var tag = tagComponents[c].tag;
                            if (!BTagged.AllTaggedInOpenScenes.ContainsKey(tag)) BTagged.AllTaggedInOpenScenes.Add(tag, new List<GameObject>());
                            if (!BTagged.AllTaggedInOpenScenes[tag].Contains(tagComponents[c].gameObject)) BTagged.AllTaggedInOpenScenes[tag].Add(tagComponents[c].gameObject);
                        }
                    }
                }
            }
        }
    }
}
#endif