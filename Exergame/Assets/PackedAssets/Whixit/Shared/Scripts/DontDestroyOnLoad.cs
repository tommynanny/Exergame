﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Whixit
{
	public class DontDestroyOnLoad : MonoBehaviour
	{
		void Awake()
		{
			DontDestroyOnLoad(gameObject);		
		}
	}
}
