﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace Whixit
{
	public static class Extensions
	{
		public static void Resize<T>(this List<T> list, int newSize, T content)
		{
			int currentSize = list.Count;

			if(newSize < currentSize)
			{
				list.RemoveRange(newSize, currentSize - newSize);
			}
			else if(newSize > currentSize)
			{
				if(newSize > list.Capacity) list.Capacity = newSize;
				list.AddRange(Enumerable.Repeat(content, newSize - currentSize));
			}
		}



		public static void Resize<T>(this  List<T> list, int newSize) where T : new()
		{
			Resize(list, newSize, new T());
		}



		public static void Set<T>(this List<T> list, int index, T content)
		{
			if(index < list.Count)
			{
				list[index] = content;
			}
			else if(index == list.Count)
			{
				list.Add(content);
			}
			else
			{
				list.Resize(index + 1, default(T));
				list[index] = content;
			}
				

		}



	}
}
