Thank you for purchasing a Whixit product.


Full documentation for this and all other products can be found
at http://www.whixit.com and support on the Unity3D forum at 
http://forum.unity.com/threads/coming-soon-targeting-projector-mousemanager.492414/


We hope you find the Whixit range of products a valuable resource in your game design.
