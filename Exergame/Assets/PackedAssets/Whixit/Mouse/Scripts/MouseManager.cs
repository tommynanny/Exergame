﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;




namespace Whixit.Mouse
{
	[System.Serializable]
	public class MouseManager : MonoBehaviour 
	{
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// INSTANCE VARIABLES & CONSTANTS


		// the library of mouse settings
		[SerializeField]
		private List<WMouse> m_library = new List<WMouse>();



	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// STATIC VARIABLES


		// the singleton instance
		public static MouseManager singleton = null;

		// the current mouse
		private static int m_currentMouse = -1;

		// the mouse position
		private static Vector2 m_position = Vector2.zero;

		// is the mouse locked to the center but visible?
		private static bool m_lockVisible = false;

		// animation timer
		private static float m_animcounter = 0;

		// current animation frame
		private static int m_frame = -1;



	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ACCESSORS


		/// <summary>
		/// Gets the mouse position.
		/// </summary>
		/// <value>The position.</value>
		public static Vector2 Position
		{
			get { return m_position; }
		}
	
		/// <summary>
		/// Gets the library.
		/// </summary>
		/// <value>The library.</value>
		public List<WMouse> Library
		{
			get { return m_library; }
			internal set { m_library = value; }
		}



	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// MONOBEHAVIOURS & INITIALIZATION


		void Awake()
		{
			// are we unique? Ensures that only one instance is existant at runtime
			EnsureSingleton();
		}



		void Update()
		{
			// lock center and visible?
			if(m_lockVisible)
			{
				Cursor.lockState = CursorLockMode.Locked;
				Cursor.lockState = CursorLockMode.None;
			}

			// save the mouse position
			m_position = Input.mousePosition;

			// not animated or hidden?
			if(m_frame == -1 || !Cursor.visible) return;

			// do the animation
			if((m_animcounter += Time.deltaTime) >= m_library[m_currentMouse].AnimationSpeed)
			{
				m_animcounter -= m_library[m_currentMouse].AnimationSpeed;
				Cursor.SetCursor(m_library[m_currentMouse].MouseTexture[m_frame], m_library[m_currentMouse].MouseHotSpot, CursorMode.Auto);
				if(++m_frame >= m_library[m_currentMouse].MouseTexture.Count) m_frame = 0;
			}
		}



	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// PUBLIC STATIC METHODS


		/// <summary>
		/// Sets the mouse cursor from the library.
		/// </summary>
		/// <param name="index">Index.</param>
		public static void SetMouse(int index)
		{
			// must have an instance
			if(singleton == null) throw new System.NullReferenceException("MouseManager.SetMouse: Requires an instance.");

			// does the library contain this index?
			if(index >= 0 && index < singleton.m_library.Count)
			{
				m_currentMouse = index;
				Cursor.SetCursor(singleton.m_library[index].MouseTexture[0], singleton.m_library[index].MouseHotSpot, CursorMode.Auto);
				m_animcounter = 0;
				m_frame = singleton.m_library[index].MouseTexture.Count > 1 ? 0 : -1;
			}
			else
			{
				// set the default
				m_currentMouse = -1;
				Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
				m_animcounter = 0;
				m_frame = -1;
			}
		}



		/// <summary>
		/// Sets the mouse cursor from the library.
		/// </summary>
		/// <param name="index">Index.</param>
		public static void SetMouse(string cursorName)
		{
			// must have an instance
			if(singleton == null) throw new System.NullReferenceException("MouseManager.SetMouse: Requires an instance.");

			// find the library item by name
			for(int n = 0; n < singleton.m_library.Count; n++)
			{
				if(singleton.m_library[n] == null) continue;
				if(singleton.m_library[n].Name == cursorName)
				{
					m_currentMouse = singleton.m_library[n].ID;
					Cursor.SetCursor(singleton.m_library[n].MouseTexture[0], singleton.m_library[n].MouseHotSpot, CursorMode.Auto);
					m_animcounter = 0;
					m_frame = singleton.m_library[n].MouseTexture.Count > 1 ? 0 : -1;
					return;
				}
			}

			// set the default
			m_currentMouse = -1;
			Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
			m_animcounter = 0;
			m_frame = -1;
		}



		/// <summary>
		/// Gets the index of the current mouse in the Mouse Library.
		/// </summary>
		/// <returns>The mouse.</returns>
		public static int GetMouse()
		{
			return m_currentMouse;
		}



		/// <summary>
		/// Sets the mouse visible.
		/// </summary>
		/// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
		public static void Show()
		{
			Cursor.visible = true;
			m_lockVisible = false;
			m_animcounter = 0;
		}



		/// <summary>
		/// Sets the mouse hidden.
		/// </summary>
		/// <value><c>true</c> if enabled; otherwise, <c>false</c>.</value>
		public static void Hide()
		{
			Cursor.visible = false;
			m_lockVisible = false;
		}



		/// <summary>
		/// Sets the mouse visibility.
		/// </summary>
		public static void ToggleVisibility()
		{
			if(Cursor.visible) Hide(); else Show();
		}



		/// <summary>
		/// Locks the mouse to the center of the screen.
		/// </summary>
		/// <value><c>true</c> if lock center; otherwise, <c>false</c>.</value>
		public static void LockCenterVisible()
		{
			Cursor.visible = true;
			Cursor.lockState = CursorLockMode.Locked;
			m_lockVisible = true;
			m_animcounter = 0;
		}



		/// <summary>
		/// locks the mouse and renders it invisible.
		/// </summary>
		/// <value><c>true</c> if lock center; otherwise, <c>false</c>.</value>
		public static void LockCenterHidden()
		{
			Cursor.visible = false;
			Cursor.lockState = CursorLockMode.Locked;
			m_lockVisible = false;
		}



		/// <summary>
		/// Resets the mouse to the center of the screen, making it visible, and setting the library index to zero.
		/// </summary>
		public static void ResetMouse()
		{
			SetMouse(0);
			Cursor.visible = true;
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.lockState = CursorLockMode.None;
			m_lockVisible = false;
		}



	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// INTERNAL METHODS


		/// <summary>
		/// Called to ensure a singleton instance.
		/// </summary>
		private void EnsureSingleton()
		{
			if(singleton != null)
			{
				if(singleton != this) Destroy(this);
				return;
			}

			// get our instance
			singleton = this;
		}

	

	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	}
}
