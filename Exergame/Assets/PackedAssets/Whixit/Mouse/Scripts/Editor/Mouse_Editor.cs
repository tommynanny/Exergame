﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEditor.SceneManagement;




namespace Whixit.Mouse
{
	public class Mouse_Editor
	{
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// MENU ITEMS - MOUSE (300s)


		[MenuItem("Whixit/Add Mouse Manager To Scene", false, 300)]
		static void MenuAddMouse()
		{
			// is there a whixit manager object?
			GameObject whixit = GameObject.Find("/# Whixit");
			if(whixit == null)
			{
				whixit = new GameObject("# Whixit");
				whixit.transform.SetSiblingIndex(0);
				whixit.AddComponent<Whixit.DontDestroyOnLoad>();
			}

			// add a mouse manager to the whixit object
			MouseManager manager = whixit.GetComponentInChildren<MouseManager>();
			if(manager == null)
			{
				GameObject mmanager = new GameObject("# Mouse Manager");
				mmanager.transform.SetParent(whixit.transform);
				mmanager.AddComponent<MouseManager>();
			}

			// make the scene dirty
			EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
		}



		[MenuItem("Whixit/Add Mouse Manager To Scene", true, 300)]
		static bool MenuAddMouse2()
		{
			return (GameObject.FindObjectOfType<MouseManager>() == null);
		}



	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	}
}
