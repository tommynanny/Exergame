﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;



namespace Whixit.Mouse
{
	[CustomEditor(typeof(MouseManager))]
	public class MouseManager_CustomInspector : Editor
	{
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// FIELDS & PARAMETERS


		// whixit root path
		private string _whixitRootPath = "";

		// the serialized object
		private static MouseManager _target;

		// the gui textures
		private static Texture2D _ulogo, _ulogobg, _udark, _umedium, _ulight;

		// the gui boxes
		private static GUIStyle _ulogobox, _ulogoboxtxt, _udarkbox, _umediumbox, _ulightbox;



	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// MONOBEHAVIOURS


		void OnEnable()
		{
			// get our target
			_target = (target as MouseManager);
		}



	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// INSPECTOR & SCENE METHODS


		public override void OnInspectorGUI()
		{
			bool haschanged = false;

			// are we enabled?
			if(!_target.gameObject.activeInHierarchy && _target.enabled) GUI.enabled = false;

			// create the styles first time
			if(_ulogo == null) GenerateStyles();



			// the logo
			//EditorGUILayout.Space();
			//EditorGUILayout.BeginVertical(_udarkbox);
			//EditorGUILayout.BeginHorizontal(_ulogobox);
			//GUILayout.Label(_ulogo, GUILayout.Height(48), GUILayout.Width(138));
			//GUILayout.Label("Whixit.Mouse\nMouse Manager", _ulogoboxtxt);
			//EditorGUILayout.EndHorizontal();
			//EditorGUILayout.EndVertical();
			//EditorGUILayout.Space();
			//EditorGUILayout.Space();



			// help - description
			//EditorGUILayout.HelpBox("The Mouse Manager provides an ease to use library of static or animated mouse cursors for your project.", MessageType.Info);
			//EditorGUILayout.Space();
			//EditorGUILayout.Space();



			// information box - start
			EditorGUILayout.BeginVertical(_udarkbox);
			EditorGUILayout.BeginVertical(_umediumbox);

			// the current mouse position
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Mouse Position");
			GUI.backgroundColor = new Color32(42, 54, 90, 255);
			if(GUI.enabled) EditorGUILayout.SelectableLabel("("+(int)MouseManager.Position.x+" , "+(int)MouseManager.Position.y+")", GUI.skin.textField, GUILayout.Height(16), GUILayout.Width(150));
			GUI.backgroundColor = new Color32(42, 54, 90, 255);
			EditorGUILayout.EndHorizontal();

			// the current mouse cursor
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Mouse Cursor");
			GUI.backgroundColor = new Color32(42, 54, 90, 255);
			if(GUI.enabled) EditorGUILayout.SelectableLabel((MouseManager.GetMouse() == -1 ? "Default (-1)" : MouseManager.singleton.Library[MouseManager.GetMouse()].Name+" ("+MouseManager.GetMouse()+")"), GUI.skin.textField, GUILayout.Height(16), GUILayout.Width(150));
			GUI.backgroundColor = new Color32(42, 54, 90, 255);
			EditorGUILayout.EndHorizontal();

			// information box - end
			EditorGUILayout.EndVertical();
			EditorGUILayout.EndVertical();
			EditorGUILayout.Space();
			EditorGUILayout.Space();



			// button - start
			EditorGUILayout.BeginVertical(_udarkbox);
			EditorGUILayout.BeginVertical(_ulightbox);

			// add button
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.Space();
			if(GUILayout.Button("New Library Item", GUILayout.Width(120)))
			{
				_target.Library.Add(new WMouse());
				_target.Library[_target.Library.Count-1].ID = _target.Library.Count-1;
				EditorUtility.SetDirty(target);
				return;
			}
			EditorGUILayout.Space();
			if(GUILayout.Button("Library Backup", GUILayout.Width(100)))
			{
				LibraryBackup();
				EditorUtility.SetDirty(target);
				return;
			}
			EditorGUILayout.Space();
			if(GUILayout.Button("Library Restore", GUILayout.Width(100)))
			{
				LibraryRestore();
				EditorUtility.SetDirty(target);
				return;
			}
			EditorGUILayout.Space();
			EditorGUILayout.EndHorizontal();
			EditorGUILayout.Space();

			// end content - light
			EditorGUILayout.EndVertical();
			EditorGUILayout.EndVertical();
			EditorGUILayout.Space();
			EditorGUILayout.Space();


			// the mouse cursors library
			if(_target.Library.Count > 0)
			{
				for(int n = 0; n < _target.Library.Count; n++)
				{
					// library item start
					EditorGUILayout.BeginVertical(_udarkbox);
					EditorGUILayout.BeginVertical(_ulightbox);

					EditorGUILayout.BeginHorizontal();
					EditorGUILayout.PrefixLabel("Library Index ["+_target.Library[n].ID+"]");
					string newname = EditorGUILayout.TextField(_target.Library[n].Name, GUILayout.Width(100));
					if(newname != _target.Library[n].Name)
					{
						_target.Library[n].Name = newname;
						haschanged = true;
					}
					EditorGUILayout.Space();
					if(GUILayout.Button("Remove", GUILayout.Width(80), GUILayout.Height(16)))
					{
						_target.Library.RemoveAt(n);
						EditorUtility.SetDirty(_target);
						return;
					}
					EditorGUILayout.EndHorizontal();
					EditorGUILayout.Space();

					EditorGUILayout.BeginHorizontal();
					EditorGUILayout.PrefixLabel("     HotSpot");
					Vector2 newhs = EditorGUILayout.Vector2Field("", _target.Library[n].MouseHotSpot, GUILayout.Width(100));
					if(newhs != _target.Library[n].MouseHotSpot)
					{
						_target.Library[n].MouseHotSpot = newhs;
						haschanged = true;
					}
					EditorGUILayout.EndHorizontal();
					EditorGUILayout.Space();

					EditorGUILayout.BeginHorizontal();
					EditorGUILayout.PrefixLabel("     Texture"+(_target.Library[n].MouseTexture.Count > 1 ? "s" : ""));
					Texture2D newtx = (Texture2D)EditorGUILayout.ObjectField(_target.Library[n].MouseTexture[0], typeof(Texture2D), false);
					if(newtx != _target.Library[n].MouseTexture[0])
					{
						_target.Library[n].MouseTexture[0] = newtx;
						haschanged = true;
					}
					bool animated = EditorGUILayout.ToggleLeft("Animated?", (_target.Library[n].MouseTexture.Count == 1 ? false : true), GUILayout.Width(80));
					if(animated)
					{
						// animated, so ensure more than 1 texture
						if(_target.Library[n].MouseTexture.Count == 1)
						{
							_target.Library[n].MouseTexture.Add(null);
							haschanged = true;
						}
					}
					else
					{
						// not animated, so ensure only 1 texture
						if(_target.Library[n].MouseTexture.Count > 1)
						{
							for(int t = _target.Library[n].MouseTexture.Count-1; t >= 1; t--) _target.Library[n].MouseTexture.RemoveAt(t);
							haschanged = true;
						}
					}
					EditorGUILayout.EndHorizontal();

					if(animated)
						{
						for(int t = 1; t < _target.Library[n].MouseTexture.Count; t++)
						{
							EditorGUILayout.BeginHorizontal();
							EditorGUILayout.PrefixLabel(" ");
							Texture2D newtx2 = (Texture2D)EditorGUILayout.ObjectField(_target.Library[n].MouseTexture[t], typeof(Texture2D), false, GUILayout.Width(200));
							if(newtx2 != _target.Library[n].MouseTexture[t])
							{
								_target.Library[n].MouseTexture[t] = newtx2;
								haschanged = true;
							}
							EditorGUILayout.EndHorizontal();
						}

						EditorGUILayout.BeginHorizontal();
						EditorGUILayout.PrefixLabel(" ");
						if(GUILayout.Button("+", GUILayout.Width(30)))
						{
							_target.Library[n].MouseTexture.Add(null);
							EditorUtility.SetDirty(_target);
							return;
						}
						EditorGUILayout.LabelField("", GUILayout.Width(2));
						if(_target.Library[n].MouseTexture.Count > 2)
						{
							if(GUILayout.Button("-", GUILayout.Width(30)))
							{
								_target.Library[n].MouseTexture.RemoveAt(_target.Library[n].MouseTexture.Count-1);
								EditorUtility.SetDirty(_target);
								return;
							}
						}

						EditorGUILayout.EndHorizontal();
						EditorGUILayout.Space();

						EditorGUILayout.BeginHorizontal();
						EditorGUILayout.PrefixLabel("     Animation Speed");
						float newas = EditorGUILayout.FloatField("", _target.Library[n].AnimationSpeed, GUILayout.Width(100));
						if(newas != _target.Library[n].AnimationSpeed)
						{
							_target.Library[n].AnimationSpeed = newas;
							haschanged = true;
						}
						EditorGUILayout.EndHorizontal();
						EditorGUILayout.Space();
					}

					// library item end
					EditorGUILayout.EndVertical();
					EditorGUILayout.EndVertical();
					EditorGUILayout.Space();
					EditorGUILayout.Space();
				}
			}
			else
			{
				// library item start
				EditorGUILayout.BeginVertical(_udarkbox);
				EditorGUILayout.BeginVertical(_ulightbox);

				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.LabelField("Library contains no custom mouse cursors.");
				EditorGUILayout.EndHorizontal();
				EditorGUILayout.Space();

				// library item end
				EditorGUILayout.EndVertical();
				EditorGUILayout.EndVertical();
				EditorGUILayout.Space();
				EditorGUILayout.Space();
			}
				

			// has anything changed?
			if(haschanged)
			{
				EditorUtility.SetDirty(target);
			}

			// repaint
			Repaint();
		}



	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// INTERNAL METHODS


		private void GetWhixitRoot()
		{
			string[] items = AssetDatabase.FindAssets("Whixit.Shared.Logo.BG", null);
			if(items.Length == 1)
			{
				string path = AssetDatabase.GUIDToAssetPath(items[0]);
				path = path.Replace("\\","/");
				_whixitRootPath = path.Replace("/Shared/Editor/EditorTextures/Whixit.Shared.Logo.BG.png", "");
			}
		}



		private void GenerateStyles()
		{
			// get the whixit root
			if(_whixitRootPath == "") GetWhixitRoot();

			// the logo
			_ulogo = AssetDatabase.LoadAssetAtPath<Texture2D>(_whixitRootPath+"/Shared/Editor/EditorTextures/Whixit.Shared.Logo.png"); 

			// the logo
			_ulogobg = AssetDatabase.LoadAssetAtPath<Texture2D>(_whixitRootPath+"/Shared/Editor/EditorTextures/Whixit.Shared.Logo.BG.png"); 

			// the dark box
			_udark = AssetDatabase.LoadAssetAtPath<Texture2D>(_whixitRootPath+"/Shared/Editor/EditorTextures/Whixit.Shared.Dark.png");

			// the medium box
			_umedium = AssetDatabase.LoadAssetAtPath<Texture2D>(_whixitRootPath+"/Shared/Editor/EditorTextures/Whixit.Shared.Medium.png");

			// the light box
			_ulight = AssetDatabase.LoadAssetAtPath<Texture2D>(_whixitRootPath+"/Shared/Editor/EditorTextures/Whixit.Shared.Light.png");

			// build the ulogobox style
			_ulogobox = new GUIStyle(GUI.skin.box);
			_ulogobox.normal.background = _ulogobg;
			_ulogobox.fixedHeight = 48;
			_ulogobox.stretchWidth = false;
			_ulogobox.margin = new RectOffset(0,0,0,0);

			_ulogoboxtxt = new GUIStyle(GUI.skin.label);
			_ulogoboxtxt.alignment = TextAnchor.MiddleRight;
			_ulogoboxtxt.fontStyle = FontStyle.Bold;
			_ulogoboxtxt.padding = new RectOffset(0,12,6,4);
			_ulogoboxtxt.normal.textColor = new Color32(87,55,23, 255);

			// build the medium box style
			_udarkbox = new GUIStyle(GUI.skin.box);
			_udarkbox.normal.background = _udark;
			_udarkbox.margin = new RectOffset(0,8,0,0);
			_udarkbox.padding = new RectOffset(1,1,1,1);

			// build the medium box style
			_umediumbox = new GUIStyle(GUI.skin.box);
			_umediumbox.normal.background = _umedium;
			_umediumbox.margin = new RectOffset(0,0,0,0);
			_umediumbox.padding = new RectOffset(4,4,4,4);
			_umediumbox.normal.textColor = new Color32(87,55,23, 255);

			// build the light style
			_ulightbox = new GUIStyle(GUI.skin.box);
			_ulightbox.normal.background = _ulight;
			_ulightbox.margin = new RectOffset(0,0,0,0);
			_ulightbox.padding = new RectOffset(4,4,4,4);
			_ulightbox.normal.textColor = new Color32(87,55,23, 255);
		}



		/// <summary>
		/// Backup the Library to the editor save area
		/// </summary>
		private void LibraryBackup()
		{
			// the library backup path
			string libraryPath = _whixitRootPath + "/Shared/Editor/EditorBackupFiles";
			string filePath = libraryPath + "/MouseData.json";

			// ensure directory exists
			if(!Directory.Exists(libraryPath))
			{
				Directory.CreateDirectory(libraryPath);
			}

			// turn the library into json
			string json = "/* JSONESQUE - WHIXIT.MOUSE.LIBRARY */\n\n";
			for(int n = 0; n < _target.Library.Count; n++) json += JsonUtility.ToJson(_target.Library[n]) + "\n\n";

			// save the backup file
			File.WriteAllText(filePath, json);
			AssetDatabase.Refresh();
		}



		/// <summary>
		/// Restore the Library from a backup file.
		/// </summary>
		public void LibraryRestore()
		{
			// the library backup path
			string libraryPath = _whixitRootPath + "/Shared/Editor/EditorBackupFiles";
			string filePath = libraryPath + "/MouseData.json";

			// do we have a backup file?
			if(System.IO.File.Exists(filePath))
			{
				// empty the Library
				_target.Library.Clear();

				// get the file
				string[] lines = System.IO.File.ReadAllLines(filePath);

				// read the data
				foreach(string line in lines)
				{
					if(line.Length < 2) continue;
					if(line.Contains("/*")) continue;
					if(line.Contains("*/")) continue;

					try
					{
						WMouse wm = JsonUtility.FromJson<WMouse>(line);
						_target.Library.Set(wm.ID, wm);
					}
					catch
					{
					}
				}
			}
		}



	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	}
}
