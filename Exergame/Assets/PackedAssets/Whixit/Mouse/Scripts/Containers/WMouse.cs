﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



namespace Whixit.Mouse
{
	[System.Serializable]
	public class WMouse
	{
		/// <summary>
		/// The id of this mouse cursor.
		/// </summary>
		public int ID = 0;

		/// <summary>
		/// The name of this mouse cursor.
		/// </summary>
		public string Name = "";

		/// <summary>
		/// The mouse texture(s).
		/// </summary>
		public List<Texture2D> MouseTexture = new List<Texture2D>();

		/// <summary>
		/// The animation speed.
		/// </summary>
		public float AnimationSpeed = 0.05f;

		/// <summary>
		/// The mouse hot spot.
		/// </summary>
		public Vector2 MouseHotSpot = new Vector2(0,0);

		/// <summary>
		/// Initializes a new instance of the <see cref="Whixit.Mouse.WMouse"/> class.
		/// </summary>
		public WMouse()
		{
			Name = "Name";
			MouseTexture.Add(null);
		}
	}
}
