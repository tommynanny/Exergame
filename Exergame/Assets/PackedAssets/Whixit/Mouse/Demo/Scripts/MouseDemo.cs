﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Whixit.Mouse;


namespace Whixit.Demos
{
	public class MouseDemo : MonoBehaviour
	{
		private int m_mousepointer = 0;



		void Start()
		{
			MouseManager.SetMouse(m_mousepointer);
		}



		void Update()
		{
			if(Input.GetMouseButtonUp(0))
			{
				if(++m_mousepointer > 5) m_mousepointer = 0;
				MouseManager.SetMouse(m_mousepointer);
			}
			if(Input.GetMouseButtonUp(1))
			{
				MouseManager.ToggleVisibility();
			}
			if(Input.GetMouseButtonUp(2))
			{
				MouseManager.ResetMouse();
			}
		}



	}
}
